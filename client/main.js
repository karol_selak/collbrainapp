//window.__start = new Date();
ntimer.start('START');

import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';

import '../imports/startup/nlog';
import '../imports/startup/globalConstants';
import { setPathParams } from '../imports/client/actions';
import createStore from '../imports/client/createStore';
import MainLayout from '../imports/client/containers/MainLayout';
import TheBigSpace from '../imports/client/containers/TheBigSpace';
import '../imports/startup/client/polyfilling';
import '../imports/startup/client/globalTester';
import '../imports/startup/client/i18n';
import Point from 'point-geometry';
import injectTapEventPlugin from 'react-tap-event-plugin';
import pjson from '../package.json'; //TODO zamiast tego zrobić metodę pobierającą wersję z serwera, aby nie udostępniać package.json światu
//przydatne biblioteki wyciągnięte do przestrzeni globalnej:
window.Point = Point;

//ustawienie wersji na podstawie package.json:
document.title += ' '+pjson.version;

injectTapEventPlugin(); //obsługa dotykania

if (Meteor.settings.public.env == 'production' && window.location.protocol != "https:") {
  window.location.protocol = "https:";
}

//start:
Meteor.startup(() => {
  ntimer.log('START', 'startup')
  //Meteor.subscribe('lthoughts'); //TODO prowizorka z subskrypcjami - potem to usunąć
  var store = createStore();
  /**/window._store = store;/**/
  const onEnter = store => {
    ntimer.log('START', 'onEnter')
    return (arg) => {
      setPathParams(arg.location.pathname.slice(1))(store.dispatch, store.getState)
    }  
  }
  render((
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route component={MainLayout}>
          <Route path="/" onEnter={onEnter(store)} components={{main: TheBigSpace}} />
          <Route path="/:path" onEnter={onEnter(store)} components={{main: TheBigSpace}} />
          {/* przykład ścieżki: /th1+open:th2,th3,th4;th5+isClosed+open:th6,th7, gdzie thi to alias myśli albo @ + _id*/}
        </Route>
      </Router>
    </Provider>
  ), document.getElementById('app'));
});
