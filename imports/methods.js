Meteor.methods({
  //TODO czy jest sens zachowywać funkcję dbInit; problem: miałoby to sens, gdyby się dodało myśli takie jak CONJUNCTION, ALTERNATION etc. i inne, które mogą się pojawić w podczas  rozwoju
  dbInit: () => {
    Meteor.call('addUser', {
      username: 'admin',
      email: 'admin@admin.org',
      password: 'admin'
    });
    //czekamy na utworzenie usera
    var iv = Meteor.setInterval(function() {
      /**/nlog('waiting for admin account creating...','#: ThoughtMethods std');/**/
      var id = Meteor.users.findOne({username: 'admin'});
      if(id) {
        clearInterval(iv);
        /**/nlog('admin id: ', id, '#: ThoughtMethods std');/**/
        Roles.addUsersToRoles([id],'admin');
        Meteor.call('dbInitCbHeart', id);
      }
    },1000);
    //TODO generalnie problem wywołania funkcji zaraz po createUser próbowałem rozwiązywać
    //na przeróżne sposoby, przez callbacki, $.wait.then, funkcję onCreateUser, ale ostatecznie
    //musiała zostać ta brzydka pętla oczekująca - jeśli jest lepsze rozwiązanie tego problemu,
    //można je kiedyś zaimplementować
  },
  dbInitCbHeart: (admin_id) => {
    if (!Meteor.isServer) {
      return;
      //wyłączamy symulację w przeglądarce
    }
    //TODO przerobić to, aby dodawanie było poprzez metodę addThought, potem zrobić dodawanie #USERa
    if (!Thoughts.findOne({alias: 'heart'})) {
      Wordings.insert({
        status: 'accepted',
        rating : 1,
        content: 'przestrzeń główna Collbraina',
        visibility: 'public',
        author_id : admin_id,
      }, (err,wid) => {
        if(err) {
          throw new Meteor.Error('#2.3', '' + err);
        }
        Thoughts.insert({
          classes : ['#SPACE'],
          alias : 'heart',
          author_id : admin_id,
          visibility: 'public',
          wordings : [wid]
        }, (err) => {
          if(err) {
            throw new Meteor.Error('#2.4', '' + err);
          }
          Wordings.insert({
            status: 'accepted',
            rating : 1,
            content: 'KLASA',
            className: true,
            visibility: 'public',
            author_id : admin_id,
          }, (err,wid) => {
            if(err) {
              throw new Meteor.Error('#2.3', '' + err);
            }
            Thoughts.insert({
              classes : ['#CLASS'],
              alias : 'CLASS',
              author_id : admin_id,
              visibility: 'public',
              wordings : [wid]
            }, (err) => {
              if(err) {
                throw new Meteor.Error('#2.4', '' + err);
              }
              Wordings.insert({
                status: 'accepted',
                rating : 1,
                content: 'PRZESTRZEŃ',
                className: true,
                visibility: 'public',
                author_id : admin_id,
              }, (err,wid) => {
                if(err) {
                  throw new Meteor.Error('#2.3', '' + err);
                }
                Thoughts.insert({
                  classes : ['#CLASS'],
                  alias : 'SPACE',
                  author_id : admin_id,
                  visibility: 'public',
                  wordings : [wid]
                }, (err) => {
                  if(err) {
                    throw new Meteor.Error('#2.4', '' + err);
                  }
                  
                });
              });
            });
          });
        });
      });
    } else {
      throw new Meteor.Error('# Db init failed - you have HeartOfCollbrain thought already inserted.','Query for {type: "HeartOfCollbrain"}');
    }
  }
});
