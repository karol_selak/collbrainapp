import React, { Component, PropTypes } from 'react';
import Joyride from 'react-joyride';

class Ride extends Component {
  start() {
    this.refs.joyride.reset(true);
  }
  render() {
    const wait = () => {
      this.refs.joyride.stop()
      Meteor.setTimeout(this.refs.joyride.start.bind(this, true), 1500) //czekamy na przeładowanie widoku TODO zrobić to lepiej
    }
    const pause = () => {
      this.refs.joyride.stop();
      Meteor.setTimeout(this.refs.joyride.start, 1500) //czekamy na zastopowanie (?) TODO zrobić to lepiej
    }
    var steps = [{
        text: 'Witaj w Collbrainie! Ten samouczek pozwoli Ci zrozumieć ideę i działanie całego serwisu. Jesli chcesz, możesz też go pominąć i wrócić później - będzie dostępny pod ikonką.',
        selector: '.helpIcon',
        position: 'top-left'
      },{
        text: 'Na początek zaznaczmy, że system jest w bardzo wczesnej wersji, więc niektóre rzeczy mogą działać wolno lub wcale. W razie problemów może pomóc odświeżenie strony.',
        selector: '.version',
        position: 'right'
      },{
        text: 'Collbrain to projekt z misją - naszym celem jest walka z chaosem informacyjnym. Chcemy uczynić Internet miejscem bardziej logicznym i wiarygodnym, gdzie wymiana myśli jest bardziej owocna.',
        selector: '#mission',
        position: 'right'
      },{
        text: 'Aby to osiągnąć, tworzymy bazę wiedzy, która ma gromadzić i przetwarzać dane w podobny sposób, jak czyni to ludzki umysł. Każdy może ją współtworzyć i mieć do niej dostęp.',
        selector: '#foundations',
        position: 'right'
      },{
        text: 'Baza opiera się na jednostkach zwanych myślami. Mogą być to zdania lub pojedyncze słowa - ważne, aby wyrażały jakąś treść. Większość rzeczy, z którymi będziesz mieć tu do czynienia, będzie stanowiła jakiś rodzaj myśli.',
        selector: '.th-body',
        position: 'bottom',
        callback: (arg) => {
          if (arg.type == 'step:before') {
            this.props.setMainThought('sS8G2TCbjypfct5g9', 0);
            wait();
          }
        }
      },{
        text: 'Wszystkie myśli przynależą do klas (które również są odmianą myśli). W przyszłości pozwoli to na lepsze odczytywanie sensu myśli przez algorytmy. Obecnie panuje pełna dowolność w nazywaniu klas. Jeśli klasa o danej nazwie nie istnieje, jest automatycznie tworzona przy dodawaniu',
        selector: '#class_main_0',
        position: 'bottom'
      },{
        text: 'Szczególnym rodzajem myśli są relacje, które łączą inne myśli ze sobą. Na tym przykładzie widzimy relację implikacji łączącą dwa zdania.',
        selector: '#loc_rel_0',
        position: 'right',
        callback: (arg) => {
          if (arg.type == 'step:before') {
            pause();
          } else if (arg.type == 'beacon:trigger') {
            this.props.setMainThought('AA7nRmNLSr4XPYBYN', 0);
          }
        }
      },{
        text: 'W podobny sposób da się rozłożyć każde inne zdanie. Przyjrzyj się, jak rozłożony został ten cytat Alberta Einsteina.',
        selector: '#loc_main_0',
        position: 'right',
        callback: (arg) => {
          if (arg.type == 'step:before') {
            pause();
          } else if (arg.type == 'beacon:trigger') {
            this.props.setMainThought('jNwh4RQFsYYPSiHf5', 0);
          }
        }
      },{
        text: 'Myśli w systemie lokowane są wewnątrz zagnieżdżonych w sobie przestrzeni - każda myśl ma własną. Pełnią one funkcję porządkującą i można je otwierać w trybie zagnieżdżonym, tak jak widzisz teraz.',
        selector: '.helpIcon',
        position: 'top-left',
        callback: (arg) => {
          if (arg.type == 'step:before') {
            pause();
          } else if (arg.type == 'beacon:trigger') {
            this.props.setPathParams('foundations+open:@4hcgknSaa8JtxEEH7,@5YKdnfCJKPkshbMHs');
          }
        }
      },{
        text: 'Niektóre myśli pełnią wyłącznie funkcję przestrzeni. Przykładem jest tu przestrzeń główna, która grupuje w sobie najważniejsze działy Collbraina.',
        selector: '.th-body',
        position: 'bottom',
        callback: (arg) => {
          if (arg.type == 'step:before') {
            pause();
          } else if (arg.type == 'beacon:trigger') {
            this.props.setPathParams('heart');
          }
        }
      },{
        text: 'Szczególnym działem jest metaprzestrzeń - miejsce wymiany myśli dotyczących samego Collbraina. Jeśli masz jakieś uwagi odnośnie naszego serwisu, możesz zamieścić je właśnie tutaj.',
        selector: '#loc_8ezy3msWCB6nJMev4',
        position: 'bottom'
      },{
        text: 'A teraz możesz kliknąć tą ikonkę, by przejść do swojej przestrzeni prywatnej i samemu odkryć pozostałe możliwości Collbraina.',
        selector: '.profileButton',
        position: 'bottom-right'
      }]
    var options = {    
      ref: 'joyride',
      autoStart: true,
      run: !localStorage.getItem('rideFinished'),
      type: 'continuous',
      holePadding: 10,
      callback: (arg) => {
        console.log(arg)
        if (arg.type == 'finished') {
          localStorage.setItem('rideFinished', true); //TODO czy dodać jeszcze jakąś flagę do konta usera
        } else {
          if (steps[arg.index] && steps[arg.index].callback) {
            steps[arg.index].callback(arg);
          }
        }


/*else if ((arg.type == 'step:before' || arg.type == 'tooltip:before') && arg.action != 'close'){
          switch (arg.index) {
            case 3:
              this.props.setMainThought('sS8G2TCbjypfct5g9', 0);
            break;//cdBvFa4Qz8mhdLwi9 meta
            case 444:
              this.props.setMainThought('AA7nRmNLSr4XPYBYN', 0);
              if (arg.action == 'next') {
                this.refs.joyride.stop()
                Meteor.setTimeout(this.refs.joyride.start.bind(this, true), 1000)
              }
            break;//XCauSwJ4Rm6Ap3yGr
            case 6:
              this.props.setMainThought('jNwh4RQFsYYPSiHf5', 0);
              this.refs.joyride.stop()
            break;//XCauSwJ4Rm6Ap3yGr

          }*/
        //}
      },
      showSkipButton: true,
      locale: {
        back: 'Wstecz',
        close: 'Zamknij',
        last: 'Koniec',
        next: 'Dalej',
        skip: 'Zakończ samouczek'
      },
      steps
    }
    return <div>
      <Joyride ref='joyride' {...options} />
      <div id='loc_rel_0' />
    </div>;
  }
}

export default Ride;
