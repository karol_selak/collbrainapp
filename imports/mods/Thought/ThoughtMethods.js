import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Thoughts, Wordings, Locations } from '../../collections';
import { nsets, nlog, nlogi, nlogc } from '../../startup/nlog';
import { ThoughtSchema, WordingSchema, LocationSchema } from './ThoughtSchemas';

const interpretClasses = (newThought, language) => {
  if (newThought.classes.length) {
    newThought.classes = newThought.classes.map((cl,i) => {
      cl = cl.toUpperCase();
      var wrd = {
        content: cl,
        className: true,
        language: language || newThought.wordings[0].language //TODO wymyślić coś lepszego
      }
      var classWrd = Wordings.findOne(wrd);
      if (classWrd) {
        var classTh = Thoughts.findOne({wordings: classWrd._id});
        return classTh._id;
      } else {
        //gdy nie znaleźliśmy takiego typu, tworzymy go:
        wrd.author_id = newThought.author_id;
        wrd.visibility = 'public';
        wrd.addingDate = newThought.addingDate;
        WordingSchema.clean(wrd);
        var wid = Wordings.insert(wrd);
        var date = new Date();
        return Thoughts.insert({
          classes: ['#CLASS'], //konwencja (inna niż przy pathu): tidy to czysty tekst, a aliasy zaczynają się od '#'
          author_id: Meteor.userId(),
          wordings: [wid],
          visibility: 'public',
          status: 'ready',
          addingDate: date,
          commitDate: date,
        })
      }
    });
  }
  return newThought;
}

Meteor.methods({
  changeDraft: (tid, draft) => {
    var th = Thoughts.findOne(tid);
    var userId = Meteor.userId();    
    if (th.author_id != userId) {
      throw new Meteor.Error('# Changing draft failed - access denied');
    }
    if (th.status != 'draft' && !Roles.userIsInRole(Meteor.userId(), 'admin')) {
      throw new Meteor.Error('# Changing draft failed - thought is not a draft and you are not an admin');
    }
    if (draft.status == 'ready') {
      draft.commitDate = new Date();
    }
    //TODO#wrd
    var sanitizedWordings = []; //pamięta kolejność
    var sanitizedWordingsSet = {}; //pamięta przynależność z dostępem O(1)
    draft.wordings.forEach(w => {
      var t = typeof(w);
      if (t == 'string' && Wordings.findOne(w)) { //gdy w tablicy jest id bez pokrycia (nie jest spełniony 2. warunek), wording jest ignorowany
        sanitizedWordings.push(w);
        sanitizedWordingsSet[w] = true;
      } else if (t == 'object') {
        var oldWording = Wordings.findOne(w._id);
        if (oldWording) {
          if (oldWording.author_id == userId) {
            w.author_id = userId;
            w.addingDate = oldWording.addingDate;
            WordingSchema.clean(w);
            if (w.className) { //konwencja: nazwy typów pisane są wielkimi literami
              w.content = wording.content.toUpperCase();
            }
            WordingSchema.validate(w);
            w._id = oldWording._id; //domyślnie WordingSchema nie przepuszcza id-ków
            Wordings.update({_id: w._id}, w);
            console.log(w)
            sanitizedWordings.push(w._id);
            sanitizedWordingsSet[w._id] = true;
          } //else {} - zmiany w obcych wordingach są ignorowane
        } else {
          w = WordingSchema.clean(w); //czyścimy wordinga ze śmieci
          if (w.className) { //konwencja: nazwy typów pisane są wielkimi literami
            w.content = w.content.toUpperCase();
          }
          w.author_id = userId;
          WordingSchema.validate(w);
          w = Wordings.insert(w);
          sanitizedWordings.push(w);
          sanitizedWordingsSet[w] = true;
        }
      }
    });
    //uzupełniamy zbiór docelowy wordingów o wordingi cudze, które nie zostały uwzględnione w drafcie (np. hacker próbował je usunąć jak własne):
    th.wordings.forEach(w => {
      w = Wordings.findOne(w);
      if (w.author_id != userId && !sanitizedWordingsSet[w._id]) {
        sanitizedWordingsSet[w._id] = true;
        sanitizedWordings.push(w._id);
      }
    });
    draft.addingDate = th.addingDate;
    draft.mainLocation = th.mainLocation;
    draft.alias = th.alias;
    draft = interpretClasses(draft); //klasy interpretowane tak jak przy nowym wstawianiu
    draft.wordings = []; //aby przejść przez testy
    delete draft.locations; //TODO czy na pewno będzie co usuwać
    draft = ThoughtSchema.clean(draft);
    draft.author_id = userId;
    ThoughtSchema.validate(draft);
    draft.wordings = sanitizedWordings;
    Thoughts.update({_id: tid}, draft);
  },
  addThought: (newThought, classInterpretation = true) => {
    /**/nlog(' ','#: ThoughtMethods std nts');/**/
    /**/nlog('addThought()',[newThought],'#: ThoughtMethods std');/**/

    if (! Meteor.userId()) {
      throw new Meteor.Error('# Adding thought failed - you are not signed in');
    }
    if (newThought.alias) {
      var th = Thoughts.findOne({alias: newThought.alias});
      if (th) {
        throw new Meteor.Error('# Adding thought failed - there\'s already one thought with alias "'+newThought.alias+'".');
      }
    }
    /**/nlogi('second log ',newThought);/**/
    newThought.addingDate = new Date();
    var locations = newThought.locations || []; //TODO czy to ma sens
    newThought = ThoughtSchema.clean(newThought); //czyścimy myśl ze śmieci
    if (!newThought.input_tid ^ !newThought.output_tid) {//== A xor B
      throw new Meteor.Error('# Adding thought failed - if you have input_tid, output_tid is obligatory and vice versa'); //TODO czy w przyszłości to się nie zmieni; czy jest sens to sprawdzać
    }
    /*if (newThought.input_tid && (newThought.input_tid == newThought.output_tid) && !locations.length) {
      throw new Meteor.Error('# Adding thought failed - self-relations should always have location when adding');
    }*/
    newThought.author_id = Meteor.userId();
    newThought.wordings.forEach(w => {
      w.author_id = Meteor.userId();
      w.addingDate = newThought.addingDate;
      if (w.className) { //konwencja: nazwy typów pisane są wielkimi literami
        w.content = w.content.toUpperCase();
      }
    })
    /**/nlogi('before validating against ThoughtSchema');/**/
    //sprawdzamy poprawność myśli (gdy zła, wywala błąd):
    ThoughtSchema.validate(newThought);
    if (classInterpretation) {
      newThought = interpretClasses(newThought);
    }

    //wstawienie poszczególnych elementów myśli do bazy:
    for (let i in newThought.wordings) {
      var wid = Wordings.insert(newThought.wordings[i]);
      newThought.wordings[i] = wid;
    }
    var tid = Thoughts.insert(newThought);
    var mainLid;
    locations.forEach((loc, idx)=>{
      loc.childTid = tid;
      LocationSchema.clean(loc);
      LocationSchema.validate(loc);
      if (idx == 0) { 
        mainLid = Locations.insert(loc);
      } else {
        Locations.insert(loc);
      }
    });
    Thoughts.update({_id: tid}, {$set: {mainLocation: mainLid}});

    /**/console.log('thought added, tid: ',tid);/**/
    return tid;
  },
  addWording: (wording, tid) => {
    if (! Meteor.userId()) {
      throw new Meteor.Error('# Adding thought failed - you are not signed in');
    }
    wording = WordingSchema.clean(wording); //czyścimy wordinga ze śmieci
    if (wording.className) { //konwencja: nazwy typów pisane są wielkimi literami
      wording.content = wording.content.toUpperCase();
    }
    wording.author_id = Meteor.userId();
    wording.addingDate = new Date();
    WordingSchema.validate(wording);
    var wid = Wordings.insert(wording);
    Thoughts.update({_id: tid}, {$push: {wordings: wid}}); //TODO#wrd
  },
  addLocation: (location) => {
    location = LocationSchema.clean(location);
    LocationSchema.validate(location);
    Locations.insert(location);
  },
  moveLocation: (_id, x, y, parentTid) => {
    /*if (!parentTid) {
      Locations.update({_id}, {$set: {x, y}});
    } else {*/
      Locations.update({_id}, {$set: {x, y, parentTid}});    
    //}
  },
  moveParentLocation: (parentTid, x, y) => {
    x = -x;
    y = -y;
    Locations.update({parentTid}, {$inc: {x, y}}, {multi: true})
  },
  deleteThought: tid => {
    var th = Thoughts.findOne(tid);
    if (Roles.userIsInRole(Meteor.userId(), 'admin') ||
      (th.author_id == Meteor.userId() && th.status == 'draft')) {      
      var rels = Thoughts.find({$or: [{input_tid: tid}, {output_tid: tid}]}).fetch();
      var locs = Locations.find({parentTid: tid}).fetch();
      //TODO czy do warunków blokujących usunięcie myśli nie dodać sytuacji, gdy jakaś myśl jest instancją klasy, którą reprezentuje nasza myśl
      if (rels.length || locs.length) {
        throw new Meteor.Error('# Deleting thought failed - other thoughts are connected to ours. Delete all thoughts and thought locations connected with this thought first.');
      } else {
        Wordings.remove({_id: {$in: th.wordings}}); //TODO co z sytuacją, gdy user usuwa drafta, a są w nim cudze wordingi
        Locations.remove({childTid: tid});
        Thoughts.remove(tid);
      }
    } else {
      throw new Meteor.Error('# Deleting thought failed - access denied');
    }
  },
  deleteLocation: lid => {
    //TODO określić, kto i kiedy może usuwać lokacje
    Locations.remove(lid);
  },

  //metody specjalne administratora:

  restoreDraft: tid => {
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
      Thoughts.update({_id: tid}, {$set: {status: 'draft'}});
    }
  },

  //metody do celów specjalnych:
  /*__newFormatOfLocations__: () => {
    var locs = Locations.find().fetch();
    locs.forEach((loc)=>{
      var tid = Thoughts.findOne({locations: loc._id})._id;
      Locations.update({_id: loc._id}, {$set: {childTid: tid}});
      Locations.update({_id: loc._id}, {$rename: {tid: 'parentTid'}});    
    })
    var ths = Thoughts.find().fetch();
    ths.forEach((th)=>{
      if (th.locations[0]) {
        Thoughts.update({_id: th._id}, {$set: {mainLocation: th.locations[0]}});
      }
    })
    Thoughts.update({}, {$unset: {locations: 1}}, {multi: true});
  },
  __fixProblemsWithMainLocations__: () => {
    //uwaga! działa poprawnie tylko w przypadkach, gdy jednej myśli odpowiada jedna lokacja
    var ths = Thoughts.find({mainLocation: {$exists: false}}).fetch().map(th => th._id);
    var locs = Locations.find({$and: [{childTid: {$in: ths}}, {isConditional: {$exists: false}}]}).fetch();
    console.log(locs);
    locs.forEach((loc)=>{
      Thoughts.update({_id: loc.childTid}, {$set: {mainLocation: loc._id}});
    });
  }*/
});
