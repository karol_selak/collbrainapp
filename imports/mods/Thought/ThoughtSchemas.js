const LocationSchema = new SimpleSchema({
  _id: {
    type: String,
    optional: true //zazwyczaj myśl walidowana jest bez tego pola, choć rzecz jasna każdy dokument musi je mieć
  },
  parentTid: {
    type: String,
  },
  childTid: {
    type: String,
  },
  x: {
    type: Number,
    decimal: true,
    label: 'x coordinate'
  },
  y: {
    type: Number,
    decimal: true,
    label: 'y coordinate'
  },
  isConditional: {
    type: Boolean,
    optional: true
  }
});
const WordingSchema = new SimpleSchema({
  _id: {
    type: String,
    optional: true //zazwyczaj myśl walidowana jest bez tego pola, choć rzecz jasna każdy dokument musi je mieć
  },
  addingDate: {
    type: Date
  },
  status: {
    type: String,
    allowedValues: ['waiting', 'accepted', 'rejected'],
    defaultValue: 'accepted'
  },
  understandability: {
    type: Number,
    min: 0,
    max: 1,
    defaultValue: 1,
    decimal: true
  },
  content: {
    type: String,
    label: 'main content of wording'
  },
  language: {
    type: String,
    optional: true
  },
  className: {
    type: Boolean,
    optional: true,
    label: 'className flag'
  },
  author_id: {
    type: String,
    label: '_id of author (hashtag)'
  },
  visibility: {
    type: String,
    allowedValues: ['private', 'group', 'public'],
    defaultValue: 'private'
  },
  groups: {
    type: [String],
    optional: true,
    label: 'user groups wording is visible for'
  }
});
const ThoughtSchema = new SimpleSchema({
  _id: {
    type: String,
    optional: true //zazwyczaj myśl walidowana jest bez tego pola, choć rzecz jasna każdy dokument musi je mieć
  },
  addingDate: {
    type: Date
  },
  commitDate: {
    type: Date,
    optional: true
  },
  classes: {
    type: [String],
    defaultValue: [],
    label: 'classes (aliases or ids of class thoughts)'
  },
  alias: {
    type: String,
    optional: true,
    label: 'alias (we can use it instead of hash id)'
  },
  author_id: {
    type: String,
    label: '_id of author'
  },
  wordings: {
    type: [WordingSchema],
    defaultValue: [],
    label: 'array with wordings'
  },
  mainLocation: {
    type: String,
    optional: true
  },
  visibility: {
    type: String,
    allowedValues: ['private', 'group', 'public'],
    defaultValue: 'private'
  },
  status: {
    type: String,
    allowedValues: ['draft', 'ready', 'dead'],
    defaultValue: 'draft'
  },
  groups: {
    type: [String],
    optional: true,
    label: 'user groups thought is visible for'
  },
  input_tid: {
    type: String,
    optional: true,
    label: 'tid of input thought (in case of relation)'
  },
  output_tid: {
    type: String,
    optional: true,
    label: 'tid of output thought (in case of relation)'
  },
  ifSymmetric: {
    type: Boolean,
    optional: true,
    label: 'flag indicating if relation is symmetric (in case of relation)'
  }
});
export {LocationSchema, WordingSchema, ThoughtSchema}
