import { Meteor } from 'meteor/meteor';
import { push } from 'react-router-redux';
import { generatePath } from '../../../client/helpers/path';
import { calculateViewBoxScale } from '../../../client/helpers/purecraft';
import { loadThoughts, createScreens, getLocationFromEvent} from './ThoughtHelpers';
import { Thoughts, Wordings, Locations } from '../../../collections';

const setThoughts = () => {
  return (dispatch, getState) => {
    ntimer.log('START', 'setThoughts1')
    var pathParams = getState().pathParams;
    var {thoughts, tidsToLids} = loadThoughts(pathParams);
    //var tidsToLids = getTidsToLids(thoughts);
    var screens = createScreens(pathParams, thoughts, tidsToLids);
    ntimer.log('START', 'setThoughts2')
    nlog('#: special', pathParams, thoughts, tidsToLids, screens);
    dispatch({
      type: 'SET_THOUGHTS_AND_REST',
      thoughts,
      screens,
      tidsToLids
    });
  }
}

const openThought = (tid, screenIdx, alias) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'OPEN_THOUGHT_IN_PATH',
      tid,
      screenIdx,
      alias
    })
    dispatch(push('/'+generatePath(getState().pathParams))); //odświeżenie URLa
    setThoughts()(dispatch, getState);
  }
}
const closeThought = (tid, screenIdx, alias) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'CLOSE_THOUGHT_IN_PATH',
      tid,
      screenIdx,
      alias
    })
    dispatch(push('/'+generatePath(getState().pathParams))); //odświeżenie URLa
    setThoughts()(dispatch, getState);
  }
}
const highlightThought = (tid, ifOpen) => {
  return dispatch => {
    dispatch({
      type: 'HIGHLIGHT_THOUGHT',
      tid, ifOpen
    });
  }
}
const lowlightThought = () => {
  return dispatch => {
    dispatch({
      type: 'LOWLIGHT_THOUGHT',
    });
  }
}
const highlightChildren = (screenIdx) => {
  return dispatch => {
    dispatch({
      type: 'HIGHLIGHT_CHILDREN',
      screenIdx
    });
  }
}
const lowlightChildren = () => {
  return dispatch => {
    dispatch({
      type: 'LOWLIGHT_CHILDREN',
    });
  }
}
const setMainThought = (tid, screenIdx, alias) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_MAIN_THOUGHT',
      tid,
      screenIdx,
      alias
    });
    closeAllChildren(screenIdx)(dispatch, getState);
  }
}
const closeAllChildren = screenIdx => {
  return (dispatch, getState) => {
    dispatch({
      type: 'CLOSE_ALL_CHILDREN',
      screenIdx
    });
    dispatch(push('/'+generatePath(getState().pathParams))); //odświeżenie URLa
    setThoughts()(dispatch, getState);
  }
}
const goToMainParent = screenIdx => {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_MAIN_THOUGHT',
      tid: Locations.findOne(getState().thoughts['main_'+screenIdx].core.mainLocation).parentTid,
      screenIdx
    });
    dispatch(push('/'+generatePath(getState().pathParams))); //odświeżenie URLa
    setThoughts()(dispatch, getState);
  }
}
const moveThought = (args) => {
  var {event, thought, screen, shiftX, shiftY, background} = args;
  return (dispatch, getState) => {
    var {x,y,lid} = getLocationFromEvent(event, screen, shiftX, shiftY, background);
    if (lid == thought.lid) {
      Meteor.call('moveParentLocation', getState().thoughts[lid].tid, x, y);
    } else if (thought.lid.slice(0,4) == 'rel_' || thought.lid.slice(0,12) == 'virtual_rel_') {
      Meteor.call('addLocation', {
        x, y,
        parentTid: getState().thoughts[lid].tid,
        isConditional: true,
        childTid: thought.tid
      })
    } else {
      Meteor.call('moveLocation', thought.lid, x, y, getState().thoughts[lid].tid, (error) => {
        if (!error) {
          return;
        };
        throw error;
      });
    }
  }
}
const addLocation = (args) => {
  var {event, thought, screen, shiftX, shiftY, background, isConditional} = args;
  var {x,y,lid} = getLocationFromEvent(event, screen, shiftX, shiftY, background);
  return (dispatch, getState) => {
    Meteor.call('addLocation', {
      x, y,
      parentTid: getState().thoughts[lid].tid,
      isConditional,
      childTid: thought.tid
    })
  }
}
const deleteThought = (tid) => {
  return () => {
    Meteor.call('deleteThought', tid, (error) => {
      if (!error) {
        return;
      };
      throw error;
    });
  };
}
const deleteLocation = (lid) => {
  return () => {
    Meteor.call('deleteLocation', lid, (error) => {
      if (!error) {
        return;
      };
      throw error;
    });
  };
}
const restoreDraft = tid => {
  return () => {
    Meteor.call('restoreDraft', tid, (error) => {
      if (!error) {
        return;
      };
      throw error;
    });
  };
}
export { deleteThought,
  deleteLocation,
  setThoughts,
  openThought,
  closeThought,
  closeAllChildren,
  goToMainParent,
  setMainThought,
  highlightThought,
  lowlightThought,
  highlightChildren,
  lowlightChildren,
  moveThought,
  addLocation,
  restoreDraft }
