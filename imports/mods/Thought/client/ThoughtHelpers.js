import { Thoughts, Wordings, Locations } from '../../../collections';
import LThought from './LThought';
import { windowToSVG } from '../../../client/helpers/purecraft';
import { littleMoses } from '../../../client/helpers/spaceManagers';

const specialClasses = new Set(['CONJUNCTION', 'ALTERNATION', 'IMPLICATION', 'IFF']);

/*const hasClass = (th, classAlias) => { //TODO w przyszłości zrobić wykrywanie klas, po których się dziedziczy
  for (let i in th.classes) {
    let cl = th.classes[i];
    if (cl.slice(0,1) == '#') {
      if (cl.slice(1) == classAlias) {
        return true;
      }
    } else {
      if (Thoughts.findOne(cl).alias == classAlias) {
        return true;
      }
    }
  }
  return false;
}*/

const detectSpecialClass = th => { //TODO w przyszłości zrobić wykrywanie klas, po których się dziedziczy
  //założenie: dana myśl może posiadać co najwyżej jedną klasę specjalną (więc jak się jakąś znajdzie, to koniec szukania)
  //TODO przemyśleć, czy to założenie na pewno jest dobre
  /*
    Ogólnie sytuacja jest taka, że w przypadku hipotetycznych dwóch klas specjalnych przy jednej myśli
    zachodziłby konflikt - bo nie wiadomo, w jaki sposób to wyświetlić. Potencjalne rozwiazania:
    1. Ustalenie hierarchii klas specjalnych, gdzie mocniejsza zawsze "zasłaniałaby" słabszą.
    2. Pozostawić w gestii użytkownika, który sposób wyświetlania myśli uzna za priorytetowy.
    3. Wdrożyć ogólną hierarchię ważności klas już na poziomie myśli w bazie danych - tak że np. classes[0]
    jest zawsze priorytetowy względem classes[1].
  */
  for (let i in th.classes) {
    let cl = th.classes[i];
    if (cl.slice(0,1) == '#') {
      var alias = cl.slice(1);
      if (specialClasses.has(alias)) {
        th.specialClass = alias;
        return;
      }
    } else {
      var alias = Thoughts.findOne(cl).alias;
      if (specialClasses.has(alias)) {
        th.specialClass = alias;
        return;
      }
    }
  }
}

const joinWithAddings = th => {
  th.author = Meteor.users.findOne(th.author_id);
  var tids = Locations.find({$and: [
    {parentTid: th._id},
    {isConditional: {$ne: true}},
  ]}).fetch().map(e => e.childTid);
  //debugger
    //TODO czy jest sens to optymalizować
  th.childrenAmount = Thoughts.find({_id: {$in: tids}}).count();
  detectSpecialClass(th);
  return th;
}

const tryOpen = (lth, openIds, openAliases, locatedChildren) => {
  /**/nlog('#: ThoughtActions','tryOpen()');/**/
  if (!openIds.has(lth.tid) && !openAliases.has(lth.core.alias)) {
    return;
  }
  /**/nlog('#: ThoughtActions','id or alias found');/**/
  var locs = Locations.find({ parentTid: lth.tid }).fetch();
    //wyszukujemy wszystkie widoczne lokacje
  lth.children = [];
  locs.forEach(loc => {
    //dla każdej lokacji znajdujemy myśl:
    var th = Thoughts.findOne(loc.childTid);
    if (th) {
      joinWithAddings(th);
      //opakowanie myśli konstruktorem:
      var lThought = new LThought(th, loc, lth.lid);
      tryOpen(lThought, openIds, openAliases, locatedChildren);
      getMainWording(lThought);
      getClasses(lThought);
      locatedChildren.push(lThought);
      lth.children.push(lThought.lid);
    }
  });
//  lth.children = newChildren.map(ch => ch.lid);
};
const widenCrater = (childLid, shift, parentTh, managers, thoughts) => { //shift = {x,y}
  shift.x *= parentTh.getChildScale();
  shift.y *= parentTh.getChildScale();
  managers[parentTh.lid].widenCrater(childLid, shift, 'mutate it');
  if (parentTh.parentLid) {
    widenCrater(parentTh.lid, shift, thoughts[parentTh.parentLid], managers, thoughts);
  }
}
const tryCreateManager = (lth, managers, thoughts) => {
  /**/nlog('#: ThoughtActions','tryCreateManger()');/**/
  /**/nlog('#: ThoughtActions nts',managers);/**/  
  if (!lth.children) {
    return;
  }
  var manager = new littleMoses();
  lth.getChildren(thoughts).forEach(ch => {
    if (!ch.children) {
      return;
    }
    manager.addCrater({
      x: ch.x,
      y: ch.y,
      height: ch.getCraterHeight(),
      width: ch.getCraterWidth(),
      id: ch.lid
    }, 'mutate it');
    if (lth.parentLid) {
      widenCrater(lth.lid, {x: ch.getCraterWidth(), y: ch.getCraterHeight()},
        thoughts[lth.parentLid], managers, thoughts);
    }
  });
  managers[lth.lid] = manager;
  lth.getChildren(thoughts).forEach(ch => {
    tryCreateManager(ch, managers, thoughts);  
  });
}
const calculateCoords = (th, managers, thoughts, globalCoords, ptLid = null) => {
  if (th.children) {
    var rectCoords = managers[th.lid].translate(th.getRectCoords());
    if (ptLid) {// && managers[ptLid].getCraters()[th.lid] //ew. wstawić
      var pt_global = globalCoords[ptLid].thoughtCoords;
      var c = managers[ptLid].getCraters()[th.lid];
      var x = c.x < 0 ? c.x-c.width : c.x;
      var y = c.y < 0 ? c.y-c.height : c.y;

      //TODO refactoring
      var corner = managers[th.lid].translate({
        x: th.spaceBorders.left,
        y: th.spaceBorders.top
      });
      var vx = ((c.width + th.getWidth())/2/th.getChildScale() + corner.x)*th.getChildScale();
      var vy = ((c.height + th.getHeight())/2/th.getChildScale() + corner.y)*th.getChildScale();

      var groupCoords = {
        x: x + (c.width)/2 - vx,
        y: y + (c.height)/2 - vy
      };
      globalCoords[th.lid] = {
        thoughtCoords: {
          x: pt_global.x + groupCoords.x * pt_global.scale,
          y: pt_global.y + groupCoords.y * pt_global.scale,
          scale: pt_global.scale * th.getChildScale()
        },
        groupCoords,
        rectCoords
      }
    } else {
      globalCoords[th.lid] = {
        thoughtCoords: {
          x: 0,
          y: 0,
          scale: th.getChildScale()
        },
        groupCoords: {
          x: th.x,
          y: th.y
        },
        rectCoords
      }
    }
    th.getChildren(thoughts).forEach(ch => {
      calculateCoords(ch, managers, thoughts, globalCoords, th.lid);    
    });
  } else {
    if (ptLid) {
      var pt_global = globalCoords[ptLid].thoughtCoords;
      var coords = managers[ptLid].translate({x: th.x, y: th.y});
      globalCoords[th.lid] = {
        thoughtCoords: {
          x: pt_global.x + coords.x * pt_global.scale,
          y: pt_global.y + coords.y * pt_global.scale,
          scale: pt_global.scale
        }
      }
      /*/nlog('global: ', {x: th.global_x, y: th.global_y});/*/
    } else {
      globalCoords[th.lid] = {
        thoughtCoords: {
          x: 0,
          y: 0,
          scale: 1
        }
      }
    }
  }
  /*if (th.isConditional) {
    globalCoords[th.lid].thoughtCoords.scale *= gConst('relationScale');
  }*/
}

//TODO czy createScreens nie powinno być elementem reducera, bez akcji SET_SCREENS, interpretowane jedynie przez wstawianie myśli
const createScreens = (pathParams, thoughts, tidsToLids) => {
  /**/nlog('#: ThoughtActions','createScreens()');/**/
  var screens = [];
  if (pathParams.mainThoughts) {
    //tworzenie screenów z głównych myśli (tych gdzie lid = mainx):
    for (let i = 0; i < pathParams.mainThoughts.length; i++) {
      let th = thoughts['main_'+i];
      if (!th) {
        continue;
      }
      var managers = {
        null: {translate: o => o}
      };
      tryCreateManager(th, managers, thoughts);
      var globalCoords = {};
      calculateCoords(th, managers, thoughts, globalCoords);
      
      //wyliczenie coordsów dla relacji:
      let j = 0;
      while (true) {
        let rel = thoughts['rel_'+j];
        if (!rel) {
          break;
        }
        var input = thoughts[tidsToLids[rel.core.input_tid][0]];
        var output = thoughts[tidsToLids[rel.core.output_tid][0]];
        var inCoords = globalCoords[input.lid].thoughtCoords;
        var outCoords = globalCoords[output.lid].thoughtCoords;
        var x = (inCoords.x + outCoords.x) / 2;
        var y = (inCoords.y + outCoords.y) / 2;
        var scale = (inCoords.scale + outCoords.scale) / 2; // * gConst('relationScale');
        globalCoords['rel_' + j++] = {
          thoughtCoords: {x,y,scale},
          ifRelation: true
        }
      }
      
      //ustalenie viewBoksa przestrzeni SVG:
      var vb = globalCoords['main_'+i].rectCoords;
      screens.push({
        idx: i,
        managers,
        viewBox: {
          x: vb ? vb.x * th.getChildScale() - gConst('tbsMargin') : -500,
          y: vb ? vb.y * th.getChildScale() - gConst('tbsMargin') : -350,
          width: vb ? vb.width * th.getChildScale() + 2*gConst('tbsMargin') : 1000,
          height: vb ? vb.height * th.getChildScale() + 2*gConst('tbsMargin') : 700
        },
        globalCoords
      });
    }
  }
  return screens;
}
const getMainWording = lth => {
  lth.core.wordings[0] = Wordings.findOne(lth.core.wordings[0]);
}
const getClasses = lth => {
  lth.core.classTids = lth.core.classes;
  lth.core.classes = lth.core.classTids.map(cl => {
    if (cl.slice(0,1) == '#') {
      var th = Thoughts.findOne({alias: cl.slice(1)}); //TODO#wrd
    } else {
      var th = Thoughts.findOne(cl); //TODO#wrd
    }
    if (!th) {
      return '';
    }
    for (let i in th.wordings) {
      var wrd = Wordings.findOne(th.wordings[i]);
      if (!wrd || !wrd.className) {
        continue; //szukamy tylko spośród sformułowań będących oficjalnymi nazwami typu
      }
      if (!firstWrd) {
        var firstWrd = wrd;
      }
      if (wrd.language == i18n.getLanguage()) {
        return wrd.content; //jeśli znajdziemy nasz język
      } else if (wrd.language == 'en') {
        var englishWrd = wrd;
      }
    }
    return englishWrd ? englishWrd.content : firstWrd ? firstWrd.content : ''; //jeśli nie znajdziemy naszego języka
  });
}
const loadThoughts = (pathParams) => {
  var ids = [];
  var aliases = [];
  if (pathParams && pathParams.mainThoughts) {
    pathParams.mainThoughts.forEach((th)=>{
      if (th._id) {
        ids.push(th._id);          
      } else if (th.alias) {
        aliases.push(th.alias);          
      } else {
        throw '# Wrong thought in pathParams.mainThoughts';        
      }
    });
    var mainThoughtsFound = Thoughts.find({$or: [
      {alias: {$in: aliases}},
      {_id: {$in: ids}}
    ]}).fetch();
    //TODO informacja w przypadku gdy któraś z myśli (wszystkie) nie została znaleziona w bazie
    var locatedChildren = [];
    var locatedThoughts = mainThoughtsFound.map((thf, idx)=>{
      var pathTh = pathParams.mainThoughts.find(th => th._id == thf._id || th.alias == thf.alias); //wyciągamy z pathParams fragment odpowiadający znalezionej myśli thf, aby zobaczyć, czy ma ona otwarte dzieci
      if (pathTh.openScions) {
        var setOfOpenIds = new Set(pathTh.openScions.map(e => e._id));
        var setOfOpenAliases = new Set(pathTh.openScions.map(e => e.alias));  
      } else {
        var setOfOpenIds = new Set();
        var setOfOpenAliases = new Set(); 
      }
      if (!pathTh.isClosed) {
        setOfOpenIds.add(pathTh._id);
        setOfOpenAliases.add(pathTh.alias);
      }
      //zwykle _id lub alias będzie undefined, dlatego usuwamy ten klucz ze zbioru:
      setOfOpenIds.delete(undefined);        
      setOfOpenAliases.delete(undefined);
      joinWithAddings(thf);
      var lThought = new LThought(thf, {x: 0, y: 0, _id: 'main_'+idx});
      tryOpen(lThought, setOfOpenIds, setOfOpenAliases, locatedChildren);
      getMainWording(lThought);
      getClasses(lThought);
      return lThought;
    })
    locatedThoughts = locatedThoughts.concat(locatedChildren);

    var virtualSelfRels = []; //autorelacje ulokowane w witrualnych lokacjach - uzupełniane podczas findRelations
    //(wirtualne lokacje to takie lokacje, które wyglądają jak lokacje warunkowe relacji, ale nie ma ich w bazie)

    //rekurencyjna funkcja wyszukująca relacje:
    const findRelations = (thoughts, allTids, newTids, i) => {
      var relations = Thoughts.find({$or: [
        {$and: [
          {input_tid: {$in: Object.keys(allTids)}},
          {output_tid: {$in: Object.keys(newTids)}}
        ]},{$and: [
          {input_tid: {$in: Object.keys(newTids)}},
          {output_tid: {$in: Object.keys(allTids)}}
        ]}
      ]}).fetch();
      relations = relations.map(th => {
        joinWithAddings(th);
        if (th.input_tid == th.output_tid) {
          var virtual = new LThought(th, {_id: 'virtual_rel_' + i});
          virtualSelfRels.push(virtual);
        }
        var th = new LThought(th, {_id: 'rel_' + i++});
        getMainWording(th);
        getClasses(th);
        return th;
      });
      var relTids = {};
      relations.forEach(th => { //tworzenie zbioru nowo dodanych tidów
        relTids[th.tid] = true;
      });
      thoughts = thoughts.concat(relations);
      for (let rt in relTids) { //usuwanie tidów, które nie są nowe
        if (allTids[rt]) {
          delete relTids[rt];
        }
      }
      if (Object.keys(relTids).length) {
        return findRelations(thoughts, Object.assign(allTids, relTids), relTids, i);
      } else {
        return thoughts;
      }
    }
    var tids = {};
    locatedThoughts.forEach(th => {
      tids[th.tid] = true;
    });
    locatedThoughts = findRelations(locatedThoughts, tids, tids, 0);


    //zamiana tablicy na indeksowaną mapę:
    var obj = {};
    locatedThoughts.forEach(th => {
      obj[th.lid] = th;      
    });
    locatedThoughts = obj;

    //stworzenie indeksu do wyszukiwania lidów po tidach:
    var tidsToLids = getTidsToLids(locatedThoughts);

    //dodanie autorelacji ulokowanych w wirtualnych lokacjach:
    //TODO unikać dodawania wirtualnych lokacji w momencie, gdy istnieją nie-wirtualne, warunkowe
    virtualSelfRels.forEach((th) => {
      var input = locatedThoughts[tidsToLids[th.core.input_tid][0]];
      th.x = gConst('selfRelation.xShift');
      th.y = gConst('selfRelation.yShift');
      th.isVirtual = true;
      th.parentLid = input.parentLid;
      if (input.children) {
        input.children.push(th.lid);
      } else {
        th.x += input.x;
        th.y += input.y;
        locatedThoughts[th.parentLid].children.push(th.lid);
      }
      locatedThoughts[th.lid] = th;
      if (tidsToLids[th.tid]) {
        tidsToLids[th.tid].push(th.lid);    
      } else {
        tidsToLids[th.tid] = [th.lid];
      }
    })
    
    //obliczanie granic:
    for (let key of Object.keys(locatedThoughts)) {
      locatedThoughts[key].countSpaceBorders(locatedThoughts);
    }
    return {thoughts: locatedThoughts, tidsToLids};
    //TODO optymalizacja, tak aby nie pytać za każdym razem o wszystkie myśli, ale tylko o część
    //(potrzebny nowy argument informujący, co wymaga update'u)
    //problemy z tym związane:
    //1. czy przypadkiem to już nie jest optymalne (optimistic UI)
    //2. czy baza danych jest w stanie poinformować nas, które dokładnie dane się zmieniły (zrozumieć działanie trackera)
  } else {
    return {thoughts: {}, tidsToLids: {}}
  }
//    createScreens()(dispatch, getState);
}
const getLocationFromEvent = (event, screen, shiftX=0, shiftY=0, bkg) => {
  const doWithBkg = () => {
    var lid = bkg.id.slice(4);
    var w = $(window);
    var cs = screen.globalCoords[lid];
    var {x,y} = windowToSVG(screen.viewBox, w.width(), w.height(), event.clientX, event.clientY);
    x = (x - cs.thoughtCoords.x) / cs.thoughtCoords.scale;
    y = (y - cs.thoughtCoords.y) / cs.thoughtCoords.scale;
    x += shiftX;
    y += shiftY;
    var {x,y} = screen.managers[lid].translateBack({x,y})
    return {x,y,lid}
  }
  if (bkg) {
    return doWithBkg();  
  } //else:
  var elems = document.elementsFromPoint(event.clientX, event.clientY);
  for (let i in elems) {
    if (elems[i].id.slice(0,3) == 'bkg') {
      var bkg = elems[i];
      return doWithBkg();
    }
  }
}
const getTidsToLids = (thoughts) => {
  var result = {};
  for (let lid in thoughts) {
    var tid = thoughts[lid].tid;
    if (result[tid]) {
      result[tid].push(lid);    
    } else {
      result[tid] = [lid];
    }
  }
  return result;
}
export {loadThoughts, createScreens, getLocationFromEvent}
