import React, { Component, PropTypes } from 'react';

export default class Crater extends Component {
  render() {
    var c = this.props.managers[this.props.parentLid].getCrater(this.props.lid);
    var bigRectCoords = this.props.bigRectCoords;
    var x = c.x < 0 ? c.x-c.width : c.x; //>
    var y = c.y < 0 ? c.y-c.height : c.y; //>
    this.th = this.props.thoughts[this.props.lid];

    //rozszerzenie o margines:
    x -= this.th.getWidth()/2;
    y -= this.th.getHeight()/2;
    var height = c.height + this.th.getHeight();
    var width = c.width + this.th.getWidth();

    //var ifHover = this.props.addings.highlighted.closeTid == this.th.tid; // || this.props.addings.highlightedChildrenInScreen == this.props.screen.idx; //TODO pomyśleć, czy jest sens podświetlać kratery przy zamykaniu wszystkich dzieci
    //var classes = classNames({
    //  'tsCraters' : true,
    //  'hover' : ifHover
    //});

    return <g className='tsCraters'
      onMouseOver={this.onMouseOver.bind(this)}
      onMouseOut={this.onMouseOut.bind(this)}
      onClick={this.onClick.bind(this)}>
      <rect x={bigRectCoords.x} y={y} width={bigRectCoords.width} height={height} filter='url(#blurY)'/>
      <rect x={x} y={bigRectCoords.y} width={width} height={bigRectCoords.height} filter='url(#blurX)'/>
    </g>;
  }
  onMouseOver() {
    //this.props.highlightThought(this.th.tid);
  }
  onMouseOut() {
    //this.props.lowlightThought(this.th.tid);
  }
  onClick() {
    this.props.lowlightThought(this.th.tid);
    this.props.closeThought(this.th.tid, this.props.screenIdx, this.th.core.alias);
  }
}
Crater.propTypes = {
  managers: PropTypes.object.isRequired,
  parentLid: PropTypes.string.isRequired,
  lid: PropTypes.string.isRequired,
  bigRectCoords: PropTypes.object.isRequired,
  thoughts: PropTypes.object.isRequired,
  highlightThought: PropTypes.func.isRequired,
  lowlightThought: PropTypes.func.isRequired,
  closeThought: PropTypes.func.isRequired,
  screenIdx: PropTypes.number.isRequired
};
