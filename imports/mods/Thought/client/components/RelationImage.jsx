import React, { Component, PropTypes } from 'react';
import { getArrowParams } from '../../../../client/helpers/purecraft';
import { Arrow1 } from '../../../../client/helpers/Shapes';
import ThoughtImage from './ThoughtImage';

export default class RelationImage extends Component {
  onClick() {
    this.props.deleteLocation(this.props.secondLocation.lid);
  }
  render() {
    var center = this.props.coords[this.props.input.lid].thoughtCoords;
    var r1 = {
      center,
      width: this.props.input.getWidth()*center.scale,
      height: this.props.input.getHeight()*center.scale
    }
    center = this.props.coords[this.props.output.lid].thoughtCoords;
    var r2 = {
      center,
      width: this.props.output.getWidth()*center.scale,
      height: this.props.output.getHeight()*center.scale
    }
    if (this.props.secondLocation) {
      var coords = this.props.coords[this.props.secondLocation.lid].thoughtCoords
      var scale = coords.scale;// / gConst('relationScale');
      var p = getArrowParams(r1,r2,coords);
      p.ifSymmetric = this.props.thought.core.ifSymmetric;
      if (this.props.secondLocation.isConditional) {
        return <g className='relation-arrow'>
          <Arrow1 arrowParams={p} scale={scale} filter='url(#sh1)' onClick={this.onClick.bind(this)} className='bent'/>
        </g>;
      } else {
        return <g className='relation-arrow'>
          <Arrow1 arrowParams={p} scale={scale} filter='url(#sh1)' onClick={this.onClick.bind(this)} />
        </g>;
      }
    } else {
      var avScale = (r1.center.scale + r2.center.scale) / 2;
      if (!(r1.center.x == r2.center.x && r1.center.y == r2.center.y)) {
        var p = getArrowParams(r1,r2);
        p.ifSymmetric = this.props.thought.core.ifSymmetric;
        return <g className='relation-arrow'>
          <Arrow1 arrowParams={p} scale={avScale} filter='url(#sh1)' />
        </g>;
      } else { //sytuacja, gdy początek i koniec relacji są w tym samym punkcie:
        avScale *= 0.7; //TODO wyciągnąć stąd te consty i zrobić ogólny przepis na to
        //TODO a może po prostu już na etapie dodawania zadbać, aby relacja A-->A dodawała się od razu z lokacją warunkową
        var pivot = {x: r1.center.x + 100*avScale, y: r1.center.y - 50*avScale};
        var p = getArrowParams(r1,r2, pivot);
        return <g className='relation-arrow'>
          <Arrow1 arrowParams={p} scale={avScale} filter='url(#sh1)' />
        </g>;
      }
    }
    /*return <line x1={p.crossPoint1.x} y1={p.crossPoint1.y} x2={p.crossPoint2.x} y2={p.crossPoint2.y} style={{stroke:'rgb(0,0,0)',strokeWidth:'2'}} />;*/
  }
}
