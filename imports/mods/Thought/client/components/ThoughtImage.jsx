import React, { Component, PropTypes } from 'react';
import Interactive from '../../../../client/helpers/Interactive';
import { onClicks } from '../../../../client/helpers/onClicks';
import { calculateViewBoxScale, windowToSVG, getStrSize } from '../../../../client/helpers/purecraft';
import { Rect1, Triangle, Polygon, AndShield } from '../../../../client/helpers/Shapes';
import UserAvatar from '../../../User/client/UserAvatar';

class ThoughtImage extends Component {
  //metody zapobiegające przypadkowemu przeciąganiu podczas klika: //TODO czy przywrócić to w jakiejś innej formie
  /*onMouseDown(e) {
    if (e.which != 1) {
      return;
    }
    this.savedX = e.pageX;
    this.savedY = e.pageY;
  }
  onMouseUp(e) {
    if (e.which != 1) {
      return;
    }
    if (Math.abs(e.pageX*1 - this.savedX*1) + Math.abs(e.pageY*1 - this.savedY*1) < 5) {//> 
      this.onClick(e);
    }
  }*/
  render() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    this.ifMy = this.props.thought.core.author_id == Meteor.userId();
    return <g transform={this.getThoughtImageTransform()}
      key={'loc_'+th.lid} className='thought-image' >
      
      {th.children ? this.renderParentAdditionalBackground() : null}

      <g filter='url(#sh3)'>
        {this.renderThoughtShapeDefs()}

        {this.renderThoughtBody()}        
        {this.renderAuthorAvatar()}
        {this.renderChildrenAmountLabel()}

        {/**/(()=>{
          ntimer.log('START', 'ThoughtImage');
          ntimer.log('ACTION', 'ThoughtImage');
          ntimer.log('TRUE_ACTION', 'ThoughtImage');
        })()/**/}
        
      </g>
    </g>;
  }
  getThoughtWithWidthAndHeight() {
    return {
      th: this.props.thought,
      w: this.props.thought.getWidth(),
      h: this.props.thought.getHeight()
    }
  }
  getThoughtImageTransform() {
    var result = '';
    result += `translate(${this.props.coords.x}, ${this.props.coords.y})`;
    result += ' ';
    result += `scale(${this.props.coords.scale})`;
    return result;
  }
  renderParentAdditionalBackground() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    var margin = gConst('commonThought.openBackgroundMargin');
    return <Rect1 className='background-of-open-thought'
      filter='url(#sh1.5)'
      x={-w/2 - margin}
      y={-h/2 - margin}
      width={w + 2 * margin}
    height={h + 2 * margin}/>
  }
  renderThoughtShapeDefs() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    switch(th.core.specialClass) {
      case 'CONJUNCTION':
        return this.renderThoughtShapeDefsOption(<AndShield size={20*th.getOwnScale()} x={0} y={0} />);
      default:
        return this.renderThoughtShapeDefsOption(<Rect1 x={-w/2} y={-h/2} width={w} height={h} />);
    }
  }
  renderThoughtShapeDefsOption(interior) {
    return <defs>
      <clipPath id={`clipPath_${this.props.thought.lid}`}>
        {interior}
      </clipPath>
    </defs>
  }

  //class label:
  renderClassLabel() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    return th.getClassLabelParams() && !this.props.noLabels ? <g className='class-label'
      onClick={this.onClassLabelClick.bind(this)}>
      {this.renderClassLabelBackground()}
      {this.renderClassLabelText()}
    </g> : null
  }
  onClassLabelClick() {
    var firstClassTid = this.props.thought.core.classTids[0];
    var ifThoughtHasSomeClass = firstClassTid && firstClassTid.length
    if (!ifThoughtHasSomeClass) {
      this.openEditor(); //jeśli nie ma nazwy klasy, kliknięcie na klasę uruchamia edytor
      //TODO zasada: nie powinno dać się zaakceptować myśli bez ustalonej klasy
    } else if (x[0] != '#') {
     this.props.setMainThought(x, this.props.screen.idx);
    } else {
     this.props.setMainThought(undefined, this.props.screen.idx, x.slice(1));      
    }
  }  
  openEditor() {
    if (!this.ifMy) {
      return;
    } else {
      this.props.openEditor('editing', {
        classes: this.getStringWithClasses(),
        content: this.props.thought.core.wordings[0].content,
        thought: this.props.thought
      });      
    }
  }
  getStringWithClasses() {
    var classes = '';
    for (let i in this.props.thought.core.classes) {
      if (i*1) {
        classes += ', ';
      }
      classes += this.props.thought.core.classes[i];
    }
    return classes; //'class1, class2, class3'
  }
  renderClassLabelBackground() {
    var th = this.props.thought;
    var w = th.getWidth();
    var h = th.getHeight();
    var par = th.getClassLabelParams();
    var clw = par.width;
    var clh = par.height;
    return <Rect1
      id={'class_' + th.lid}
      filter='url(#sh1.5)'
      x={-w/2 + 1}
      y={-h/2 - clh}
      width={clw + 2}
    height={clh * 1.2} />
  }
  renderClassLabelText() {
    var th = this.props.thought;
    var w = th.getWidth();
    var h = th.getHeight();
    var par = th.getClassLabelParams();
    var clw = par.width;
    var clh = par.height;
    return <foreignObject x={-w/2 + 2}
      y={-h/2 - clh}
      width={clw}
      height={clh}>
      <div className='text-container'>
        {this.getClassLabelText()}
      </div>
    </foreignObject>
  }
  getClassLabelText() {
    return this.props.thought.core.classes[0] || '';
      //TODO czy to na pewno dobry pomysł, by wyświetlać tylko jedną klasę
  }


  onBodySingleClick() {
    this.props.openThought(this.props.thought.tid, this.props.screen.idx, this.props.thought.core.alias);
  }
  onBodyDoubleClick() {
    this.props.setMainThought(this.props.thought.tid, this.props.screen.idx, this.props.thought.core.alias);
  }
  onCornerClick() {
    this.openEditor();
  }

  onClick(e) {
    e.preventDefault();
    if (e.altKey && e.ctrlKey && Roles.userIsInRole(Meteor.userId(), 'admin')) {
      this.props.deleteLocation(this.props.thought.lid);
    } else if (e.shiftKey && e.ctrlKey && Roles.userIsInRole(Meteor.userId(), 'admin')) {
      this.props.restoreDraft(this.props.thought.tid);
    } else {
      onClicks(e, this.onBodySingleClick.bind(this), this.onBodyDoubleClick.bind(this), 300);
      
      //naprawa buga z pozostającym kursorem "move":
      const htmlStyle = $('html')[0].style.cursor = ''; //TODO sprawdzić, w którym momencie to rozwiązanie przestało być konieczne
      //edit: czasami nadal ten move pozostawał, więc odkomentowałem - trzeba patrzeć, czy dalej się to dzieje
    }
  }
  getDraggableOptions() {
    return {
      onstart: event => {
        if ((event.buttons && event.buttons != 1) || event.altKey || event.shiftKey) {
          return;
        }
        const ghost = this.refs.group.cloneNode(true);
        ghost.style.filter = 'url(#br0.5)';
        this.refs.group.parentNode.appendChild(ghost);
        this.ghost = ghost;
        const w = $(window);
        this.scale = calculateViewBoxScale(this.props.screen.viewBox, w.width(), w.height()) * this.props.coords.scale;
        //wyliczenie, jaka jest różnica między środkiem myśli a punktem, w którym została ona chwycona:        
        var {x,y} = windowToSVG(this.props.screen.viewBox, w.width(), w.height(), event.clientX, event.clientY);
        var cs = this.props.screen.globalCoords[this.props.thought.lid]
        //var {x: thx, y: thy} = this.props.screen.managers[th.parentLid].translate({x: th.x, y: th.y})
        this.handShiftX = (cs.thoughtCoords.x - x) / cs.thoughtCoords.scale;
        this.handShiftY = (cs.thoughtCoords.y - y) / cs.thoughtCoords.scale;
      },
      onmove: event => {
        if ((event.buttons && event.buttons != 1) || !this.ghost || event.altKey || event.shiftKey) {
          return;
        }
        var elems = document.elementsFromPoint(event.clientX, event.clientY);  
        //podświetlanie przestrzeni, nad którą aktualnie się znajdujemy:
        var ifRemove = true;
        for (let i in elems) {
          if (elems[i].id.slice(0,3) == 'bkg') {
            ifRemove = false;
            if (this.ghost.spaceBelow) {
              this.ghost.spaceBelow.classList.remove('on-drag-over')
            }
            this.ghost.spaceBelow = elems[i]; //zapamiętujemy przestrzeń, nad którą jesteśmy
            this.ghost.spaceBelow.classList.add('on-drag-over')
            break;
          }
        }
        if (this.ghost.spaceBelow && ifRemove) {
          this.ghost.spaceBelow.classList.remove('on-drag-over')
        }
        // keep the dragged position in the data-x/data-y attributes
        const x = (this.ghost.x || 0) + event.dx / this.scale
        const y = (this.ghost.y || 0) + event.dy / this.scale
        // translate the element
        this.ghost.style.webkitTransform =
        this.ghost.style.transform =
        'translate(' + x + 'px, ' + y + 'px)'

        // update the posiion attributes
        this.ghost.x = x;
        this.ghost.y = y;
      },
      onend: event => {
        if (!this.ghost) {
          return;
        }
        if (this.ghost.spaceBelow) {
          this.ghost.spaceBelow.classList.remove('on-drag-over');
        }
        this.refs.group.parentNode.removeChild(this.ghost);
        try {
          if (event.ctrlKey) {
            this.props.addLocation({
              event,
              thought: this.props.thought,
              screen: this.props.screen,
              shiftX: this.handShiftX,
              shiftY: this.handShiftY,
              background: this.ghost.spaceBelow,
              isConditional: false
            })
          } else {
            this.props.moveThought({
              event,
              thought: this.props.thought,
              screen: this.props.screen,
              shiftX: this.handShiftX,
              shiftY: this.handShiftY,
              background: this.ghost.spaceBelow
            })
          }
        } catch(err) {
          setTimeout(()=>{
            throw err
          },0)
 //         console.warn(err) //TODO wyjaśnić, co to za błąd ("Cannot read property 'type'['class'??] of undefined") i czy coś nam psuje
        }
        delete this.ghost;
        delete this.scale;
      }
    }
  }
  getFontAwesomeIcon(name) {
    switch (name) {
      case 'fa-globe':
        return '\uf0ac'
      case 'fa-lock':
        return '\uf023'
      case 'fa-users':
        return '\uf0c0'
      case 'fa-ellipsis-h':
        return '\uf141'
      default:
        throw '# Wrong fontawesome icon name.'
    }
  }


  //render corner:

  renderCorner() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    var classes = classNames({
      corner: true,
      my: this.ifMy    
    });
    var cp = th.getCornerParams();
    return <g className={classes} onClick={this.onCornerClick.bind(this)} 
      transform={`translate(${w/2},${h/2}) scale(${cp.scale*th.getOwnScale()})`} >
      {/*<Polygon
        className='corner-background'
        points={[{x: 0, y: -h/2},
        {x: 0, y: 0},
        {x: -h*gConst('draftCorner.ratio')/2, y: 0}]}
      />*/}
      <ellipse className='corner-background'
        cx={cp.background.cx}
        cy={cp.background.cy}
        rx={cp.background.rx}
        ry={cp.background.ry} />
      <text x={cp.text.x} y={cp.text.y}>
        {this.getFontAwesomeIcon('fa-ellipsis-h')}
      </text>
    </g>
  }

  //render visibility icon:
  
  renderVisibilityIcon() {
    return <g transform={`scale(${this.props.thought.getOwnScale()})`}
      className='visibility-icon'>
      {this.renderVisibilityIconCore()}
    </g>
  }
  renderVisibilityIconCore() {
    switch (this.props.thought.core.visibility) {
      case 'public':
        return this.renderVisibilityIconOption('fa-globe')
      case 'private':
        return this.renderVisibilityIconOption('fa-lock')
      case 'group':
        return this.renderVisibilityIconOption('fa-users')
      default:
        throw '# Wrong thought visibility.'
    }
  }
  renderVisibilityIconOption(icon) {
    var gc = this.props.thought.getVisibilityIconParams();
    return <text x={gc.x} y={gc.y} fontSize={gc.fontSize} >
      {this.getFontAwesomeIcon(icon)}
    </text>
  }
  renderChildrenAmountLabel() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    var c = gConst('commonThought.childrenAmountLabel');
    return th.core.childrenAmount && !th.children ? <foreignObject
      x={w/2 - c.width + c.shiftX}
      y={-h/2 - c.shiftY} >
      <div className='children-amount-label'>
        <span>{th.core.childrenAmount}</span>
      </div>
    </foreignObject> : null;
  }

  //render thought body:

  getThoughtContent() {
    return this.props.thought.core.wordings[0] ? this.props.thought.core.wordings[0].content : '';
  }
  renderAuthorAvatar() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    var sc = th.getOwnScale();
    return <g className='author-avatar' onClick={this.props.openUserThought.bind(this,th.core.author_id,0)}
      transform={`translate(${-w/2-3*sc},${-h/2-3*sc}) scale(${0.2*sc})`}
      filter='url(#sh4)' >
      <circle r={25} cx={25} cy={25} fill='white' ></circle>
      <foreignObject>
        <UserAvatar className='avatar' user={th.core.author} size={50} />
      </foreignObject>
    </g>
  }

  renderThoughtBody() {
    var {th,w,h} = this.getThoughtWithWidthAndHeight();
    var fontSize = th.fontScaling(getStrSize(this.getThoughtContent()).w);

    return <g ref='group'>
      {this.renderClassLabel()}
      <Interactive draggable draggableOptions={this.getDraggableOptions()} >
        {/*<g filter='url(#sh2)'>*/}

        <a href={`/@${th.tid}`}>
          <g className='th-body'
          clipPath={`url(#clipPath_${th.lid})`}
            onClick={e => e.preventDefault()}
            onTouchTap={this.onClick.bind(this)}>
    {/*onMouseDown={this.onMouseDown.bind(this)}
        onMouseUp={this.onMouseUp.bind(this)}*/}
          {/*tło*/}
            <Rect1 className='th-background'
              id={'loc_'+th.lid}
              x={-w/2}
              y={-h/2}
              width={w}
            height={h}/>
            {this.renderVisibilityIcon()}

            {/*pole tekstowe*/}
            <foreignObject x={-th.getWordingWidth()/2+th.getHandle()/2} y={-th.getWordingHeight()/2}
              width={th.getWordingWidth()} height={th.getWordingHeight()}>
              <div className='text-container'>
                <div style={{fontSize}}>{/**/}
                  {this.getThoughtContent()} 
                </div>
              </div>
            </foreignObject>
          </g>
        {/*</g>*/}
        </a>
      </Interactive>
      {this.renderCorner()}
    </g>
  }
}
ThoughtImage.propTypes = {
  coords: PropTypes.object.isRequired,
  thought: PropTypes.object.isRequired,
  screen: PropTypes.object.isRequired,
  openThought: PropTypes.func.isRequired,
  setMainThought: PropTypes.func.isRequired,
  openEditor: PropTypes.func.isRequired
};
export default ThoughtImage;
