import React, { Component, PropTypes } from 'react';
import Crater from './Crater';
import Interactive from '../../../../client/helpers/Interactive';
import {Rect1, Arrow2} from '../../../../client/helpers/Shapes';
import { onClicks } from '../../../../client/helpers/onClicks';

import ThoughtImage from './ThoughtImage';
/**/
TSPACES = [];
TIMS = [];
/**/
class ThoughtSpace extends Component {
  componentDidMount() {
  }
  componentWillUpdate(newProps) {
    //TODO przemyśleć, po co to było i czy jest to konieczne w 1.3
    /*/#/nlog(' ','#: ThoughtSpace std nts');/#/
    /#/nlog('componentWillUpdate()', [this.props.thought.lid, this.props.thought], '#: ThoughtSpace std warn');/#/
    newProps.thought.space = this;
    newProps.thought.manager = this.getManager();
    newProps.thought.parentManager = this.props.parent ? this.props.parent.getManager() : this.getManager(null);
    if ((newProps.thought.x !== this.props.thought.x || newProps.thought.y !== this.props.thought.y) &&
      this.props.parent && this.props.thought.is_opened) {
        //wykrywanie zmian własnego położenia

      /#/nlog('changes detected, newProps: ', newProps, '#: ThoughtSpace std');/#/

      this.props.parent.getManager().updateCrater({
        x: newProps.thought.x,
        y: newProps.thought.y,
        id: this.props.thought.lid
      });
      this.props.parent.forceUpdate();
    }*/
  }
  componentWillUnmount() {

  }
  getManager(id) {
    if (!this.props.thought && !id) {
      return;
    }
    var m = this.props.screen.managers;
    var result = id ? m[id] : m[this.props.thought.lid];
    return result ? result : m[null];
  }
  getParentId() {
    return this.props.parent.thought.lid;
  }
  renderChildren() {
    /**/nlog('renderChildren()','#: ThoughtSpace std');/**/
    return this.props.thought.getChildren(this.props.thoughts).map((th)=>{
      /**/nlogi('child: ',[th.lid, th]);/**/
      //TODO jak spróbujemy zrobić wiele screenów, będziemy mieć tutaj chyba problem, bo będą się zawsze renderować wszystkie dzieci, nawet jeśli w jednym oknie będą zamknięte
      return <ThoughtSpace
        key={th.lid}
        {...this.props}
        thought={th} />;
    });
  }
  renderCraters(bigRectCoords) {
    /**/nlog('renderCraters()','#: ThoughtSpace std');/**/
    var manager = this.getManager();
    var result = [];
    for (var id in manager.getCraters()) {
      result.push(<Crater key={id+'_crater'}
        managers={this.props.screen.managers}
        parentLid={this.props.thought.lid}
        lid={id}
        bigRectCoords={bigRectCoords}
        {...this.props}
        screenIdx={this.props.screen.idx} />);
    }
    return result;
  }
  /*singleClick() {
    this.props.lowlightThought(this.props.thought.tid);
    this.props.setMainThought(this.props.thought.tid, this.props.screen.idx);
  }
  doubleClick() {
    this.props.setMainThought(this.props.thought.tid, this.props.screen.idx);
  }*/
  onCloseButtonClick() {
    this.props.closeThought(this.props.thought.tid, this.props.screen.idx, this.props.thought.core.alias);
  }
/*  onBackgroundClick() {
    this.props.closeThought(this.props.thought.tid, this.props.screen.idx, this.props.thought.core.alias)
  }*/
  onBackgroundClick(e) {
    if (this.ifMain) {
//      this.props.lowlightChildren();
      onClicks(e, ()=>{}, this.props.closeAllChildren.bind(this, this.props.screen.idx), 300);
    } else {
      this.props.lowlightThought(this.props.thought.tid);
      //onClicks(e, this.singleClick.bind(this), this.doubleClick.bind(this), 300);
      this.props.setMainThought(this.props.thought.tid, this.props.screen.idx);
    }
  }
/*  onMouseOver() {
    this.props.highlightThought(this.props.thought.tid);
  }
  onMouseOut() {
    this.props.lowlightThought(this.props.thought.tid);
  }*/
  /*onMouseOverMain() {
    //this.props.highlightChildren(this.props.screen.idx);
  }
  onMouseOutMain() {
    //this.props.lowlightChildren();
  }
  onMouseOver() {
    //this.props.highlightThought(this.props.thought.tid);
  }
  onMouseOut() {
    //this.props.lowlightThought(this.props.thought.tid);
  }*/
  /*onCloseButtonMouseOver() {
    this.props.highlightThought(this.props.thought.tid, true);
  }
  onCloseButtonMouseOut() {
    this.props.lowlightThought(this.props.thought.tid, true);
  }*/
  renderSmallArrows(rectCoords, ifOutside) {
    var middle = new Point(rectCoords.x+rectCoords.width/2, rectCoords.y+rectCoords.height/2);
    var l = 50*0.6;
    var th = 30*0.6;
    var shift = 4;
    var s = 2;
    if (ifOutside) {
      var r = [0, 90, 180, 270];
    } else {
      var r = [180, 270, 0, 90];
    }
    return <g><Arrow2 className='smallArrow'
        transform={`translate(${rectCoords.x - shift},${middle.y}) scale(1,${s}) rotate(${r[0]})`}
        length={l} thickness={th} />
      <Arrow2 className='smallArrow'
        transform={`translate(${middle.x},${rectCoords.y - shift}) scale(${s},1) rotate(${r[1]})`}
        length={l} thickness={th} />
      <Arrow2 className='smallArrow'
        transform={`translate(${rectCoords.x + rectCoords.width + shift},${middle.y}) scale(1,${s}) rotate(${r[2]})`}
        length={l} thickness={th} />
      <Arrow2 className='smallArrow'
        transform={`translate(${middle.x},${rectCoords.y + rectCoords.height + shift}) scale(${s},1) rotate(${r[3]})`}
        length={l} thickness={th} /></g>;
  }
  render() {
    if (!(this.props.thought && this.props.thought.lid)) {
      return null;
    }
    /**/nlog('render()','#: ThoughtSpace std');/**/

    var th = this.props.thought;
    var {thoughtCoords, rectCoords, groupCoords} = this.props.screen.globalCoords[th.lid];
    if (rectCoords && groupCoords) {
      //boki trójkątów narożnych: TODO tu chyba nie powinno być odwołania do gConst, ale do state'a jakiegoś
      //var a = gConst('tsMargin')*2+8;//th.getWidth()*(1/th.getChildScale() - 1);
      //var b = a*gConst('cornerRatio');//th.getHeight()*(1/th.getChildScale() - 1);
      var ifMain = th.lid.slice(0,5) == 'main_';      
      var ifArraysIn = this.props.addings.highlighted.closeTid == th.tid ||
          (!ifMain && this.props.addings.highlightedChildrenInScreen == this.props.screen.idx);
      var ifArraysOut = this.props.addings.highlighted.openTid == th.tid;
      var classes = classNames({
        'ts-background': true,
        'main': ifMain,
        'hover' : ifArraysOut
      });
      this.ifMain = ifMain;
      return <g className='thought-space'
        transform={`translate(${groupCoords.x},${groupCoords.y}) scale(${th.getChildScale()})`}
        >
        <Rect1 className={classes}
          filter='url(#sh6)'
          id={'bkg_'+th.lid}
          x={rectCoords.x} y={rectCoords.y}
          width={rectCoords.width} height={rectCoords.height}
        onClick={this.onBackgroundClick.bind(this)} />
        {/*this.renderCraters(rectCoords)*/}
        {/*<ThoughtImage key={th.lid} thought={th} toggleOpenClosed={this.toggleOpenClosed}/>*/}

        {/*<g className='tsCorners' transform={`translate(${rectCoords.x+8},${rectCoords.y-8})`}>
          <path d={'M '+(rectCoords.width-a)+' 0 h '+a+' v '+b+' Z'} />
        </g>*/}
        {this.renderChildren()}
        {ifArraysIn ? this.renderSmallArrows(rectCoords) : ifArraysOut ? this.renderSmallArrows(rectCoords, true) : null}
        {ifMain ? null : this.renderCloseButton(rectCoords)}
        {/**/(()=>{
          ntimer.log('START', 'ThoughtSpace');
          ntimer.log('ACTION', 'ThoughtSpace');
          ntimer.log('TRUE_ACTION', 'ThoughtSpace');
        })()/**/}
      </g>;
    } else {
      return null;/*<ThoughtImage key={th.lid} thought={th}
                 toggleOpenClosed={this.toggleOpenClosed}
                 manager={this.props.parent ? this.props.parent.getManager() : this.getManager(null)}
      />*/
    }
  }
  renderCloseButton(rectCoords) {
    var gc = gConst('closeButton');
    return <g className='close-button'
      onClick={this.onCloseButtonClick.bind(this)}
      transform={`translate(
        ${rectCoords.x + rectCoords.width - gc.width + gc.shiftX/2},
        ${rectCoords.y-gc.shiftY/2}
      )`}
    >
      {/*<Rect1
        filter='url(#sh4)'
        x={rectCoords.x + rectCoords.width - gc.width + gc.shiftX} y={rectCoords.y - gc.shiftY}
        width={gc.width} height={gc.height}
      onClick={this.onCloseButtonClick.bind(this)} />*/}
      <ellipse className='background'
        filter='url(#sh4)'
        cx={gc.width/2} cy={gc.height/2}
        rx={gc.width/2} ry={gc.height/2}
      />
      <foreignObject>
        <div className='icon'>
        &times;
        </div>
      </foreignObject>

      {/*<Arrow2 className='closeArrow'
        transform={`translate(${rectCoords.x + rectCoords.width - gc.arrow.shiftX},${rectCoords.y + gc.arrow.shiftY}) rotate(135)`}
        length={20} thickness={10} />*/}
    </g>
  }
  addCrater(cr) {
    this.getManager().addCrater(cr);
    if (this.props.parent) {
      this.props.parent.widenCrater(this.props.thought.lid, {
        x: cr.width,
        y: cr.height
      });
    }
  }
  updateCraterBorders(new_th) {
    if (!this.props.parent) {
      return;
    }
    var b = new_th.spaceBorders;
    var oldb = this.props.thought.spaceBorders;
    var diff = {
      left: b.left - oldb.left,
      right: b.right - oldb.right,
      top: b.top - oldb.top,
      bottom: b.bottom - oldb.bottom
    };

    var tl = this.getManager().translate({x: oldb.left, y: oldb.top});
    var br = this.getManager().translate({x: oldb.right, y: oldb.bottom});
    b = {
      left: tl.x + diff.left,
      right: br.x + diff.right,
      top: tl.y + diff.top,
      bottom: br.y + diff.bottom
    };
    var h = (b.bottom - b.top) * new_th.getChildScale() - new_th.getHeight();
    var w = (b.right - b.left) * new_th.getChildScale() - new_th.getWidth();
    if (h < 0) {//>
      h = 0;
    }
    if (w < 0) {//>
      w = 0;
    }
    var crater = this.props.parent.getManager().getCrater(new_th.lid);
    if (crater && (crater.height !== h || crater.width !== w)) {
      h -= crater.height;
      w -= crater.width;
      this.props.parent.widenCrater(new_th.lid, {
        x: w / new_th.getChildScale(),
        y: h / new_th.getChildScale()
      });
    }
  }
  removeCrater(id) {
    var shift = this.getManager().removeCrater(id);
    if (this.props.parent) {
      this.props.parent.widenCrater(this.props.thought.lid, {
        x: -shift.x,
        y: -shift.y
      });
    }
  }
  widenCrater(id, params) {
    params.x *= this.props.thought.getChildScale();
    params.y *= this.props.thought.getChildScale();
    this.getManager().widenCrater(id, params);
    if (this.props.parent) {
      this.props.parent.widenCrater(this.props.thought.lid, params);
    }
  }
  toggleOpenClosed() {
    /**/nlog(' ','#: ThoughtSpace event nts');/**/
    /**/nlog('onMouseOver()',[this.props.thought.lid, this.props.thought],'#: ThoughtSpace event warn');/**/
    if (!this.props.thought.is_opened) {
      /**/nlog('will open','#: ThoughtSpace event');/**/
      this.props.thought.is_opened = true;
      if (this.props.parent) {
        /**/nlogi('has parent');/**/
        //ustalanie wymiarów krateru:
        var b = this.props.thought.spaceBorders;
        var h = (b.bottom - b.top) * this.props.thought.getChildScale() - this.props.thought.getHeight();
        var w = (b.right - b.left) * this.props.thought.getChildScale() - this.props.thought.getWidth();
        if (h < 0) {//>
          h = 0;
        }
        if (w < 0) {//>
          w = 0;
        }
        this.props.parent.addCrater({
          x: this.props.thought.x,
          y: this.props.thought.y,
          height: h,
          width: w,
          id: this.props.thought.lid
        });
      }

    } else {
      /**/nlog('will close','#: ThoughtSpace event');/**/
      if (this.props.parent) {
        /**/nlogi('has parent');/**/
        this.props.parent.removeCrater(this.props.thought.lid);
      }
      this.props.thought.is_opened = false;
    }
    this.props.tbs.forceUpdate();
  }
}
ThoughtSpace.propTypes = {
  thought: PropTypes.object.isRequired,
  thoughts: PropTypes.object.isRequired,
  addings: PropTypes.object.isRequired,
  screen: PropTypes.object.isRequired,
  closeThought: PropTypes.func.isRequired,
  closeAllChildren: PropTypes.func.isRequired,
  setMainThought: PropTypes.func.isRequired,
  highlightThought: PropTypes.func.isRequired,
  lowlightThought: PropTypes.func.isRequired,
};

export default ThoughtSpace;
