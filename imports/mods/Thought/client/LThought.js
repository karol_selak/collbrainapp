///LThought = Located Thought

class LThought {
  constructor(th, loc, parentLid = null) {
    if (!th || !loc) {
      return {};
    }
    this.core = th;
    this.gc = (()=>{
      switch(this.core.specialClass) {
        case 'CONJUNCTION':
          return gConst('conjunctionThought');
        default:
          return gConst('commonThought');
      }
    })();
    this.x = loc.x;
    this.y = loc.y;
    this.isConditional = loc.isConditional;
    this.lid = loc._id;
    this.tid = th._id;
    this.spaceBorders = {
      left: -this.getDefaultBorderLeft(),
      right: this.getDefaultBorderRight(),
      bottom: this.getDefaultBorderBottom(),
      top: -this.getDefaultBorderTop()
    };
    this.parentLid = parentLid;    
  };
  getCraterHeight() {
    var h = (this.spaceBorders.bottom - this.spaceBorders.top) * this.getChildScale() - this.getHeight();
    return h > 0 ? h : 0;
  };  
  getCraterWidth() {
    var w = (this.spaceBorders.right - this.spaceBorders.left) * this.getChildScale() - this.getWidth();
    return w > 0 ? w : 0;
  };
  getRectCoords() {
    var b = this.spaceBorders;    
    return {
      x: b.left,
      y: b.top,
      width: b.right - b.left,
      height: b.bottom - b.top
    };  
  };
  getOwnScale() { //własna skala myśli to skala, która wpływa na rozmiary stałe myśli, jednak związana jest tylko z wyświetlaniem i nie jest brana pod uwagę przy interakcjach (w przeciwieństwie do skali z obiektu thoughtCoords)
    if (this.lid.slice(0,4) == 'rel_' || this.isConditional || this.isVirtual) {
      return 0.5;    
    } else {
      switch(this.core.specialClass) {
        case 'CONJUNCTION':
          return 0.8;
        default:
          return 1;
      }
    }
  }
  fontScaling(x) {
    return this.gc.fontScaling(x) * this.getOwnScale();
  };
  getHandle() {
    return 0;
    //TODO czy idea chwytaka jeszcze nam się na coś przyda
    /*if (this.lid.slice(0,5) != 'main_') { //&& this.lid.slice(0,4) != 'rel_') {
      return gConst('commonThought.handle') * this.getOwnScale();
    } else {
      return 0;
    }*/
  };
  getHeight() {
    return this.gc.height * this.getOwnScale();
  };
  getWidth() {
    return this.gc.width * this.getOwnScale();
  };
  getWordingWidth() {
    return (this.gc.width - this.gc.padding*2 - this.gc.handle) * this.getOwnScale();
  };
  getWordingHeight() {
    return (this.gc.height - this.gc.padding*2) * this.getOwnScale();
  };
  getWordingShiftX() {
    return this.gc.underWordingShift.x * this.getOwnScale();
  };
  getWordingShiftY() {
    return this.gc.underWordingShift.y * this.getOwnScale();
  };
/*  getButtonsX(idx = 0) {
    return this.getWidth()/2 + idx*gConst('commonThought.editButtons.smallShiftX') + gConst('commonThought.editButtons.shiftX')
  };
  getButtonsY() {
    return this.getHeight()/2 + gConst('commonThought.editButtons.shiftY')
  };
  getButtonsRadius() {
    return gConst('commonThought.editButtons.radius')
  };*/
  getTsMargin() {
    return this.gc.tsMargin;
  };
  getChildScale() {
    return this.gc.childScale;
  };
  getDefaultBorderLeft() {
    return this.gc.defaultBorderLeftRight*this.getOwnScale();
  };
  getDefaultBorderRight() {
    return this.gc.defaultBorderLeftRight*this.getOwnScale();
  };
  getDefaultBorderBottom() {
    return this.gc.defaultBorderBottomTop*this.getOwnScale();
  };
  getDefaultBorderTop() {
    return this.gc.defaultBorderBottomTop*this.getOwnScale();
  };
  getClassLabelParams() {
    return this.gc.classLabel;
  };
  getCornerParams() {
    return this.gc.corner;
  };
  getVisibilityIconParams() {
    return this.gc.visibilityIcon;
  };
  getChildren(thoughts) {
    if (this.children) {
      return this.children.map(ch => {
        return thoughts[ch];
      });  
    } else {
      throw "# You cannot get children of closed thought";
    }
  }
  countSpaceBorders(thoughts) {
    this.spaceBorders = {
      left: -this.getDefaultBorderLeft(),
      right: this.getDefaultBorderRight(),
      bottom: this.getDefaultBorderBottom(),
      top: -this.getDefaultBorderTop()
    };
    if (this.children) {
      this.getChildren(thoughts).forEach(ch => {
        /*if (ch.isConditional) {
          return;
        }*/
        if (ch.x - ch.getDefaultBorderLeft() < this.spaceBorders.left) {
          this.spaceBorders.left = ch.x - ch.getDefaultBorderLeft();
        } else if (ch.x + ch.getDefaultBorderRight() > this.spaceBorders.right) {
          this.spaceBorders.right = ch.x + ch.getDefaultBorderRight();
        }
        if (ch.y - ch.getDefaultBorderTop() < this.spaceBorders.top) {
          this.spaceBorders.top = ch.y - ch.getDefaultBorderTop();
        } else if (ch.y + ch.getDefaultBorderBottom() > this.spaceBorders.bottom) {
          this.spaceBorders.bottom = ch.y + ch.getDefaultBorderBottom();
        }
      });
    }
  };
}

export default LThought;
