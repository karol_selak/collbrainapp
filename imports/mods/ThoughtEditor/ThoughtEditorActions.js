const openEditor = (mode = 'adding', draft) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'OPEN_EDITOR',
      mode,
      draft
    })
  }
}
const closeEditor = (draft) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'CLOSE_EDITOR',
      draft
    })
  }
}
const hideEditor = (draft) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'HIDE_EDITOR'
    })
  }
}
const revealEditor = (draft) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'REVEAL_EDITOR'
    })
  }
}
const changeAddingMethod = (method) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'CHANGE_ADDING_METHOD',
      method
    })
  }
}
const addThought = (args) => {
  return (dispath, getState) => {
    var {parentLid, x, y, content, lidIn, lidOut, classes, language, ifSymmetric, status, visibility} = args;
    if (!language) {
      language = i18n.getLanguage();
    }
    Meteor.call('addThought', {
      locations : (x ? [{
        parentTid: getState().thoughts[parentLid].tid,
        x, y
      }] : []),
      input_tid: (lidIn ? getState().thoughts[lidIn].tid : undefined),
      output_tid: (lidOut ? getState().thoughts[lidOut].tid : undefined),
      wordings: [{content, language, visibility}], //TODO! language przemyśleć
      classes, ifSymmetric, status, visibility
    }, (error) => {
      if (!error) {
        return;
      };
      throw error;
    });
  };
};
const changeDraft = (args) => {
  return (dispath, getState) => {
    var {thought, content, lidIn, lidOut, classes, language, ifSymmetric, status, visibility} = args;
    if (!language) {
      language = i18n.getLanguage();
    }
    var wordings = thought.core.wordings;
    wordings[0] = {_id: wordings[0]._id, content, language, visibility};
      //TODO gdy będzie obsługa wordingów, zrobić to jakoś ładniej (pamiętanie indeksu? content jako tablica nowych treści?)
    Meteor.call('changeDraft', thought.tid, {
      input_tid: (lidIn ? getState().thoughts[lidIn].tid : thought.core.input_tid),
      output_tid: (lidOut ? getState().thoughts[lidOut].tid : thought.core.output_tid),
      //jeśli zmieniamy we lub wy, wpisujemy nowe tidy na podstawie zadanych nowych lidów - niestety muszą znajdować się one w pamięci przeglądarki
      //TODO czy nie lepiej zrobić to inaczej
      wordings, classes, status, visibility,
      ifSymmetric: ifSymmetric === undefined ? thought.core.ifSymmetric : ifSymmetric //jeśli chcemy zmienić symetryczność, musimy wyrazić się wprost true albo false
    }, (error) => {
      if (!error) {
        return;
      };
      throw error;
    });
  };
};

export {openEditor, closeEditor, hideEditor, revealEditor, changeAddingMethod, addThought, changeDraft}
