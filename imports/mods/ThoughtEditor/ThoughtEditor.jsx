import React, { Component, PropTypes } from 'react';
import {getLocationFromEvent} from '../Thought/client/ThoughtHelpers';
import {Rect1} from '../../client/helpers/Shapes';
//import selectize from 'selectize';

export default class ThoughtEditor extends Component {
  splitClasses(cl) {
    return cl ? cl.split(',').map(s=>s.trim()) : [];
  }
  addingLayerOnClick(event) {
    var status = this.refs.ifDraft.checked ? 'draft' : 'ready';
    var visibility = $(this.refs.visibility).find(':selected').val();
    if (this.m == 'common') {
      if (this.props.editor.isHidden) {
        this.props.revealEditor();
      }
      var {x,y,lid} = getLocationFromEvent(event, this.props.screen, 0, 0, this.spaceBelow)
      this.props.addThought({
        parentLid: lid,
        x, y, status, visibility,
        content: this.refs.content.value,
        classes: this.splitClasses(this.refs.classes.value)
      });
      this.refs.content.value = '';
      this.refs.content.focus();
    } else if (this.m.slice(0,10) == 'relational') {
      var elems = document.elementsFromPoint(event.clientX, event.clientY);
      for (let i in elems) {
        if (elems[i].id.slice(0,4) == 'loc_') {
          if (this.clickedLid) {
            if (this.props.editor.isHidden) {
              this.props.revealEditor();
            }
            this.props.addThought({
              lidIn: this.clickedLid,
              lidOut: elems[i].id.slice(4),
              content: this.refs.content.value,
              classes: this.splitClasses(this.refs.classes.value),
              status, visibility,
              ifSymmetric: (this.m == 'relationalSymmetric') || undefined //false zamieniamy na undefined, bo undefined w bazie nie pojawia się jako pole - oszczędzamy pamięć
            });
            delete this.clickedLid;
          } else {
            this.clickedLid = elems[i].id.slice(4);
            break;
          }
        }
      }
    } /*else if (this.m == 'mixed') {
      var elems = document.elementsFromPoint(event.clientX, event.clientY);
      for (let i in elems) {
        if (elems[i].id.slice(0,4) == 'loc_') {
          if (this.clickedLid) {
            var averageEvent = {
              clientX: (event.clientX + this.oldEventX) / 2,
              clientY: (event.clientY + this.oldEventY) / 2,
            }
            var {x,y,lid} = getLocationFromEvent(averageEvent, this.props.screen)
            this.props.addThought({
              lidIn: this.clickedLid,
              lidOut: elems[i].id.slice(4),
              content: this.refs.content.value,
              parentLid: lid,
              classes: this.splitClasses(this.refs.classes.value),
              x, y, status, visibility
            });
            delete this.clickedLid;
            delete this.oldEventX;
            delete this.oldEventY;
          } else {
            this.clickedLid = elems[i].id.slice(4);
            this.oldEventX = event.clientX;
            this.oldEventY = event.clientY;
            break;
          }
        }
      }
    }*/
  }
  onDelete() {
    this.props.setModal('QUESTION', null, {
      title: i18n('thEditor.deleteModal.title'),
      content: i18n('thEditor.deleteModal.content'),
      options: [{
        function: this.modalDeleteCompletely,
        name: i18n('thEditor.deleteModal.options.completely'),
        bootstrapBtnClass: 'btn-danger'
      },{
        function: this.modalDeleteLocationOnly,
        name: i18n('thEditor.deleteModal.options.onlyFromThisSpace'),
        bootstrapBtnClass: 'btn-primary'
      }]
    });
  }
  modalDeleteCompletely() {
    this.props.deleteThought(this.props.editor.thoughtDraft.thought.tid)
    this.onClose();
  }
  modalDeleteLocationOnly() {
    this.props.deleteLocation(this.props.editor.thoughtDraft.thought.lid)
    this.onClose();
  }
  onClose() {
    if (this.spaceBelow) {
      this.spaceBelow.classList.remove('on-add-here');
    }    
    this.props.closeEditor(this.props.editor.mode == 'adding' ? {
      content: this.refs.content.value,
      classes: this.refs.classes.value,
//visibility
    } : null);
  }
  onAccept(e) {
    this.props.changeDraft({
      thought: this.props.editor.thoughtDraft.thought,
      content: this.refs.content.value,
      classes: this.splitClasses(this.refs.classes.value),
      status: e.ifCommit ? 'ready' : 'draft',
      visibility: $(this.refs.visibility).find(':selected').val()
    });
    this.onClose();
  }
  onCommit() {
    this.onAccept({ifCommit: true});
  }
  componentWillMount() {
    this.ifAdding = this.props.editor.mode == 'adding';
    if (this.ifAdding) {
      this.addingLayerOnKeyDownBound = this.addingLayerOnKeyDownAdding.bind(this)
    } else {
      this.addingLayerOnKeyDownBound = this.addingLayerOnKeyDown.bind(this)
    }
    document.addEventListener('keydown', this.addingLayerOnKeyDownBound, false)
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.addingLayerOnKeyDownBound, false)
  }
  toggleSymmetric() {
    if (this.props.editor.addingMethod == 'relationalSymmetric') {
      this.props.changeAddingMethod('relational');
    } else {
      this.props.changeAddingMethod('relationalSymmetric');
    }
  }
  onRel() {
    if (this.props.editor.addingMethod != 'relational') {
      this.props.changeAddingMethod('relational');
    } else {
      this.props.hideEditor();
    }
  }
  onTh() {
    if (this.props.editor.addingMethod != 'common') {
      this.props.changeAddingMethod('common')
    } else {
      this.props.hideEditor();
    }
  }
  render() {
    const gc = gConst('thEditor');
    const ifAdding = this.ifAdding; //TODO ??
    const gcTh = gConst('commonThought');
    //var atm = this.props.editor.addThoughtMode;
    this.m = this.props.editor.addingMethod;
    if (ifAdding) {
      var thClasses = classNames('button', 'methodButton', 'common', {enabled: (this.m == 'common')});
      var relClasses = classNames('button', 'methodButton', 'relational', {enabled: (this.m.slice(0,10) == 'relational')});
      var rel2Classes = classNames('button', 'methodButton', 'relationalSymmetric', {enabled: (this.m == 'relationalSymmetric'), inv: (this.m.slice(0,10) != 'relational')});
    }
    var classes = classNames('thoughtEditor', {hidden: this.props.editor.isHidden});
    //var mixClasses = classNames('methodButton', {enabled: (this.m == 'mixed')});
    var vis = this.getVisibility();
    return <div>
      {this.renderAddingLayer()}
      <div className={classes}>
        <div className='content'>
          <div className='thoughtProject'>
            <div className='thoughtProjectInside'>
              <input ref='classes'
                className='classInput'
                type='text'
                autoFocus
                placeholder={i18n('thEditor.thClass')}
              defaultValue={this.props.editor.thoughtDraft && this.props.editor.thoughtDraft.classes} />
              <textarea ref='content'
                placeholder={i18n('thEditor.typeThoughtContentHere') }
              defaultValue={this.props.editor.thoughtDraft && this.props.editor.thoughtDraft.content} />
              <div className='visibility'>
                <select defaultValue={vis} ref='visibility' className="form-control" id="sel1">
                  <option value='private' > {i18n('thEditor.vis.private')}</option>
                  <option value='public' > {i18n('thEditor.vis.public')}</option>
                  {/*<option>{i18n('thEditor.vis.group')}</option>*/}
                </select>
              </div>
              {ifAdding ? <div className='draftCheckbox'>
                  <input type='checkbox' ref='ifDraft' defaultChecked /> {i18n('thEditor.draft')}
                </div> : null}
{/*              <div className='visibility'>
                <select defaultValue={vis} ref='visibility' className="form-control" id="sel1">
                  <option value='private' >{i18n('thEditor.vis.private')}</option>
                  <option value='public' >{i18n('thEditor.vis.public')}</option>
                </select>
              </div>*/}
              {ifAdding ? null : <div type='button' className='commitButton'
                    data-toggle='tooltip'
                    data-placement='top'
                    title={i18n('thEditor.commitThoughtToMakeItActive')}
                  onClick={this.onCommit.bind(this)} >
                    {i18n('thEditor.commit')}
                  </div>}
            </div>
          </div>
          <div className='buttons'>
            {this.props.editor.mode == 'adding' ? [
              <div key='butt1' type='button' className={rel2Classes}
                data-toggle='tooltip'
                data-placement='top'
                title={i18n('thEditor.bilateralRelation')}
              onClick={this.toggleSymmetric.bind(this)}></div>,
              <div key='butt2' type='button' className={relClasses}
                data-toggle='tooltip'
                data-placement='top'
                title={i18n('thEditor.addAsRelation')}
              onClick={this.onRel.bind(this)}></div>,
              <div key='butt3' type='button' className={thClasses}
                data-toggle='tooltip'
                data-placement='top'
                title={i18n('thEditor.addAsCommonThought')}
              onClick={this.onTh.bind(this)}></div>
            ] : [
              <div key='butt1' type='button' className='button acceptButton enabled'
                data-toggle='tooltip'
                data-placement='top'
                title={i18n('thEditor.saveChanges')}
              onClick={this.onAccept.bind(this)}>
                <span className='glyphicon glyphicon-ok'></span>
              </div>,
              /*<div key='butt2' type='button' className='button commitButton enabled' onClick={this.props.changeAddingMethod.bind(this, 'relational')}>
                <span className='glyphicon glyphicon-cloud-upload'></span>
              </div>,*/
              <div key='butt3' type='button' className='button removeButton enabled'
                data-toggle='tooltip'
                data-placement='top'
                title={i18n('thEditor.deleteThought')}
              onClick={this.onDelete.bind(this)}>
                <span className='glyphicon glyphicon-trash'></span>
              </div>
            ] }
            <div type='button'
              className='button closeButton'
              data-toggle='tooltip'
              data-placement='top'
              title={i18n('thEditor.cancel')}
            onClick={this.onClose.bind(this)}>&times;</div>
          </div>
        </div>
      </div>
    </div>
  }
  renderAddingLayer() {
    if (this.ifAdding) {
      return this.renderActiveAddingLayer()
    } else {
      return this.renderAddingLayerForRightClickOnly()
    }
  }
  renderActiveAddingLayer() {
    var addingLayerClasses = classNames('thoughtAddingLayer', this.m + 'Layer');
    return <div className={addingLayerClasses}
      onMouseMove={this.addingLayerOnMove.bind(this)}
      onContextMenu={this.addingLayerOnRightClick.bind(this)}
      onClick={this.addingLayerOnClick.bind(this)}>
    </div>
  }
  renderAddingLayerForRightClickOnly() {
    return <div className='thoughtAddingLayer'
      onContextMenu={this.addingLayerOnRightClick.bind(this)}
      onClick={this.addingLayerOnRightClick.bind(this)}>
    </div>
  }
  addingLayerOnMove(event) {
    var elems = document.elementsFromPoint(event.clientX, event.clientY);  
    //podświetlanie przestrzeni, nad którą aktualnie się znajdujemy:
    var ifRemove = true;
    for (let i in elems) {
      var sl = elems[i].id.slice(0,4);
      if ((this.m == 'common' &&  sl == 'bkg_') || ((this.m.slice(0,10) == 'relational' || this.m == 'mixed') && sl == 'loc_')) {
      //przy metodzie zwykłej podświetlamy przestrzenie, przy relacyjnej myśli
        ifRemove = false;
        if (this.spaceBelow) {
          this.spaceBelow.classList.remove('on-add-here')
        }
        this.spaceBelow = elems[i]; //zapamiętujemy przestrzeń, nad którą jesteśmy
        this.spaceBelow.classList.add('on-add-here')
        break;
      }
    }
    if (this.spaceBelow && ifRemove) {
      this.spaceBelow.classList.remove('on-add-here')
    }
    //TODO identyczny kod jest w ThoughtImage, pomyśleć, czy jest sens wyodrębniać go jako osobną funkcję i jak tak to gdzie ją wsadzić
  }
  addingLayerOnKeyDownAdding(e, x) {
    if (e.keyCode == 27) { //ESC
      this.onClose();
    }
  }
  addingLayerOnKeyDown(e, x) {
    if (e.keyCode == 27) { //ESC
      this.onClose();
    } else if (e.keyCode == 13) { //ENTER
      this.onAccept({});
    }
  }
  addingLayerOnRightClick(e) {
    e.preventDefault();
    if (this.props.editor.isHidden) {
      this.props.revealEditor();
    } else {
      this.onClose();
    }
  }
  getVisibility() {
    try {    
      return this.props.editor.thoughtDraft.thought.core.visibility;
    } catch (e) {
      return this.getMainThought().core.visibility
    }
  }
  getMainThought() {
    return this.props.thoughts['main_'+this.props.screen.idx];
  }
}
ThoughtEditor.propTypes = {
  closeEditor: PropTypes.func.isRequired
}
