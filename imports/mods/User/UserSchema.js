const UserSchema = new SimpleSchema({
  username: {
    type: String
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  password: {
    type: String
  }
});

export default UserSchema;
