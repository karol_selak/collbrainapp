import UserSchema from './UserSchema';

Meteor.methods({
  addUser: (user) => {
    user = UserSchema.clean(user);
    UserSchema.validate(user);
    Accounts.createUser(user);
  },
  addUsersToRoles: (users, roles, group) => {
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
      Roles.addUsersToRoles(users, roles, group);
    } else {
      throw new Meteor.Error('# Adding roles for users failed - access denied');
    }
  },
  removeUsersFromRoles: (users, roles, group) => {
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
      Roles.removeUsersFromRoles(users, roles, group);
    } else {
      throw new Meteor.Error('# Removing roles from users failed - access denied');
    }
  },
  deleteRole: (role) => {
    if (Roles.userIsInRole(Meteor.userId(), 'admin')) {
      Roles.deleteRole(role);
    } else {
      throw new Meteor.Error('# Deleting role failed - access denied');
    }
  },
  generateUserThought: (userId) => {
    var user = Meteor.users.findOne(userId);
    if (!user) {
      throw new Meteor.Error(`# Generating user thought failed - there's no user with id ${userId}.`);
    }
    var name = user.profile ? user.profile.name : user.username;
    try {    
      var language = user.services.facebook.locale.slice(0,2) || 'en';
      //TODO rozwinąć dla innych serwisów
    } catch (e) {
      var language = 'en';
    }
    
    Meteor.call('addThought', {
      _id: userId,
      wordings: [{
          content: name,
          language,
          visibility: 'public'
        }],
      classes: ['#USER'],
      status: 'ready',
      visibility: 'public'
    }, false, (err,tid)=>{
      if (err) {
        throw err;
      }
      Meteor.call('addThought', {
        locations: [{parentTid: tid, x: 130, y: 0}],
        wordings: [{
            content: name + ' - przestrzeń prywatna',
            language: 'pl',
            visibility: 'private' //TODO??? wsparcie dla wielu języków, ale tworzenie tylko po ang i w języku usera
          },{
            content: name + ' - private space',
            language: 'en',
            visibility: 'private'
          }],
        classes: ['#SPACE'],
        status: 'draft',
        visibility: 'private'
      }, false);
    });
  }
});
