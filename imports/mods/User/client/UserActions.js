import { setMainThought } from '../../Thought/client/ThoughtActions';

const openUserThought = (userId, screenIdx = 0) => {
  return (dispatch, getState) => {
    const set = () => {
      setMainThought(userId, screenIdx)(dispatch, getState);
    };
    if (!Thoughts.findOne(userId)) {
      Meteor.call('generateUserThought', userId, set)
    } else {
      set();
    }
  }
}

export { openUserThought }
