import React, { Component, PropTypes } from 'react';
import Avatar from 'react-avatar';

class UserAvatar extends Component {
  render() {
    var u = this.props.user;
    if (u.services && u.services.facebook) {
      return <Avatar className='avatar' facebookId={u.services.facebook.id} size={this.props.size} round />
    } else {
      return <Avatar className='avatar' textSizeRatio={2.2} name={u.username} size={this.props.size} round />
    }
  }
}
UserAvatar.propTypes = {
  user: PropTypes.object.isRequired,
  size: PropTypes.number.isRequired,
}

export default UserAvatar;
