import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

Thoughts = new Mongo.Collection('thoughts');
Wordings = new Mongo.Collection('wordings');
Locations = new Mongo.Collection('locations');

if (Meteor.isServer) {
  Meteor.publishComposite('lthoughts', function(pathParams) {
    var ids = [];
    var aliases = [];
    function pushTh(th) {
      if (th._id) {
        ids.push(th._id);          
      } else if (th.alias) {
        aliases.push(th.alias);          
      } else {
        throw '# Wrong thought in pathParams.mainThoughts';        
      }
    }
    if (pathParams && pathParams.mainThoughts) {
      pathParams.mainThoughts.forEach((th)=>{
        pushTh(th);
        if (th.openScions) {
          th.openScions.forEach(pushTh);
        }
      });
    }

    if (Roles.userIsInRole(this.userId, 'admin')) { //admin widzi wszystko, przynajmniej na razie
      var filter = {};
    } else {
      var filter = {
        //filtracja wyników pod względem praw dostępu:
        $or: [{visibility: 'public'},
          {author_id: this.userId},
          {$and: [{visibility: 'group'},
            {groups: {$in: Roles.getGroupsForUser(this.userId)}}
          ]}
        ]
      }
    }

    //standardowe dane uzupełniające dla myśli:
    const stdThoughtChildren = [{
      //odnajdywanie relacji:
      find: function(thought) {
        return Thoughts.find({$and:
          [{$or: [
            { input_tid: thought._id },
            { output_tid: thought._id }
          ]}, filter]
        });
      },
      //children: /* dodane rekurencyjnie na końcu definicji */
    },{
      //odnajdywanie autorów:
      find: function(thought) {
        return Meteor.users.find({_id: thought.author_id});
      }
    },{
      //odnajdywanie sformułowań:
      find: function(thought) {
        return Wordings.find({$and: [{_id: thought.wordings[0]}, filter]}); //TODO czy na pewno chcemy tylko jeden wrd
      }
    },{
      //odnajdywanie klas:
      find: function(thought) {
        var tids = [];
        var aliases = [];
        thought.classes.forEach((cl)=>{
          if (cl.slice(0,1) == '#') {
            aliases.push(cl.slice(1));
          } else {
            tids.push(cl);
          }
        })
        return Thoughts.find({$and: [ //TODO czy odnajdywanie po aliasach nie przyda się też w innych przypadkach
          {$or: [
            {_id: {$in: tids}},
            {alias: {$in: aliases}}
          ]},
          filter
        ]}); //TODO czy na pewno chcemy wszystkie klasy
      },
      children: [{
        find: function(classThought, thought) {
          return Wordings.find({$and: [
            {_id: {$in: classThought.wordings}},
            {className: true},
            filter
          ]},{
            limit: 1
          }); //TODO czy na pewno taki limit; jak zrobić, żeby właściwy język był widziany
        }
      },{
        find: function(classThought, thought) {
          return Meteor.users.find({_id: classThought.author_id});
        } //TODO czy na pewno potrzebujemy autora klasy
      }]
    }];
    stdThoughtChildren[0].children = stdThoughtChildren; //rekurencyjne odnajdywanie relacji

    return {
      find: function() {
        //znalezienie wszystkich otwartych myśli:
        return Thoughts.find({$and: 
          [{$or: [
            {_id: {$in: ids}},
            {alias: {$in: aliases}}
          ]}, filter]
        });
      },
      children: [{
        //dzieci ulokowane w myśli:
        find: function(thought) {
          return Locations.find({parentTid: thought._id});
        },
        children: [{ //wyłuskanie myśli na podstawie lokacji
          find: function(location, thought) {
            return Thoughts.find({$and: [{_id: location.childTid}, filter]});
          },
          children: [{
            //dzieci dzieci (wyszukiwane w celu przyspieszenia oraz numerków z ilością dzieci):
            find: function(thought) {
              return Locations.find({parentTid: thought._id});
            },
            children: [{ //wyłuskanie myśli na podstawie lokacji
              find: function(location, thought) {
                return Thoughts.find({$and: [{_id: location.childTid}, filter]});
              },
              children: stdThoughtChildren
            }]
          }].concat(stdThoughtChildren)
        }]
      },{
        //znalezienie rodzica myśli głównej:
        find: function(thought) {
          return Locations.find({_id: thought.mainLocation});
        },
        children: [{
          find: function(location) {
            return Thoughts.find({$and: [{_id: location.parentTid}, filter]})
          }
        }]
      }].concat(stdThoughtChildren) //standardowe informacje dla znalezionych otwartych myśli
    }
  });
}
export { Thoughts, Wordings, Locations };