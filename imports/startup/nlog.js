var args_to_arr = function(args) {
  var arr = [];
  for (var i in args) {
    arr.push(args[i]);
  }
  return arr;
};

var nsets = {
  //ustawienia globalne logów:
  if_any: 0,
  if_all: 0,
  if_url: 0,
  if_trace: 0,
  if_type_shown: 1,
  if_debugger: 1,
  if_timeout: 1,
  constant_timeout: 0,

  //filtry wg. plików:
  purecraft: 0,
  ThoughtImage: 0,
  ThoughtSpace: 0,
  ThoughtSpace2: 1,
  TheBigSpace: 1,
  TheBigSpace2: 0,
  collections: 0,
  LThought: 0,
  ThoughtMethods: 1,
  reducers: 1, //odpowiada za wypisywanie nazw wywoływanych akcji

  //filtry wg. roli:
  std: 0,
  event: 0,
  err: 0,
  time: 0,
  special: 0,

  set: function(obj) {
    _.extend(this,obj);
  },
  setDefault: function() {
    this.set({
      if_any: 0,
      if_all: 0,
      if_url: 0,
      if_trace: 1,
      if_type_shown: 1
    });
  }
}
var nlogi = function() {
  args = args_to_arr(arguments);
  args.splice(0,0,'#: inherit');
  nlog.apply(this, args);
}
var nlogc = function() {
  args = args_to_arr(arguments);
  args[0] = '#: ' + args[0];
  nlog.apply(this, args);
}
var nlog = function() {
  args = args_to_arr(arguments);
  var opts;
  for (var i in args) {
    if (!opts && typeof(args[i]) === 'string' && args[i].slice(0,3) === '#: ') {
      opts = args.splice(i,1)[0];
      if (opts === '#: inherit') {
        opts = nsets._last_opts || '';
      } else {
        nsets._last_opts = opts;
      }
      opts = opts.split(' ');
      opts.splice(0,1);
    }
  }
  var show_it = true;
  var params = '';
  var debug = false;
  var url = true;
  var trace = true;
  var warn = false;
  var type_shown = true;
  var timeout = 0;
  if (opts) {
    for (var i in opts) {
      if (opts[i]*1 === opts[i]) {
        timeout = opts[i]*1;
      }
      if (opts[i] === 'debug' || opts[i] === 'deb' || opts[i] === 'pause' || opts[i] === 'p') {
        debug = true;
      } else if (opts[i] === 'no_url' || opts[i] === 'nu') {
        url = false;
      } else if (opts[i] === 'no_type_shown' || opts[i] === 'nts') {
        type_shown = false;
      } else if (opts[i] === 'no_trace' || opts[i] === 'nt') {
        trace = false;
      } else if (opts[i] === 'warn') {
        warn = true;
      } else {
        show_it *= nsets[opts[i]];
        params += opts[i] + ' ';
      }
    }
  }
  params = params.slice(0,params.length-1);
  if (nsets.if_all || (nsets.if_any && show_it)) {
    if (nsets.if_type_shown && type_shown) {
      if (params) {
        args.splice(0,0,'['+params+']: ');
      }
    }
    if (warn) {
      console.warn.apply(console, args);
    } else if (nsets.if_trace) {
      console.trace.apply(console, args);
    } else {
      console.log.apply(console, args);
    }
    if (url && nsets.if_url) {
      var obj = {};
      Error.captureStackTrace(obj, log);
      var str = obj.stack;
      str = str.slice(str.indexOf('(')+1, str.indexOf(')'));
      console.log(str);
    }
    if (debug && nsets.if_debugger) {
      debugger; //ten debugger ma tutaj zostać
    }
    if (nsets.constant_timeout) {
      timeout = timeout > nsets.constant_timeout ? timeout : nsets.constant_timeout;
    }
    if (timeout && nsets.if_timeout) {
      if (timeout > 5000) {
        console.warn('# log timeout set to value higher than 5s - break canceled');
      } else {
        endtime = new Date().getTime() + timeout;
        while (new Date().getTime() < endtime) {}
      }
    }
  }
}
var ntimer = {
  mute: true,
  logs: [],
  start(name) {
    this[name] = new Date();
    if (this.mute) {
      this.logs.push('@@[ntimer]: start timer '+ (name || ''))
    } else {
      nlog('@@[ntimer]: start timer '+ (name || ''));
    }

  },
  log(name, comment) {
    if (this[name]) {
      if (this.mute) {
        this.logs.push('#: time', name + ' - ' + comment + ' - '+((new Date())-this[name])+' ms')
      } else {
        nlog('#: time', name + ' - ' + comment + ' - '+((new Date())-this[name])+' ms')
      }
    }
  },
  del(name) {
    delete this[name];
  }
}
if (Meteor.isClient) {
  window.nsets = nsets;
  window.nlog = nlog;
  window.nlogi = nlogi;
  window.nlogc = nlogc;
  window.ntimer = ntimer;
}
export {nsets, nlog, nlogi, nlogc, ntimer}
