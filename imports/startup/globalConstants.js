import { backlooker } from '../client/helpers/purecraft';

var constants = backlooker({
  svg: "http://www.w3.org/2000/svg",
  xlink: "http://www.w3.org/1999/xlink",
  goToParentButton: {
  //xShift: get,
  //yShift: get,   
    height: 60,
    width: 110,
    get xShift() {
      return this._.tbsMargin * 0.7;
    },
    get yShift() {
      return this._.tbsMargin * 0.4;
    }
  },
  relationScale: 1,
  openTriangles: {
    ratio: 1.1,
    hidden: 0.5,
    size: 0.3
  },
  closeButton: {
    width: 25,
    height: 25,
    shiftX: 15,
    shiftY: 15,
    arrow: {
      shiftX: 9,
      shiftY: 9
    }
  },
  commonThought: {
    openBackgroundMargin: 10,
    height: 50,
    width: 100,
    padding: 0,
    handle: 6,
    classLabel: {
      get width() {
        return this._.width * 0.4;
      },
      height: 8
    },
    underWordingShift: {
      x: 5,
      y: 5
    },
    tsMargin: 30,
    childScale: 0.75,
    editButtons: {
      radius: 6,
      shiftX: -2,
      shiftY: -5,
      get smallShiftX() {
        return -this.radius*2 - 0.5;
      }
    },
    fontScaling: x => 5.5/Math.log2(0.0025*x/3*2+1.18) + 1,  //przełożenie długości tekstu na rozmiar czcionki
    get ratio() {
      return this.height/this.width;
    },
    get defaultBorderLeftRight() {
      return this.width/2 + this.tsMargin;
    },
    get defaultBorderBottomTop() {
      return this.height/2 + this.tsMargin;
    },
    childrenAmountLabel: {
      shiftX: 1,
      shiftY: 1,
      width: 13,
      height: 9
    },
    corner: {
      scale: 0.8,
      background: {
        cx: -8,
        cy: -8,
        rx: 8,
        ry: 8
      },
      text: {
        get x() {
          return this._.background.cx - 5.2;
        },
        get y() {
          return this._.background.cy + 5;
        }
      }
    },
    visibilityIcon: {
      x: -75,
      y: 50,
      fontSize: 100
    }
  },
  conjunctionThought: {
    openBackgroundMargin: 10,
    height: 50,
    width: 50,
    padding: 0,
    handle: 6,
    //classLabel: null,
    underWordingShift: {
      x: 5,
      y: 5
    },
    tsMargin: 30,
    childScale: 0.75,
    editButtons: {
      radius: 6,
      shiftX: -2,
      shiftY: -5,
      get smallShiftX() {
        return -this.radius*2 - 0.5;
      }
    },
    fontScaling: x => 4/Math.log2(0.0025*x/3*2+1.18) + 1,  //przełożenie długości tekstu na rozmiar czcionki
    get ratio() {
      return this.height/this.width;
    },
    get defaultBorderLeftRight() {
      return this.width/2 + this.tsMargin;
    },
    get defaultBorderBottomTop() {
      return this.height/2 + this.tsMargin;
    },
    childrenAmountLabel: {
      shiftX: 1,
      shiftY: 1,
      width: 10,
      height: 7
    },
    corner: {
      scale: 0.8,
      background: {
        cx: -16,
        cy: -10,
        rx: 8,
        ry: 8
      },
      text: {
        get x() {
          return this._.background.cx - 5.2;
        },
        get y() {
          return this._.background.cy + 5;
        }
      }
    },
    visibilityIcon: {
      x: -35,
      y: 25,
      fontSize: 50
    }
  },
  //thoughtHeight: 50,
  //thoughtWidth: 100,
  thEditor: {
    scale: 2,
    height: 150,
    width: 150,
    x: 15,
    y: 15,
    thought: {
      x: 15,
      y: 25
    },
    classInputHeight: 14,
    closeBtn: {
      get x() {
        return this._.width - 1.5;
      },
      y: 1.5
    },
    addBtn: {
      x: 72.5,
      y: 80,
      width: 40,
      height: 15,
      text: {
        get x() {
          return this._.x + this._.width / 2;
        },
        get y() {
          return this._.y + this._.height / 2;
        }
      }
    },
  },
  tbsMargin: 40,
  selfRelation: {
    //scale: 0.4,
    xShift: 80,
    yShift: -30
  }
});

const gConst = arg => {
  var args = arg.split('.');
  var result = constants;
  args.forEach(a => {
    result = result[a];
  })
  return result;
};
if (Meteor.isClient) {
  window.gConst = gConst;
}
export default gConst;
