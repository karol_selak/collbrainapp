var translations = {
  'pl': {
    thEditor: {
      add: 'Dodaj',
      typeThoughtContentHere: 'treść',
      thClass: 'klasa',
      draft: 'wersja robocza',
      //publicThought: 'myśl publiczna',
      vis: {
        'public': 'myśl publiczna',
        'private': 'myśl prywatna',
        group: 'myśl grupowa'
      },
      commit: 'zatwierdź',
      cancel: 'anuluj',
      bilateralRelation: 'relacja symetryczna',
      addAsRelation: 'dodaj jako relację',
      addAsCommonThought: 'dodaj jako zwykłą myśl',
      commitThoughtToMakeItActive: 'zatwierdź myśl, aby stała się aktywna',// (nie będziesz mógł jej zmienić ani skasować)',
      saveChanges: 'zapisz zmiany',
      deleteThought: 'skasuj myśl',
      deleteModal: {
        title: 'Chcesz usunąć myśl zupełnie, czy tylko z tej przestrzeni?',
        content: 'Jeśli usuniesz myśl zupełnie, zniknie ona również z innych przestrzeni i nie będziesz mógł jej odzyskać.',
        options: {
          completely: 'Zupełnie',
          onlyFromThisSpace: 'Tylko z tej przestrzeni'
        }
      }
    },
    loginModal: {
      loginToYourAccount: 'Logowanie',
      collbrainDesctiption: '',
      name: 'login',
      password: 'hasło',
      login: 'zaloguj się',
      lostYourPassword: 'zapomniałeś hasła?',
      signInWithFacebook: 'zaloguj przez Facebooka',
      newUserRegister: 'zarejestruj się'
    },
    registerModal: {
      createNewAccount: 'Utwórz konto',
      name: 'nazwa użytkownika',
      password: 'hasło',
      repeatPassword: 'powtórz hasło',
      register: 'zarejestruj się',
      email: 'email',
      passwordsDoesntMatch: 'Podane hasła nie zgadzają się',
      wrongEmailAddress: 'Błędny adres email',
      passwordShouldBeAtLeast8CharactersLong: 'Hasło powinno mieć co najmniej 8 znaków',
      usernameIsRequired: 'Nazwa użytkownika jest wymagana'
    },
    sidePanel: {
      version: 'wersja',
      mission: 'misja',
      foundations: 'założenia',
      ideas: 'pomysły',
      bugs: 'bugi'
    }
  },
  'en': {
    thEditor: {
      add: 'Add',
      typeThoughtContentHere: 'content',
      thClass: 'class',
      draft: 'draft',
      //publicThought: 'public thought',
      visibility: 'visibility',
      vis: {
        'public': 'public thought',
        'private': 'private thought',
        group: 'group thought'
      },
      commit: 'commit',
      cancel: 'cancel',
      bilateralRelation: 'symmetric relation',
      addAsRelation: 'add as relation',
      addAsCommonThought: 'add as common thought',
      commitThoughtToMakeItActive: 'commit thought to make it active',// (you won\'t be able to change or delete it)',
      saveChanges: 'save changes',
      deleteThought: 'delete thought',
      deleteModal: {
        title: 'Do you want to remove it completely or only from this space?',
        content: 'If you remove thought completely, it will disappear also from other spaces and you will not be able to restore it.',
        options: {
          completely: 'Completely',
          onlyFromThisSpace: 'Only from this space'
        }
      }
    },
    loginModal: {
      loginToYourAccount: 'Login to Your Account',
      collbrainDesctiption: '',
      name: 'login',
      password: 'password',
      login: 'login',
      lostYourPassword: 'lost your password?',
      signInWithFacebook: 'sign in with Facebook',
      newUserRegister: 'new user register'
    },
    registerModal: {
      createNewAccount: 'Create new account',
      name: 'username',
      password: 'password',
      repeatPassword: 'repeat password',
      register: 'register',
      email: 'email',
      passwordsDoesntMatch: 'Given passwords doesn\'t match',
      wrongEmailAddress: 'Wrong email address',
      passwordShouldBeAtLeast8CharactersLong: 'Password should be at least 8 characters long',
      usernameIsRequired: 'Username is required'
    },
    sidePanel: {
      version: 'version',
      mission: 'mission',
      foundations: 'foundations',
      ideas: 'ideas',
      bugs: 'bugs'
    },
    ride: {

    }
  }
}
for (let i in translations) {
  i18n.map(i,translations[i]);
}
i18n.setDefaultLanguage('en');

//jeśli mamy zalogowanego usera, ustawiamy język z jego profilu, a jeśli się tego nie da zrobić, wykrywamy język na podstawie headerów:
if (Meteor.user() && Meteor.user().language) {
  i18n.setLanguage(Meteor.user().language);
} else {
  var langs = headers.get('accept-language').match(/[a-zA-z\-]{2,10}/g);
  for (let i in langs) {
    if (translations[langs[i]]) {
      i18n.setLanguage(langs[i]);
      break;
    }
  }
}


