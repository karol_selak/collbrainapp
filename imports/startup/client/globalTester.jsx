import { Meteor } from 'meteor/meteor';
import deepFreeze from 'deep-freeze';
import expect from 'expect';
window.Tester = {
/*  main() {
    for (var i in MOSES.getCraters()){
      var crater = MOSES.getCraters()[i];
      var c = MOSES.translate(crater);
      Tester.paintAPoint(c.x*gConst('childScale'),c.y*gConst('childScale'));
      Tester.paintAPoint((c.x+gConst('thoughtWidth')/2)*gConst('childScale'),(c.y+gConst('thoughtHeight')/2)*gConst('childScale'));
      Tester.paintAPoint((c.x-crater.width)*gConst('childScale'),(c.y-crater.height)*gConst('childScale'));
      Tester.paintAPoint((c.x-crater.width-gConst('thoughtWidth')/2)*gConst('childScale'),(c.y-crater.height-gConst('thoughtHeight')/2)*gConst('childScale'));
      Tester.paintAPoint((c.x-crater.width)*gConst('childScale'),(c.y)*gConst('childScale'));
      Tester.paintAPoint((c.x-crater.width-gConst('thoughtWidth')/2)*gConst('childScale'),(c.y+gConst('thoughtHeight')/2)*gConst('childScale'));
      Tester.paintAPoint(c.x*gConst('childScale'),(c.y-crater.height)*gConst('childScale'));
      Tester.paintAPoint((c.x+gConst('thoughtWidth')/2)*gConst('childScale'),(c.y-crater.height-gConst('thoughtHeight')/2)*gConst('childScale'));
    }

  },*/
//testy tlt i tltBack:
/*
m = new _littleMoses()
m.addCrater({x:0, y:0, height: 100, width: 100, id: 1})
m.addCrater({x:100, y:100, height: 100, width: 100, id: 2})
m.addCrater({x:-100, y:100, height: 100, width: 100, id: 3})
m.addCrater({x:100, y:-100, height: 100, width: 100, id: 4})
m.addCrater({x:-100, y:-100, height: 100, width: 100, id: 5})
m.addCrater({x:10, y:10, height: 120, width: 120, id: 6})
m.addCrater({x:-10, y:10, height: 120, width: 120, id: 7})
m.addCrater({x:10, y:-10, height: 120, width: 120, id: 8})
m.addCrater({x:-10, y:-10, height: 120, width: 120, id: 9})

arr = [];
for (let i = -1000; i<=1000; i+=5) {arr.push(i)};

for (let i in arr) {
  for (let j in arr) {
    var coords = {x: i*1, y: j*1};
    var tr = m.translate(coords);
    try {
      var coords2 = m.translateBack(tr);
    } catch (err) {
      throw {err, coords, tr, coords2}
    }    
    if (coords.x != Number.parseInt(coords2.x) || coords.y != Number.parseInt(coords2.y)) {
      throw {coords, tr, coords2}
    }
  }
}
*/

  main() {
    lm = new littleMoses();
    lm2 = new littleMoses();
    deepFreeze(lm);
    //lm.addCraterPure({x: 20, y: 25, width: 10, height: 30, id: 'xxy'});
    expect((()=>{
      lm.addCraterPure({x: 20, y: 25, width: 10, height: 30, id: 'xxy'});
      return 1;
    })()).toEqual(1);
  },
  //Kropek:
  paintAPoint(x,y) {
    var point = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    point.setAttribute('fill', 'red');
    point.setAttribute('cx', x);
    point.setAttribute('cy', y);
    point.setAttribute('r',2);
    $('svg').append(point);
  },
  pointsWire(coords_start, coords_end, translation, size, scale) {
    var x = coords_start.x,
      y = coords_start.y;
    for (;x <= coords_end.x; x += size) {
      for (;y <= coords_end.y; y += size) {
        var coords = translation({x: x, y: y});
        this.paintAPoint(coords.x*scale,coords.y*scale);
      }
      y = coords_start.y;
    }
  },

  loremIpsum: 'Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię stracił. Dziś piękność twą w drobne strączki białe dziwnie ozdabiał głowę, bo tak wedle dzisiejszej mody odsyłać konie porzucone same szczypiąc trawę ciągnęły powoli pod Twoją opiek ofiarowany, martwą podniosłem powiek i z wypukłym sklepienie całe wesoło, lecz nim po wielu kosztach i gdzie mieszkał, dzieckiem będąc, przed nauczycielem. Szczęściem, że pewnie miała wysmukłą, kształtną, pierś powabną suknię materyjalną, różową, jedwabną gors wycięty, kołnierzyk z drzew raz wraz skrzypi i pijąc obie Tadeusz przyglądał się jak światłość miesiąca. Nucąc chwyciła suknie, biegła do usług publicznych sposobił z nieba dochodziło mniej wielkie, mniej silnie, ale nigdzie nie nalewa szklanki, i lekka jak znawcy, ci znowu do kołtuna. Jeśli kto i zaraz mogłem pieszo dzielnie chodził tępy nie policzę. Opuszczali rodziców i trudno było widzieć wyżółkłych młokosów gadających przez okno, świecąca nagła, cicha i w podróży. Była to mówiąc, że po kryjomu kazał stoły z nim w wielkiej peruce, którą do kraju. Mowy starca krążyły we dworze jako swe znajome dawne. też co dzień postrzegam, jak po wielu kosztach i dalszych replik stronom dzisiaj nie mogę na spoczynek powraca. Już konie rżące lecą ze szkoły: więc choć młodzik, ale szerzej niż we brzozowym gaju stał patrząc, dumając wonnymi powiewami kwiatów oddychając oblicze aż kędy pieprz rośnie gdzie nie jest nauką łatwą ani małą. niełatwą, bo tak nazywano młodzieńca, który nosił Kościuszkowskie miano na pagórku niewielkim, we dnie świeciło całe zniszczone sekwestrami rządu bezładnością opieki, wyrokami sądu w każdej jest obora. Dozoru tego nigdy.',

  //Snajper SVG:
  svgSniper(x,y,if_all) {
    var vB_tmp = document.getElementById('svg').getAttribute('viewBox');
    document.getElementById('svg').setAttribute('viewBox', x+' '+y+' '+(x+10)+' '+(y+10));
    var result;
    if (if_all) {
      result = [];
      var elem = document.elementFromPoint(0,0);
      while (elem.tagName !== 'svg') {
        result.push(elem);
        elem.style.display = 'none';
        //zdejmujemy to co na wierzchu

        elem = document.elementFromPoint(0,0);
      }
      for (var i in result) {
        result[i].style.display = '';
        //z powrotem wkładamy to, co zdjęliśmy
      }
    } else {
      result = document.elementFromPoint(0,0);
    }
    document.getElementById('svg').setAttribute('viewBox', vB_tmp);
    return result;
  },
  testLogin() {
    Accounts.createUser({username: 'testowy', password: 'test'});
    Meteor.loginWithPassword('testowy','test');
  },
};
