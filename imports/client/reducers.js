import { combineReducers } from 'redux';
import update from 'react-addons-update';

const thoughtsReducer = (state = {}, action) => {
  /**/nlog('#: reducers nts', action.type);/**/ //loguje wszystkie akcje w aplikacji, akcje innych reducerów również
  /**/ntimer.start('ACTION');/**/


  switch (action.type) {
    case 'SET_PATHPARAMS_AND_REST':
      return action.thoughts;
    case 'SET_THOUGHTS_AND_REST':
      return action.thoughts;
    default:
      /**/ntimer.start('TRUE_ACTION');/**/      
      return state;
  }
};
const pathParamsReducer = (state = {}, action) => {
  switch (action.type) {
    case 'SET_PATHPARAMS_AND_REST':
      return action.pathParams;
    case 'OPEN_THOUGHT_IN_PATH':
      var th = state.mainThoughts[action.screenIdx];      
      if (th._id == action.tid || (th.alias && th.alias == action.alias)) {
        if (th.isClosed) {
          state = update(state, {mainThoughts: {[action.screenIdx]: {isClosed: {$set: false}}}})
        } else {
          return state; //nic się nie dzieje
        }
      } else {
        if (th.openScions) {
          state = update(state, {mainThoughts: {[action.screenIdx]: {openScions: {$push: [{_id: action.tid}]}}}});
        } else {
          state = update(state, {mainThoughts: {[action.screenIdx]: {openScions: {$set: [{_id: action.tid}]}}}});
        }
      }
      return state;
    case 'CLOSE_THOUGHT_IN_PATH':
      var th = state.mainThoughts[action.screenIdx];      
      if (th._id == action.tid || (th.alias && th.alias == action.alias)) { //jeśli zamykana myśl jest myślą główną
        if (th.isClosed) {
          return state; //nic się nie dzieje
        } else {
          state = update(state, {mainThoughts: {[action.screenIdx]: {isClosed: {$set: true}}}})
        }
      } else {
        state = update(state, {mainThoughts: {[action.screenIdx]: {openScions: {$apply: (arr)=>{
          var newArr = []          
          arr.forEach((el, idx)=>{
            if (el._id != action.tid && (el.alias != action.alias || !el.alias)) {
              newArr.push(el);
            }
          });
          return newArr;
        }}}}});
      }
      return state;
    case 'SET_MAIN_THOUGHT':
      return update(state, {mainThoughts: {[action.screenIdx]: {
        _id: {$set: action.tid},
        alias: {$set: action.alias},
        isClosed: {$set: false}
      }}});
    case 'CLOSE_ALL_CHILDREN':
      return update(state, {mainThoughts: {[action.screenIdx]: {
        openScions: {$set: []}
      }}});
    default:
      return state;
  }
};
const screensReducer = (state = [], action) => {
  switch (action.type) {
    case 'SET_PATHPARAMS_AND_REST':
      return action.screens;
    case 'SET_THOUGHTS_AND_REST':
      return action.screens;
    default:
      return state;
  }
};
const addingsReducer = (state = {editor: {}, tidsToLids: {}, modal: {}, highlighted: {}}, action) => {
  switch (action.type) {
    case 'HIGHLIGHT_THOUGHT':
      if (action.ifOpen) {
        return update(state, {highlighted: {openTid: {$set: action.tid}}});
      } else {
        return update(state, {highlighted: {closeTid: {$set: action.tid}}});
      }
    case 'LOWLIGHT_THOUGHT':
      return update(state, {highlighted: {$set: {}}});
    case 'HIGHLIGHT_CHILDREN':
      return update(state, {highlightedChildrenInScreen: {$set: action.screenIdx}});
    case 'LOWLIGHT_CHILDREN':
      return update(state, {highlightedChildrenInScreen: {$set: null}});
    case 'OPEN_EDITOR':
      return update(state, {editor: {
        isOpen: {$set: true},
        addingMethod: {$set: state.editor.addingMethod || 'common'},
        mode: {$set: action.mode || 'adding'},
        thoughtDraft: {$apply: draft => action.draft || draft}
      }});
    case 'HIDE_EDITOR':
      return update(state, {editor: {isHidden: {$set: true}}});
    case 'REVEAL_EDITOR':
      return update(state, {editor: {isHidden: {$set: false}}});
    case 'CLOSE_EDITOR':
      return update(state, {editor: {isOpen: {$set: false}, addThoughtMode: {$set: false}, thoughtDraft: {$set: action.draft}}});
    /*case 'TURN_ADD_THOUGHT_MODE_ON':
      return update(state, {editor: {addThoughtMode: {$set: true}, addingMethod: {$set: action.method || 'common'}}});
    case 'TURN_ADD_THOUGHT_MODE_OFF':
      return update(state, {editor: {addThoughtMode: {$set: false}}});*/
    case 'CHANGE_ADDING_METHOD':
      return update(state, {editor: {addingMethod: {$set: action.method}}});
    case 'SET_PATHPARAMS_AND_REST':
      return update(state, {tidsToLids: {$set: action.tidsToLids}});
    case 'SET_THOUGHTS_AND_REST':
      return update(state, {tidsToLids: {$set: action.tidsToLids}});
    case 'SET_MODAL':
      return update(state, {modal: {$set: {type: action.modalType, callback: action.callback, properties: action.properties}}});
    default:
      return state;
  }
};
/*
const profileReducer = (state = {}, action) => {
  switch (action.type) {
    case 'SET_LANGUAGE':
      return update(state, {language: {$set: action.language}});
    default:
      return state;
  }
}*/

export default combineReducers({
  pathParams: pathParamsReducer,
  thoughts: thoughtsReducer,
  screens: screensReducer,
  addings: addingsReducer,
  //profile: profileReducer
});
