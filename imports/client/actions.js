import { Meteor } from 'meteor/meteor';
import { loadThoughts, createScreens } from '../mods/Thought/client/ThoughtHelpers';
import { interpretPath } from './helpers/path';

const setPathParams = path => {
  return (dispatch, getState) => {
    ntimer.log('START', 'setPathParams1')
    var pathParams = interpretPath(path);

    Meteor.subscribe('lthoughts', pathParams, { //TODO prowizorka z subskrypcjami - potem to odkomentować
      onReady: ()=>{
        var {thoughts, tidsToLids} = loadThoughts(pathParams);
        var screens = createScreens(pathParams, thoughts, tidsToLids);
        ntimer.log('START', 'setPathParams2')
        nlog('#: special', pathParams, thoughts, tidsToLids, screens);
        dispatch({
          type: 'SET_PATHPARAMS_AND_REST',
          pathParams,
          thoughts,
          screens,
          tidsToLids
        });
      }
    });
  }
};
const setModal = (modalType, callback, properties) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'SET_MODAL',
      modalType,
      callback,
      properties
    })
  }
}

export {setPathParams, setModal}
