import React, { Component, PropTypes } from 'react';
import Point from 'point-geometry';

class Curve {
  constructor(str) {
    this.path = [];
    if (str) {
      var arr = str.split(' ');
      for (let i in arr) {
        if (arr[i] == '') {
          continue;
        }
        if (arr[i] == 'M' || arr[i] == 'L' || arr[i] == 'C') {
          this.path.push({type: arr[i], nodes: []});
        } else {
          var params = arr[i].split(',');
          var point = new Point(...params);
          this.path[this.path.length - 1].nodes.push(point);
        }
      }
    }
  }
  addSegment(segment) {
    this.path.push(segment);
  }
  concat(curve, sliceFirst) { //TODO przemyśleć, jak to zrobić, gdy na końcu będzie Z
    if (sliceFirst) {
      this.path = this.path.concat(curve.path.slice(1));
    } else {
      this.path = this.path.concat(curve.path);
      this.path[this.path.length - curve.path.length].type = 'L'; //zamiana M na L      
    }
  }
  reverse() {
    this.path = this.path.reverse();
    var newPath = [];
    var ifStart = true;
    var nowDrawn;
    var shiftVector = new Point(0,0);
    for (let i in this.path) {
      if (this.path[i].type == 'M') {
        if (nowDrawn) {
          nowDrawn.nodes.push(this.path[i].nodes[0]);
          newPath.push(nowDrawn);
        } else {
          continue; //jeśli jest pierwsze, jest zupełnie ignorowane, nie zmienia się nawet ifStart
        }
      } else if (this.path[i].type == 'L') {
        if (ifStart) {
          newPath.push({type: 'M', nodes: [this.path[i].nodes[0]]})
        } else {
          nowDrawn.nodes.push(this.path[i].nodes[0]);
          newPath.push(nowDrawn);
        }
        nowDrawn = {type: 'L', nodes: []};        
      } else if (this.path[i].type == 'C') {
        if (ifStart) {
          newPath.push({type: 'M', nodes: [this.path[i].nodes[2]]})
        } else {
          nowDrawn.nodes.push(this.path[i].nodes[2]);
          newPath.push(nowDrawn);
        }
        nowDrawn = {type: 'C', nodes: [this.path[i].nodes[1], this.path[i].nodes[0]]};
      } else {
        throw '# reverse function works only for paths made with M, L and C nodes'
      }
      if (ifStart) {
        ifStart = false;
      }
    }
    this.path = newPath;
  }
  getStartPoint() {
    return this.path[0].nodes[0]
  }
  getEndPoint() {
    var last = this.path[this.path.length - 1];
    if (last.nodes.length) {
      return last.nodes[last.nodes.length - 1];
    } else {
      var preLast = this.path[this.path.length - 2]
      return preLast.nodes[preLast.nodes.length - 1]
    }
  }
  renderPath() {
    var result = '';
    for (let i in this.path) {
      result += this.path[i].type + ' ';
      for (let j in this.path[i].nodes) {
        result += this.path[i].nodes[j].x + ',' + this.path[i].nodes[j].y + ' ';
      }
    }
    return result;
  }
}

//dla każdej publicznej metody klasy Point tworzona jest analogiczna metoda klasy Curve:

for (let func in Point.prototype) {
  if (func[0] != '_') { /*ignorujemy metody zdefiniowane jako prywatne (zgodnie z nieoficjalną
        konwencją nazwy metod prywatnych w języku JavaScript rozpoczynają się od znaku podkreślenia)*/
    Curve.prototype[func] = function() {
      //definicja danej metody:
      var result = Object.assign(new Curve(), this);
      //this oznacza obiekt krzywej, dla której wywoływana jest metoda
      //deklarujemy, iż metoda nie zmienia istniejącej krzywej, ale tworzy nową
      return Object.assign(result, { //zwracanemu obiektowi krzywej przypisujemy ścieżkę
        path: this.path.map((el) => { //nowa ścieżka jest zmodyfikowaną starą ścieżką
          var r = Object.assign({}, el) //tworzymy nowe elementy ścieżki na podstawie starych
          return Object.assign(r, {nodes: el.nodes.map((node) => {
            //nowym elementom przypisujemy nowe pola nodes, które są tworzone na bazie starych
            node = node.clone();
            return node[func](...arguments);
            /*dla każdego elementu node (będącego obiektem klasy Point) wywołujemy metodę określoną
            przez zmienną func z argumentami przekazanymi do definiowanej metody podczas jej wywołania*/
          })})
        })
      })
    }
  }
}

export default Curve;
