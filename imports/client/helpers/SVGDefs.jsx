import React, { Component } from 'react';

class SVGDefs extends Component {
  render() {
    var shadowSize = 0.7;
    var lampOffsetScale = 0.6;
    const shadow = (name, ox, oy, brightness, blur) => {
      ox *= shadowSize * lampOffsetScale;
      oy *= shadowSize * lampOffsetScale;
      blur *= shadowSize;
      return `<filter id=${name}  width="200%" height="200%">
      <feOffset result="offOut" in="SourceGraphic" dx=${ox} dy=${oy} />
      <feColorMatrix result = "matrixOut" in = "offOut" type = "matrix"
        values = "0 0 0 0 0
             0 0 0 0 0
             0 0 0 0 0
             0 0 0 ${brightness} 0" />
      <feGaussianBlur result="blurOut" in="matrixOut" stdDeviation=${blur} />
      <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
      </filter>`
    }
    const blur = (name, blurX, blurY) => {
      return `<filter id=${name}  width="200%" height="200%">
      <feGaussianBlur in="SourceGraphic" stdDeviation=${blurX + ',' +blurY} />
      </filter>`
    }
    const brightness = (name, brightness) => {
      return `<filter id=${name} >
        <feComponentTransfer><feFuncA type="linear" slope=${brightness} /></feComponentTransfer>
      </filter>`
    }
    const stripes = (name, size, color1, color2) => {
      return `<pattern id=${name}  x=${size/2} y=${size/2} width=${size} height=${size} patternUnits="userSpaceOnUse" >
        <rect width=${size} height=${size} fill=${color1} />
        <rect width=${size} height=${size/2} fill=${color2} />
      </pattern>`
    }
    var br = 0.4;
    var html = shadow('sh1', 1, 1, br, 1) + 
      shadow('sh1.5', 1.5, 1.5, br, 1.5) + 
      shadow('sh2', 2, 2, br, 2) + 
      shadow('sh3', 3, 3, br, 3) + 
      shadow('sh4', 4, 4, br, 4) + 
      shadow('sh6', 6, 6, br, 6) + 
      shadow('sh8', 8, 8, br, 8) + 
      blur('blur4', 4, 4) + 
      blur('blur6', 6, 6) + 
      blur('blur12', 12, 12) + 
      brightness('br0.5', 0.5) + 
      blur('blurX', 3, 0) + 
      blur('blurY', 0, 3) +
      stripes('draftStripes', 7, 'black', 'yellow')
    return <defs dangerouslySetInnerHTML={{__html: html}}>
    </defs>
  }
}
export default SVGDefs;
