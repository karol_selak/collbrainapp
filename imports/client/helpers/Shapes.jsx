import React, { Component, PropTypes } from 'react';
import Point from 'point-geometry';
import Curve from './Curve';

class Rect1 extends Component {
  render() {
    return <rect rx='1.5' ry='1.5' {...this.props} />;
  }
}
Rect1.propTypes = {
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
};


const getArrowHead = (gc) => {
  var C0_3 = new Point(0.5, -0.5);

  var L1 = C0_3.add(new Point(gc.arrowhead, 0));
  L1 = L1.rotate(-gc.outAngle);

  var C2_1 = L1.add(new Point(gc.round, 0).rotate(-gc.outAngle));
  var C2_3 = L1.add(new Point(1,0).rotate((Math.PI - gc.outAngle - gc.inAngle)/2));
  var C2_2 = C2_3.add(new Point(gc.round,0).rotate(-gc.inAngle))

  var L3 = C2_3.add(new Point(gc.arrowhead2, 0).rotate(Math.PI - gc.inAngle));

  //złożenie połówki grotu strzałki poprzez deklarację segmentów:
  var halfArrow = new Curve();
  halfArrow.addSegment({type: 'M', nodes: [C0_3]});
  halfArrow.addSegment({type: 'L', nodes: [L1]});
  halfArrow.addSegment({type: 'C', nodes: [C2_1, C2_2, C2_3]});
  halfArrow.addSegment({type: 'L', nodes: [L3]});

  //stworzenie obiektu całego grotu przy użyciu obiektu połówki:
  var arrowHead = halfArrow.multByPoint(new Point(1,-1)); //odbicie lustrzane połówki
  arrowHead.reverse(); //odwrócenie kierunku rysowania

  var C0_1 = arrowHead.getEndPoint().sub(new Point(gc.round, 0).rotate(gc.outAngle));
  var C0_2 = C0_1.multByPoint(new Point(1,-1));
  
  //złożenie grotu strzałki w całość:
  arrowHead.addSegment({type: 'C', nodes: [C0_1, C0_2, C0_3]});
  arrowHead.concat(halfArrow, true);
  
  return arrowHead; //TODO zrobić z tego singleton może, aby zmniejszyć ilość obliczeń
}

class Arrow1 extends Component {
  draw(params,scale=1) {
    var gc = {
      defaultScale: 1.4,
      outAngle: Math.PI/10,
      inAngle: Math.PI/3,
      arrowhead: 10,
      arrowhead2: 1.8,
      round: 0.65,
      extension: 10,
      distParam: 0.5
    }
    var p1 = params.crossPoint1;
    var p2 = params.crossPoint2;
    var p3 = params.pivot;
    var s1 = params.stick1;
    var s2 = params.stick2;
    scale *= gc.defaultScale;

    var arrow = new Curve();

    if (p3 && s1 && s2) { //strzałka wygięta:

      var slopeAngle1 = s2.angleTo(p2);
      var slopeAngle2 = s1.angleTo(s2);
		  var slopeAngle3 = p1.angleTo(s1);
      var arrowHead = getArrowHead(gc);
      arrowHead = arrowHead.mult(scale);
      if (params.ifSymmetric) {
        arrowHead2 = arrowHead.clone();
      }
      var thickness = -arrowHead.getEndPoint().y;
      arrowHead = arrowHead.rotate(slopeAngle1);
      arrowHead = arrowHead.add(p2);

      var dist2 = p2.dist(s2);

      var L0 = arrowHead.getEndPoint();
      arrow.addSegment({type: 'M', nodes: [L0]});

      var C1_1 = p2.add((new Point(dist2*gc.distParam, -thickness)).rotate(slopeAngle1))
      var shift = thickness*Math.tan((slopeAngle1-slopeAngle2)/2);

      var C1_2 = p2.add((new Point(dist2 - shift, -thickness)).rotate(slopeAngle1))
      var C1_3 = p3.add((new Point(0, -thickness)).rotate(slopeAngle2))
      arrow.addSegment({type: 'C', nodes: [C1_1, C1_2, C1_3]});

      var dist1 = p1.dist(s1)
      var S2_2 = p1.sub((new Point(dist1*gc.distParam, thickness)).rotate(slopeAngle3))
      
      if (params.ifSymmetric) {
        arrowHead2 = arrowHead2.rotate(slopeAngle3 + Math.PI);
        arrowHead2 = arrowHead2.add(p1);
        arrow.addSegment({type: 'S', nodes: [S2_2, arrowHead2.getStartPoint()]});
        arrow.concat(arrowHead2, true)
      } else {
        var S2_3 = p1.add((new Point(gc.extension, -thickness)).rotate(slopeAngle3))
        arrow.addSegment({type: 'S', nodes: [S2_2, S2_3]});
        var L3 = p1.add((new Point(gc.extension, thickness)).rotate(slopeAngle3))
        arrow.addSegment({type: 'L', nodes: [L3]});
      }

      var C4_1 = p1.add((new Point(-dist1*gc.distParam, thickness)).rotate(slopeAngle3))
      var shift2 = thickness*Math.tan((slopeAngle2-slopeAngle3)/2);
      var C4_2 = p1.add((new Point(-dist1 - shift2, thickness)).rotate(slopeAngle3))
      var C4_3 = p3.add((new Point(0, thickness)).rotate(slopeAngle2))

      var S5_3 = arrowHead.getStartPoint()
      var S5_2 = p2.add((new Point(dist2*gc.distParam, thickness)).rotate(slopeAngle1))

      arrow.addSegment({type: 'C', nodes: [C4_1, C4_2, C4_3]});
      arrow.addSegment({type: 'S', nodes: [S5_2, S5_3]});
      arrow.concat(arrowHead, true);
      
    } else { //strzałka prosta:

      var slopeAngle = p1.angleTo(p2);
      var arrowHead = getArrowHead(gc);
      var dist = p2.dist(p1);

      if (params.ifSymmetric) {
        arrow = arrowHead.add(new Point(-dist/scale, 0));
        arrow = arrow.mult(-1);
      } else {
        var L4 = new Point((dist + gc.extension)/scale, arrowHead.getEndPoint().y);
        var L5 = L4.multByPoint(new Point(1, -1));
        arrow.addSegment({type: 'M', nodes: [L4]});
        arrow.addSegment({type: 'L', nodes: [L5]});
      }

      arrow.concat(arrowHead);
      
      arrow = arrow.mult(scale);
      arrow = arrow.rotate(slopeAngle);
      arrow = arrow.add(p2);
    }
    arrow.addSegment({type: 'Z'});
    return arrow.renderPath();
  }
  render() {
    var d = this.draw(this.props.arrowParams, this.props.scale)
    var p = Object.assign({},this.props)
    delete p.arrowParams;
    return <path {...p} d={d} />
  }
}
Arrow1.propTypes = {
  arrowParams: PropTypes.object.isRequired
};
class Arrow2 extends Component {
  draw(length, thickness) {
    var p1 = new Point(-length/2, 0);
    var p2 = p1.add(new Point(thickness, thickness));
    var p3 = p2.add(new Point(0,-thickness/2));
    var p4 = new Point(length/2, thickness/2);
    var p5 = p4.multByPoint(new Point(1,-1));
    var p6 = p3.multByPoint(new Point(1,-1));
    var p7 = p2.multByPoint(new Point(1,-1));
    arrow = new Curve();
    arrow.addSegment({type: 'M', nodes: [p1]});
    arrow.addSegment({type: 'L', nodes: [p2]});
    arrow.addSegment({type: 'L', nodes: [p3]});
    arrow.addSegment({type: 'L', nodes: [p4]});
    arrow.addSegment({type: 'L', nodes: [p5]});
    arrow.addSegment({type: 'L', nodes: [p6]});
    arrow.addSegment({type: 'L', nodes: [p7]});
    return arrow.renderPath();
  }
  render() {
    var d = this.draw(this.props.length, this.props.thickness)
    var p = Object.assign({},this.props)
    delete p.length;
    delete p.thickness;
    return <path {...p} d={d} />
  }
}
Arrow2.propTypes = {
  length: PropTypes.number.isRequired,
  thickness: PropTypes.number.isRequired
};

class AndShield extends Component {
  render() {
    var p1 = new Point(1, -1);
    var p2 = new Point(2, 2);
    var p3 = new Point(-2, 2);
    var p4 = new Point(-1, -1); //TODO zaokrąglić rogi
    var shield = new Curve();
    shield.addSegment({type: 'M', nodes: [p1]});
    shield.addSegment({type: 'C', nodes: [p2,p3,p4]});
    shield = shield.mult(this.props.size);
    shield = shield.add(new Point(this.props.x, this.props.y));
    return <path {...this.props} d={shield.renderPath()}/>
  }
}

class Triangle extends Component { //TODO czy lepiej tego nie wywalić
  render() {
    var p = Object.assign({},this.props)
    var points = [p.p1, p.p2, p.p3];
    delete p.p1;
    delete p.p2;
    delete p.p3;
    return <Polygon points={points} {...p} />
  }
}
Triangle.propTypes = {
  p1: PropTypes.object.isRequired,
  p2: PropTypes.object.isRequired,
  p3: PropTypes.object.isRequired
};
class Polygon extends Component {
  render() {
    var polygon = new Curve();
    for (let i in this.props.points) {
      polygon.addSegment({type: (i*1 ? 'L' : 'M'), nodes: [this.props.points[i]]});
    }    
    polygon.addSegment({type: 'Z', nodes: []});
    var p = Object.assign({},this.props)
    delete p.points;
    return <path {...p} d={polygon.renderPath()} />
  }
}
Polygon.propTypes = {
  points: PropTypes.array.isRequired
};
/*class TriangleRound extends Component {
//TODO
//TODO może by zrobić w ogóle funkcję zaokrąglającą ostre narożniki ścieżek
  render() {
    var triangle = new Curve();
    var p1 = new Point(0,0);
    p1.add(this.props.p1);
    var p2 = new Point(0,0);
    p2.add(this.props.p2);
    var p3 = new Point(0,0);
    p3.add(this.props.p3);
    var v1 = 
    triangle.addSegment({type: 'M', nodes: [this.props.p1]});
    triangle.addSegment({type: 'L', nodes: [this.props.p2]});
    triangle.addSegment({type: 'L', nodes: [this.props.p3]});
    triangle.addSegment({type: 'Z', nodes: []});
    var p = Object.assign({},this.props)
    delete p.p1;
    delete p.p2;
    delete p.p3;
    return <path {...p} d={triangle.renderPath()} />
  }
}
TriangleRound.propTypes = {
  p1: PropTypes.object.isRequired,
  p2: PropTypes.object.isRequired,
  p3: PropTypes.object.isRequired
};*/
/**/window.Curve = Curve;/**/
export { Rect1, Arrow1, Arrow2, Triangle, Polygon, AndShield };
