const interpretPath = path => {
  var pathParams = {mainThoughts:[]};
  if (!path) {
    path = 'heart';
  }
  path.split(';').map(subPath=>{
    var mainThought = {};    
    subPath.split('+').map((p, i)=>{    
      if (i == 0) {
        if (!/\W/.test(p)) { 
          mainThought.alias = p;
        } else if (p.slice(0,1) == '@' && !/\W/.test(p.slice(1))) {
          mainThought._id = p.slice(1);
        } else {
          throw '# Wrong character in thought name: '+p;
        }
      } else if (p.slice(0,5) == 'open:') {
        mainThought.openScions = p.slice(5).split(',').map(pp=>{
          if (!/\W/.test(pp)) { 
            return {alias: pp};
          } else if (pp.slice(0,1) == '@' && !/\W/.test(pp.slice(1))) {
            return {_id: pp.slice(1)};
          } else {
            throw '# Wrong character in thought name: '+pp;
          }
        })
      } else if (p == 'isClosed') {
        mainThought.isClosed = true;
      } else {
        throw '# Wrong subpath: '+p;
      }
    });
    pathParams.mainThoughts.push(mainThought);
  })
  /**/nlog('#: actions', 'interpretPath(), pathParams:');/**/
  /**/nlog('#: actions nts', pathParams);/**/
  return pathParams;
};

const generatePath = pathParams => {
  var path = '';
  pathParams.mainThoughts.forEach((el, idx) => {
    if (el.alias) {
      path += el.alias;
    } else {
      path += '@' + el._id;
    }
    if (el.isClosed) {
      path += '+isClosed';    
    }
    if (el.openScions && el.openScions.length) {
      path += '+open:';
      el.openScions.forEach((scion, sidx) => {
        if (scion.alias) {
          path += scion.alias;
        } else {
          path += '@' + scion._id;
        }
        if (sidx < el.openScions.length - 1) {
          path += ',';
        }
      });    
    }
    if (idx < pathParams.mainThoughts.length - 1) {
      path += ';';
    }
  });
  return path;
}

export {interpretPath, generatePath};
