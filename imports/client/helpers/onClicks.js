const onClicks = (event, onClick, onDoubleClick, timeout, parentObject) => {
  //TODO pomysł optymalizacyjny do przemyślenia: czy byłby sens uzależnaić timeout od tego, ile czasu trwało pierwsze kliknięcie (szybsi ludzie klikają szybciej)
  if (!parentObject) {
    parentObject = this; //obiekt, którego będziemy używać do przechowywania zmiennej ifClickWaiting
  }
  if (parentObject.ifClickWaiting) { //przypadek z podwójnym kliknięciem
    delete parentObject.ifClickWaiting;
    onDoubleClick(event);
  } else {
    this.ifClickWaiting = true;
    Meteor.setTimeout(()=>{
      if (parentObject.ifClickWaiting) { //jeśli nie było podwójnego, realizujemy pojedyncze
        onClick(event);
        delete parentObject.ifClickWaiting;
      }
    },timeout);
  }
}

export {onClicks}
