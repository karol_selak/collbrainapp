import { priorityQueue } from './purecraft';

//mały Mojżesz - metody zewnętrzne:
//funkcja translacji na zadanej macierzy:
const tlt = function(e, matrix) {
  e *= 1;
  var vix = matrix.virtualInsert({position: e});
  var s1 = matrix.get(vix - 1) ? Math.sign(matrix.get(vix - 1).position) : -1;
  var s2 = matrix.get(vix) ? Math.sign(matrix.get(vix).position) : 1;
  var result;
  if(s1 === undefined && s2 === -1 || s1 + s2 === -2) { //gdy jesteśmy na ujemnych
    result = e + matrix.get(vix).shift;
  } else if (s2 === undefined && s1 === 1 || s1 + s2 >= 1) { //gdy jesteśmy na dodatnich (zero jest jakby dodatnie)
    result = e + matrix.get(vix - 1).shift;
  } else { //gdy jesteśmy na zerze (lub pomiędzy zerem a pierwszą ujemną)
    result = e;
  }
  return result;
};
const tltBack = function(e, matrix) {
  e *= 1;
  if (e < 0) {
    var oldC = {position: -Infinity, shift: 0};
    for (var i=0; ; i++) {
      var c = matrix.get(i);
      if (!c || c.position >= 0) {
        return {e};
      } else if (e < c.position + c.shift) {
        if (e < oldC.position + c.shift) {
          return {e: oldC.position, id: oldC.id};
        }
        return {e: e - c.shift};
      }
      oldC = c;
    }
  } else {
    var oldC = {position: Infinity, shift: 0};
    for (var i = matrix.getLength() - 1; ; i--) {
      var c = matrix.get(i);
      if (!c || c.position < 0) {
        return {e};
      } else if (e >= c.position + c.shift) {
        if (e >= oldC.position + c.shift) {
          return {e: oldC.position, id: oldC.id};
        }
        return {e: e - c.shift};
      }
      oldC = c;
    }
  }
}
const insertToMatrixPure = function(position, shift, matrix, id, craters, axis) { //axis = 'x' lub 'y'
  if (!position || !shift || !matrix || !id || !craters || !axis) {
    throw '# wrong arguments in insertToMatrixPure in littleMoses in purecraft: '+JSON.stringify(arguments);
  }
  var {idx, result: newMatrix} = matrix.insertPure({position: position, shift: 0, id: id},true);
  var s1 = newMatrix.get(idx - 1) ? Math.sign(newMatrix.get(idx - 1).position) : -1,
    s2 = newMatrix.get(idx + 1) ? Math.sign(newMatrix.get(idx + 1).position) : 1,
    foreignShift = 0;
  if(s1 === undefined && s2 === -1 || s1 + s2 === -2) { //gdy jesteśmy na ujemnych
    foreignShift += newMatrix.get(idx + 1) ? newMatrix.get(idx + 1).shift : 0;
  } else if (s2 === undefined && s1 === 1 || s1 + s2 >= 1) { //gdy jesteśmy na dodatnich
    foreignShift += newMatrix.get(idx - 1) ? newMatrix.get(idx - 1).shift : 0;
  } //na zerze foreignShift zostaje zerem

  //zmiana shiftów naszego krateru:
  if (foreignShift) {
    var newCraters = update(craters, {[id]: {[axis]: {$apply: e => e + foreignShift}}});
  } else {
    var newCraters = craters;
  }
  newMatrix = newMatrix.updateIdxPure(idx,{shift: foreignShift + shift});
  //zmiana shiftów sąsiadów - funkcja:
  var shiftNeighbour = idx => {
    var el = newMatrix.get(idx);
    newMatrix = newMatrix.updateIdxPure(idx, {shift: el.shift + shift});
    newCraters = update(newCraters, {[el.id]: {[axis]: {$apply: e => e+shift}}});
  };
  if (position < 0) { //sąsiedzi ujemni
    for (var i = idx-1; i >= 0; i--) {
      shiftNeighbour(i);
    }
  } else { //sąsiedzi nieujemni
    for (var i = idx+1; i < newMatrix.getLength(); i++) {
      shiftNeighbour(i);
    }
  }
  return {newMatrix, newCraters};
};

const insertToMatrix = (position, shift, matrix, id, craters, axis, msg) => {
  var idx = matrix.insert({position: position, shift: 0, id: id}, msg);

  var s1 = matrix.get(idx - 1) ? Math.sign(matrix.get(idx - 1).position) : -1,
    s2 = matrix.get(idx + 1) ? Math.sign(matrix.get(idx + 1).position) : 1,
    foreignShift = 0;
  if(s1 === undefined && s2 === -1 || s1 + s2 === -2) { //gdy jesteśmy na ujemnych
    foreignShift += matrix.get(idx + 1) ? matrix.get(idx + 1).shift : 0;
  } else if (s2 === undefined && s1 === 1 || s1 + s2 >= 1) { //gdy jesteśmy na dodatnich
    foreignShift += matrix.get(idx - 1) ? matrix.get(idx - 1).shift : 0;
  } //na zerze foreignShift zostaje zerem

  //zmiana shiftów naszego krateru:
  if (foreignShift) {
    craters[id][axis] += foreignShift;
  }
  matrix.updateIdx(idx,{shift: foreignShift + shift}, msg);

  //zmiana shiftów sąsiadów - funkcja:
  var shiftNeighbour = function(idx) {
    var el = matrix.get(idx);
    matrix.updateIdx(idx, {shift: el.shift + shift}, msg);
    var cr = craters[el.id];
    cr[axis] += shift;
  };
  if (position < 0) { //sąsiedzi ujemni
    for (var i = idx-1; i >= 0; i--) {
      shiftNeighbour(i);
    }
  } else { //sąsiedzi nieujemni
    for (var i = idx+1; i < matrix.getLength(); i++) {
      shiftNeighbour(i);
    }
  }
};

const removeFromMatrixPure = function(matrix, id, craters, axis) {
  var idx = matrix.getArray().findIndex(function(e){return e.id === id;});
  var shift, position;
  var newMatrix = matrix;
  var newCraters = craters;
  if (axis === 'x') {
    shift = craters[id].width;
    position = craters[id].x;
    if (position < 0) {
      shift = -shift;
    }
  } else if (axis === 'y') {
    shift = craters[id].height;
    position = craters[id].y;
    if (position < 0) {
      shift = -shift;
    }
  } else {
    throw new Meteor.Error('# Wrong axis!');  
  }
  //zmiana shiftów sąsiadów - funkcja:
  var shiftNeighbour = idx => {
    var el = newMatrix.get(idx);
    newMatrix = newMatrix.updateIdxPure(idx, {shift: el.shift - shift});
    newCraters = update(newCraters, {[el.id]: {[axis]: {$apply: e => e-shift}}});
  };
  if (position < 0) { //sąsiedzi ujemni
    for (var i = idx-1; i >= 0; i--) {
      shiftNeighbour(i);
    }
  } else { //sąsiedzi nieujemni
    for (var i = idx+1; i < matrix.getLength(); i++) {
      shiftNeighbour(i);
    }
  }
  newMatrix = newMatrix.removeIdxPure(idx);
  return {newMatrix, newCraters, shift};
};

//deklaracja małego Mojżesza:
class littleMoses {
  constructor(xMatrix, yMatrix, craters) {
    if (xMatrix instanceof priorityQueue && yMatrix instanceof priorityQueue && craters) {
      this._xMatrix = xMatrix;
      this._yMatrix = yMatrix;
      this._craters = craters;
    } else {
      this._xMatrix = new priorityQueue([],e => e.position);
      this._yMatrix = new priorityQueue([],e => e.position);
      this._craters = {};
    }
  };
  getCraters() {
    var result = {};
    for (var i in this._craters) {
      result[i] = this.getCrater(i);
    }
    return result;
  };
  getCrater(id) {
    return this._craters[id] && JSON.parse(JSON.stringify(this._craters[id]));
  };
  addCraterPure(crater) {
    ///crater: {x, y, width, height, id}
    if (this._craters[crater.id]) {
      throw "# Crater with id "+crater.id+" already exist!";
    }
    var newCraters = update(this._craters, {$merge: {[crater.id]: crater}});
    var shift_x = crater.x < 0 ? -crater.width : crater.width;
    var {newMatrix: newXMatrix, newCraters} = insertToMatrixPure(crater.x,
        shift_x, this._xMatrix, crater.id, newCraters, 'x');
    var shift_y = crater.y < 0 ? -crater.height : crater.height;
    var {newMatrix: newYMatrix, newCraters} = insertToMatrixPure(crater.y,
        shift_y, this._yMatrix, crater.id, newCraters, 'y');
    return new littleMoses(newXMatrix, newYMatrix, newCraters);
  };
  addCrater(crater, msg) {
    //crater: {x, y, width, height, id}
    if (this._craters[crater.id]) {
      throw "# Crater with id "+crater.id+" already exist!";
    }
    this._craters[crater.id] = crater;
    var shift_x = crater.x < 0 ? -crater.width : crater.width;
    insertToMatrix(crater.x, shift_x, this._xMatrix, crater.id, this._craters, 'x', msg);
    var shift_y = crater.y < 0 ? -crater.height : crater.height;
    insertToMatrix(crater.y, shift_y, this._yMatrix, crater.id, this._craters, 'y', msg);
  };
  removeCraterPure(id) {
    var {newMatrix: newXMatrix, newCraters, shift: x} = removeFromMatrixPure(this._xMatrix, id, this._craters, 'x');
    var {newMatrix: newYMatrix, newCraters, shift: y} = removeFromMatrixPure(this._yMatrix, id, newCraters, 'y');
    delete newCraters[id];
    if (x < 0) {
      x = -x;
    }
    if (y < 0) {
      y = -y;
    }
    return {x, y, newMoses: new littleMoses(newXMatrix, newYMatrix, newCraters)};
    //x i y mówią, o ile skurczyła się cała przestrzeń po usunięciu krateru
  };
  updateCraterPure(crater) {
    //TODO zoptymalizować to
    if (!this._craters[crater.id]) {
      throw new Meteor.Error("# There's no crater with id "+crater.id+"!");
    }
    var {newMatrix: newXMatrix, newCraters, shift: x} = removeFromMatrixPure(this._xMatrix, crater.id, this._craters, 'x');
    var {newMatrix: newYMatrix, newCraters, shift: y} = removeFromMatrixPure(this._yMatrix, crater.id, newCraters, 'y');
    if (x < 0) {
      x = -x;
    }
    if (y < 0) {
      y = -y;
    }
    newCraters = update(newCraters, {[crater.id]: {$merge: crater}});
    var c = newCraters[crater.id];
    if (c.x < 0) {
      var {newMatrix: newXMatrix, newCraters} = insertToMatrixPure(c.x, -c.width, newXMatrix, crater.id, newCraters, 'x');
    } else {
      var {newMatrix: newXMatrix, newCraters} = insertToMatrixPure(c.x, c.width, newXMatrix, crater.id, newCraters, 'x');
    }
    if (c.y < 0) {
      var {newMatrix: newYMatrix, newCraters} = insertToMatrixPure(c.y, -c.height, newYMatrix, crater.id, newCraters, 'y');
    } else {
      var {newMatrix: newYMatrix, newCraters} = insertToMatrixPure(c.y, c.height, newYMatrix, crater.id, newCraters, 'y');
    }
    return {x: c.width - x, y: c.height - y, newMoses: new littleMoses(newXMatrix, newYMatrix, newCraters)};
    //x i y mówią, jak zmieniła się cała przestrzeń po zmianie krateru
  };
  //zwiększanie szerokości lub wysokości krateru:
  widenCraterPure(id, shift) { //shift = {x,y}
    var newCraters = update(this._craters,{[id]: {width: {$apply: w => w + shift.x}, height: {$apply: h => h + shift.y}}});
    const updateMatrix = (matrix, axis) => {
      var newMatrix = matrix;
      matrix.getArray().find((e, idx) => {
        if (e.id === id) {
          var if_first = true;
          if (e.position < 0) {
            while (idx >= 0) {
              newMatrix = newMatrix.updateIdxPure(idx, {shift: newMatrix.get(idx).shift - shift[axis]});
              if (if_first) {
                if_first = false;
              } else {
                newCraters = update(newCraters, {[newMatrix.get(idx).id]: {[axis]: {$apply: a => a + shift[axis]}}});
                //tzn. craters[id].x += shift.x;
                //   craters[id].y += shift.y;
              }
              idx--;
            }
          } else {
            while (idx < newMatrix.getLength()) {
              newMatrix = newMatrix.updateIdxPure(idx, {shift: newMatrix.get(idx).shift + shift[axis]});
              if (if_first) {
                if_first = false;
              } else {
                newCraters = update(newCraters, {[newMatrix.get(idx).id]: {[axis]: {$apply: a => a + shift[axis]}}});
                //j.w.
              }
              idx++;
            }
          }

        }
      });
      return {newMatrix, newCraters}
    };
    var {newMatrix: newXMatrix, newCraters} = updateMatrix(this._xMatrix, 'x');
    var {newMatrix: newYMatrix, newCraters} = updateMatrix(this._yMatrix, 'y');
    return new littleMoses(newXMatrix, newYMatrix, newCraters);
  };
  widenCrater(id, shift, msg) { //shift = {x,y}
    /**/if (msg != 'mutate it') {
      console.warn('[purecraft] mutating widenCrater function called')
    }/**/
    this._craters[id].width += shift.x;
    this._craters[id].height += shift.y;
    const updateMatrix = (matrix, axis) => {
      matrix.getArray().find((e, idx) => {
        if (e.id === id) {
          var if_first = true;
          if (e.position < 0) {
            while (idx >= 0) {
              matrix.updateIdx(idx, {shift: matrix.get(idx).shift - shift[axis]}, msg);
              if (if_first) {
                if_first = false;
              } else {
                this._craters[matrix.get(idx).id][axis] -= shift[axis];
                //tzn. craters[id].x += shift.x;
                //     craters[id].y += shift.y;
              }
              idx--;
            }
          } else {
            while (idx < matrix.getLength()) {
              matrix.updateIdx(idx, {shift: matrix.get(idx).shift + shift[axis]}, msg);
              if (if_first) {
                if_first = false;
              } else {
                this._craters[matrix.get(idx).id][axis] += shift[axis];
                //j.w.
              }
              idx++;
            }
          }
        }
      });
    };
    updateMatrix(this._xMatrix, 'x');
    updateMatrix(this._yMatrix, 'y');
  };
  getXMatrix() {
    return this._xMatrix.getArray();
  };
  getYMatrix() {
    return this._yMatrix.getArray();
  };
  //translacja współrzędnych {x,y}
  translate(coords) {
    if (coords.width && coords.height) { //opcja gdy wstawiamy width i height w celu transformacji
      var x = tlt(coords.x, this._xMatrix),
        y = tlt(coords.y, this._yMatrix), //x i y to lewy górny narożnik
        xp = tlt(coords.x + coords.width, this._xMatrix),
        yp = tlt(coords.y + coords.height, this._yMatrix);
      return {
        x: x,
        y: y,
        width: xp - x,
        height: yp - y
      };
    } else { //opcja z samym x i y
      return {
        x: tlt(coords.x, this._xMatrix),
        y: tlt(coords.y, this._yMatrix)
      };
    }
  };
  //translacja wsteczna współrzędnych {x,y}
  translateBack(coords) {
    if (coords.width && coords.height) { //opcja gdy wstawiamy width i height w celu transformacji
      var x = tltBack(coords.x, this._xMatrix).e,
        y = tltBack(coords.y, this._yMatrix).e, //x i y to lewy górny narożnik
        xp = tltBack(coords.x + coords.width, this._xMatrix).e,
        yp = tltBack(coords.y + coords.height, this._yMatrix).e;
      return {
        x: x,
        y: y,
        width: xp - x,
        height: yp - y
      };
    } else { //opcja z samym x i y
      var {e: x, id: idx} = tltBack(coords.x, this._xMatrix);
      var {e: y, id: idy} = tltBack(coords.y, this._yMatrix);
      return {
        x,
        y,
        id: idx == idy ? idx : null
      };
    }
  }
}
window._littleMoses = littleMoses
export { littleMoses };
