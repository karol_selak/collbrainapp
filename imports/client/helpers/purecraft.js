import update from 'react-addons-update';
import Point from 'point-geometry';

//1000 -> 1k:
const kkk = (arg) => {
  var k = 0;
  while(arg>1000) {
    arg = (arg - arg % 1000) / 1000;
    k++;
  }
  for (var i = 0; i < k; i++) {
    arg += 'k';
  }
  return arg;
}

//obliczanie przelicznika pomiędzy rozdzielczością ekranu a rozdzielczością przestrzeni SVG:
const calculateViewBoxScale = (viewBox, windowWidth, windowHeight) => {
  return Math.min(windowHeight / viewBox.height, windowWidth / viewBox.width);
}
const windowToSVG = (viewBox, windowWidth, windowHeight, x, y) => {
  var ratioH = windowHeight / viewBox.height;
  var ratioW = windowWidth / viewBox.width;
  if (ratioH < ratioW) { //sytuacja proporcji wg. h
    return {
      x: x / ratioH + viewBox.x - (windowWidth / ratioH - viewBox.width ) / 2,
      y: y / ratioH + viewBox.y
    }
  } else { //sytuacja proporcji wg. w
    return {
      x: x / ratioW + viewBox.x,
      y: y / ratioW + viewBox.y - (windowHeight / ratioW - viewBox.height) / 2
    }
  }
}
//uzupełniacz zagnieżdżonych obiektów o odwołania do rodziców:
const backlooker = (obj) => {
  for (key in obj) {
    if (obj[key]._) {
      break;      
    }
    if (obj[key] instanceof Object) {
      obj[key]._ = obj;
      backlooker(obj[key])
    }
  }
  return obj;
}


//kolejka priorytetowa - metody zewnętrzne:
const insertToSorted = function(element, array, sortingValueFunction, ifVirtual) {
  var if_empty = !array.length;
  //wstawianie elementu do posortowanej tablicy (quicksort)
  function locationOf(element, array, start, end) {
    if (element < array[0]){
      return -1;
    }
    start = start || 0;
    end = end || array.length;
    var pivot = parseInt(start + (end - start) / 2, 10);
    if (end-start <= 1 || array[pivot] === element) {
      return pivot;
    }
    if (array[pivot] < element) {
      return locationOf(element, array, pivot, end);
    } else {
      return locationOf(element, array, start, pivot);
    }
  }
  elVal = sortingValueFunction(element);
  if (elVal*0 !== 0) {
    throw '# sorting value of element is not a number!';
  }
  var loc = locationOf(elVal, array.map(sortingValueFunction)) + 1;
  if (!ifVirtual) {
    array.splice(loc, 0, element);
  }
  if (if_empty) {
    return 0;
  } else {
    return loc;
  }
}
//kolejka priorytetowa - deklaracja klasy:
class priorityQueue {
  constructor(__arr, __sortingValueFunction) {
    this._arr = __arr || [];
    this._sortingValueFunction = __sortingValueFunction || (e => e);
    this._arr.sort((a,b) => {
      return this._sortingValueFunction(a) - this._sortingValueFunction(b);
    });
  }
  getArray() {
    //zwraca kopię tablicy:
    return JSON.parse(JSON.stringify(this._arr));
  }
  get(idx) {
    //zwraca kopię elementu:
    return this._arr[idx] ? JSON.parse(JSON.stringify(this._arr[idx])) : undefined;
  }
  insert(e, msg) {
    /**/if (msg != 'mutate it') {
      console.warn('[purecraft] mutating insert function called')
    }/**/
    return insertToSorted(e,this._arr,this._sortingValueFunction);
  }
  insertPure(e, if_index_returned) {
    var idx = insertToSorted(e,this._arr,this._sortingValueFunction,true);
    var result = new priorityQueue([
      ...this._arr.slice(0, idx),
      e,
      ...this._arr.slice(idx)    
    ], this._sortingValueFunction);
    if (if_index_returned) {
      return {result, idx}    
    } else {
      return result;    
    }
  }
  virtualInsert(e) {
    //operacja mająca na celu sprawdzenie, na której pozycji w kolejce znalazłaby się dana wartość,
    //gdybyśmy ją tam rzeczywiście wsadzili (ale naprawdę nie wsadzamy)
    return insertToSorted(e,this._arr,this._sortingValueFunction,true);
  }
  updateIdx(idx, elem, msg) {
    /**/if (msg != 'mutate it') {
      console.warn('[purecraft] mutating updateIdx function called')
    }/**/
    for (var attr in elem) {
      this._arr[idx][attr] = elem[attr];
    }
    this._arr.sort((a,b) => {
      return this._sortingValueFunction(a) - this._sortingValueFunction(b);
    });
    return this.getArray();
  }
  updateIdxPure(idx, elem) {
    return new priorityQueue(update(this._arr, {[idx]: {$merge: elem}}), this._sortingValueFunction);
  }
  removeIdx(idx, msg) {
    /**/if (msg != 'mutate it') {
      console.warn('[purecraft] mutating removeIdx function called')
    }/**/
    return this._arr.splice(idx,1)[0];
  }
  removeIdxPure(idx) {
    return new priorityQueue(update(this._arr, {$splice: [[idx,1]]}),this._sortingValueFunction);  
  }
  /*sort() {
    this._arr.sort((a,b) => {
      return this._sortingValueFunction(a) - this._sortingValueFunction(b);
    });
  }*/
  getLength() {
    return this._arr.length;
  }
}

const getRectCenter = (r) => {
  return new Point(r.x + r.width/2, r.y + r.height/2);
}
/*const ifLineSegmentsIntersects = (AB, CD) {
  
}*/
const getArrowParams = (r1, r2, pivot) => { //pivot: {x,y} rx: {x,y,height,width} lub {center: {x,y},height,width}
  if (!r1.center && !r2.center) {
    r1.center = getRectCenter(r1);
    r2.center = getRectCenter(r2);  
  } else {
    if (!(r1.center instanceof Point)) {
      r1.center = new Point(r1.center.x, r1.center.y);
    }
    if (!(r2.center instanceof Point)) {
      r2.center = new Point(r2.center.x, r2.center.y);
    }
  }
  if (pivot) {
    //TODO zrobić liczenie sticków niesymetrycznych, dzięki czemu strzałka nie będzie się dziwnie zachowywać przy niesymetrycznych konfiguracjach (np. []---------------------[]-->[])
    if (!(pivot instanceof Point)) {
      pivot = new Point(pivot.x, pivot.y);
    }
    var diff1 = pivot.sub(r1.center); //wektor prowadzący z jednego punktu do drugiego
    var diff2 = r2.center.sub(pivot); //j.w.
    var stickLength = (diff1.mag() + diff2.mag())*0.2;
    var alpha = pivot.angleTo(r2.center);
    var beta = pivot.angleTo(r1.center);
    var stickAngle = (beta - alpha - Math.PI) / 2;
    if (beta > alpha) {
      stickAngle += Math.PI;  
    }
    var stickVector = (new Point(stickLength/2, 0)).rotate(stickAngle + alpha);
    var stick1 = pivot.sub(stickVector);
    var stick2 = pivot.add(stickVector);
    var diff1s = stick1.sub(r1.center); //wektor prowadzący z jednego punktu do drugiego
    var diff2s = r2.center.sub(stick2); //j.w.
    //Tester.paintAPoint(stick1.x,stick1.y);
    //Tester.paintAPoint(stick2.x,stick2.y);

    //wyliczenie punktów przecięcia prostej z krawędziami prostokątów:
    var x = Math.abs(diff1s.x);
    var y = Math.abs(diff1s.y);
    if (y / x >= r1.height / r1.width) {
      var crossPoint1 = r1.center.add(diff1s.mult(r1.height/2/y));
    } else {
      var crossPoint1 = r1.center.add(diff1s.mult(r1.width/2/x));
    }
    var x = Math.abs(diff2s.x);
    var y = Math.abs(diff2s.y)
    if (y / x >= r2.height / r2.width) {
      var crossPoint2 = r2.center.add(diff2s.mult(-r2.height/2/y));
    } else {
      var crossPoint2 = r2.center.add(diff2s.mult(-r2.width/2/x));
    }
    return {crossPoint1, crossPoint2, pivot, stick1, stick2}
  } else {
    var diff = r2.center.sub(r1.center); //wektor prowadzący z jednego punktu do drugiego
    //wyliczenie punktów przecięcia prostej z krawędziami prostokątów:
    var x = Math.abs(diff.x);
    var y = Math.abs(diff.y)
    if (y / x >= r1.height / r1.width) {
      var crossPoint1 = r1.center.add(diff.mult(r1.height/2/y));
    } else {
      var crossPoint1 = r1.center.add(diff.mult(r1.width/2/x));
    }
    if (y / x >= r2.height / r2.width) {
      var crossPoint2 = r2.center.add(diff.mult(-r2.height/2/y));
    } else {
      var crossPoint2 = r2.center.add(diff.mult(-r2.width/2/x));
    }
    return {crossPoint1, crossPoint2}
  }
}
//mierzenie stringów w pikselach:
//TODO pomyśleć, czy nie rozszerzyć tej metody o przykład z HTML5 z linka, tak aby domyślnie działała szybsza, a druga była rezerwowa (http://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/5047712#5047712)
const getStrSize = (str, f = 'inherit') => {
  var o = $('<div>' + str + '</div>')
    .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f})
    .appendTo($('body')),
  w = o.width();
  h = o.height();
  o.remove();
  return {w,h};
}
export {kkk, priorityQueue, calculateViewBoxScale, windowToSVG, backlooker, getArrowParams, getStrSize}
