import { Tracker } from 'meteor/tracker';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

import reducers from './reducers';
import {Thoughts, Locations, Wordings} from '../collections';
import { setThoughts } from '../mods/Thought/client/ThoughtActions';



export default () => {
  const store = createStore(reducers, applyMiddleware(thunk,routerMiddleware(browserHistory)));
  Tracker.autorun(() => {
    /**/nlog('#: createStore std', 'Tracker.autorun(()=>{})')/**/
    //aktywacja trackera: //TODO czy nie da się tego zoptymalizować
    Thoughts.find().fetch();
    Wordings.find().fetch();
    Locations.find().fetch();
    setThoughts()(store.dispatch, store.getState);
  });
  return store;
};
