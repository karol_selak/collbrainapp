import React, { Component, PropTypes } from 'react';

class Modal extends Component {
  componentDidMount() {
    this.$m = $(this.refs.modal);
    this.$m.modal('show');
    this.$m.on('hide.bs.modal', this.runCallbackAndSetEmptyModal.bind(this));
  }
  componentDidUpdate() {
    this.$m.unbind('hide.bs.modal');
    this.$m.on('hide.bs.modal', this.runCallbackAndSetEmptyModal.bind(this));
    //if (!this.$m.is(':visible')) { //TODO czy ten warunek jednak nie jest konieczny
    this.$m.modal('show');
  }
  render(content) {//tabIndex='-1'
    return <div className='modal fade' id='mainModal' ref='modal' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
      <div className='modal-dialog' role='document'>
        {content}
      </div>
    </div>
  }
  renderHeader(title) {
    if (title) {
      return <div className='modal-header'>
        <button type='button' className='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h5 className='modal-title'>{title}</h5>
      </div>      
    }
  }
  hide() {
    this.$m.modal('hide');
  }
  hideWithCustomCallback(callback) {
    this.$m.unbind('hidden.bs.modal');
    this.$m.on('hidden.bs.modal', this.onHiddenWithCustomCallback.bind(this, callback));
    this.$m.modal('hide');
  }
  onHiddenWithCustomCallback(callback) {
    this.$m.unbind('hidden.bs.modal');
    callback();
  }
  runCallbackAndSetEmptyModal() {
    if (this.props.modal.callback) {
      this.props.modal.callback();
    }
    this.props.setModal();
  }
}

class LoginModal extends Modal {
  componentDidMount() {
    Meteor.setTimeout(()=>{this.refs.username.focus()}, 200); //TODO j.w.
    super.componentDidMount()
  }
  componentDidUpdate() {
    Meteor.setTimeout(()=>{this.refs.username.focus()}, 200); //TODO dlaczego autofocus nie robi się normalnie, tylko trzeba tak hackować
    super.componentDidUpdate()
  }
  render() {
    return super.render(<div className='modal-content login-modal'>
      {super.renderHeader(i18n('loginModal.loginToYourAccount'))}
      <div className='modal-body'>
        <p>{i18n('loginModal.collbrainDescription')}</p>
        <br/>
        <div className='social-icons-conatainer'>
          <div className='modal-body-left'>
            <div className='form-group'>
              <input type='text'
                ref='username'
                autoFocus
                placeholder={i18n('loginModal.name')}
                className='form-control login-field' />
              <i className='glyphicon glyphicon-user login-field-icon'></i>
            </div>

            <div className='form-group'>
              <input type='password' ref='password' placeholder={i18n('loginModal.password')} className='form-control login-field' />
              <i className='fa fa-lock login-field-icon'></i>
            </div>

            <a href='#' className='btn btn-success modal-login-btn' onClick={this.onLogin.bind(this)}>{i18n('loginModal.login')}</a>
            {/*<a href='#' className='login-link text-center'>{i18n('loginModal.lostYourPassword')}</a> TODO odzyskiwanie hasła*/}
          </div>

          <div className='modal-body-right'>
            <a href='#' className='btn btn-default facebook' onClick={this.onFacebook.bind(this)}>
              <i className='fa fa-facebook modal-icons'></i>{i18n('loginModal.signInWithFacebook')}
            </a>
            {/*TODO logowanie przez te portale:
            <a href='#' className='btn btn-default twitter'> <i className='fa fa-twitter modal-icons'></i> Sign In with Twitter </a>
            <a href='#' className='btn btn-default google'> <i className='fa fa-google-plus modal-icons'></i> Sign In with Google </a>
            <a href='#' className='btn btn-default linkedin'> <i className='fa fa-linkedin modal-icons'></i> Sign In with Linkedin </a>
            < + GITHUB> */}
          </div>
        </div>							
        <div className='form-group modal-register-btn'>
          <button className='btn btn-default' onClick={this.onRegister.bind(this)}>
            {i18n('loginModal.newUserRegister')}
          </button>
        </div>
      </div>
    </div>);
  }
  onFacebook() {
    Meteor.loginWithFacebook({requestPermissions: ['email']}, ()=>{
      if (Meteor.user()) {
        this.props.setModal();
        super.hide();
      }
    });
  }
  onLogin() {
    Meteor.loginWithPassword(this.refs.username.value, this.refs.password.value, ()=>{
      //TODO obsługa błędów logowania
      if (Meteor.user()) {
        this.props.setModal();
        super.hide();
      }
    });
  }
  onRegister() {
    super.hideWithCustomCallback(()=>{
      this.props.setModal('REGISTER', this.props.modal.callback);
    })
  }
}
LoginModal.propTypes = {
  setModal: PropTypes.func.isRequired
}
class RegisterModal extends Modal {
  componentDidMount() {
    Meteor.setTimeout(()=>{this.refs.username.focus()}, 200); //TODO j.w.
    super.componentDidMount()
  }
  componentDidUpdate() {
    Meteor.setTimeout(()=>{this.refs.username.focus()}, 200); //TODO dlaczego autofocus nie robi się normalnie, tylko trzeba tak hackować
    super.componentDidUpdate()
  }
  render() {
    return super.render(<div className='modal-content register-modal'>
      {super.renderHeader(i18n('registerModal.createNewAccount'))}
      <div className='register-modal-body'>
        <br/>
        <div className='social-icons-conatainer'>
          <div className='modal-body'>
            <div className='form-group'>
              <input type='text' ref='username' autoFocus placeholder={i18n('registerModal.name')}
                className='form-control register-field' />
              <i className='glyphicon glyphicon-user register-field-icon'></i>
            </div>
            <div className='form-group'>
              <input type='text' ref='email' placeholder={i18n('registerModal.email')} className='form-control register-field' />
              <i className='fa fa-envelope-o register-field-icon'></i>
            </div>
            <div className='form-group'>
              <input type='password' ref='password' placeholder={i18n('registerModal.password')} className='form-control register-field' />
              <i className='fa fa-lock register-field-icon'></i>
            </div>
            <div className='form-group'>
              <input type='password' ref='repeatPassword' placeholder={i18n('registerModal.repeatPassword')} className='form-control register-field' />
              <i className='fa fa-lock register-field-icon'></i>
            </div>
            <a href='#'
              className='btn btn-success modal-register-btn'
              onClick={this.onRegister.bind(this)} >
              {i18n('registerModal.register')}
            </a>
          </div>
        </div>
        <div className='alert alert-danger hidden fade in' ref='passwordsAlert'>
          <a className="close" onClick={this.onAlertClose.bind(this, 'passwordsAlert')}>&times;</a>
          {i18n('registerModal.passwordsDoesntMatch')}
        </div>
        <div className='alert alert-danger hidden fade in' ref='emailAlert'>
          <a className="close" onClick={this.onAlertClose.bind(this, 'emailAlert')}>&times;</a>
          {i18n('registerModal.wrongEmailAddress')}
        </div>
        <div className='alert alert-danger hidden fade in' ref='lengthAlert'>
          <a className="close" onClick={this.onAlertClose.bind(this, 'lengthAlert')}>&times;</a>
          {i18n('registerModal.passwordShouldBeAtLeast8CharactersLong')}
        </div>
        <div className='alert alert-danger hidden fade in' ref='usernameAlert'>
          <a className="close" onClick={this.onAlertClose.bind(this, 'usernameAlert')}>&times;</a>
          {i18n('registerModal.usernameIsRequired')}
        </div>
      </div>
    </div>);
  }
  onRegister() {
    if (this.refs.password.value != this.refs.repeatPassword.value) {
      $(this.refs.passwordsAlert).removeClass('hidden');
    } else if (!this.refs.username.value) {
      $(this.refs.usernameAlert).removeClass('hidden'); 
    } else if (!SimpleSchema.RegEx.Email.test(this.refs.email.value)) {
      $(this.refs.emailAlert).removeClass('hidden');      
    } else if (this.refs.password.value.length < 8) {
      $(this.refs.lengthAlert).removeClass('hidden'); 
    } else {
      Meteor.call('addUser', {
        username: this.refs.username.value,
        email: this.refs.email.value,
        password: this.refs.password.value
      }, (err)=>{
        if (err) {
          throw err;
        } else {
          Meteor.loginWithPassword(this.refs.username.value, this.refs.password.value, ()=>{
            if (Meteor.user()) {
              this.props.setModal();
              super.hide();
            }
          });
        }
      });     //TODO obsługa błędów zwracanych przez serwer
    }
  }
  onAlertClose(ref) {
    $(this.refs[ref]).addClass('hidden');
  }
}
RegisterModal.propTypes = {
  setModal: PropTypes.func.isRequired
}

class QuestionModal extends Modal {
  componentDidMount() {
    super.componentDidMount()
  }
  componentDidUpdate() {
    super.componentDidUpdate()
  }
  render() {
    return super.render(<div className='modal-content question-modal'>
      {super.renderHeader(this.props.modal.properties.title)}
      <div className='question-modal-body'>
        <div className='question-content'>{this.props.modal.properties.content}</div>
        <div className='question-options'>{this.renderOptions()}</div>
      </div>
    </div>);
  }
  onOptionClick(option) {
    option.function();
    super.hide();
  }
  renderOptions() {
    return this.props.modal.properties.options.map((option) => {
      return <a href='#'
        className={`btn ${option.bootstrapBtnClass || 'btn-default'} modal-register-btn`}
        onClick={this.onOptionClick.bind(this, option)} >
        {option.name}
      </a>
    })
  }
}
QuestionModal.propTypes = {
  setModal: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    properties: PropTypes.shape({
      title: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
      options: PropTypes.array.isRequired
    })
  })
}

export {LoginModal, RegisterModal, QuestionModal};
