import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import GlobalActions from '../actions';
import UserActions from '../../mods/User/client/UserActions';
import ThoughtActions from '../../mods/Thought/client/ThoughtActions';
import ThoughtEditorActions from '../../mods/ThoughtEditor/ThoughtEditorActions';
//import { reset } from 'redux-form';

import SubscribeComponent from '../helpers/SubscribeComponent';
import { Rect1 } from '../helpers/Shapes';
import SVGDefs from '../helpers/SVGDefs';
import createStore from '../createStore';
import ThoughtSpace from '../../mods/Thought/client/components/ThoughtSpace';
import ThoughtLayer from './ThoughtLayer';
import ThoughtEditor from '../../mods/ThoughtEditor/ThoughtEditor';
import {LoginModal, RegisterModal, QuestionModal} from '../modals';
//import { windowToSVG } from '../helpers/purecraft';
import UserAvatar from '../../mods/User/client/UserAvatar';
import Ride from '../../mods/Ride/client/Ride';
import pjson from '../../../package.json'; //TODO zamiast tego zrobić metodę pobierającą wersję z serwera, aby nie udostępniać package.json światu

class TheBigSpace extends Component {
  openThoughtWithResubscribe() {
    this.props.openThought(...arguments);
    //resubskrypcja naprawiająca buga przy przenoszeniu myśli w nowo otwartej przestrzeni:
  }
  moveThoughtWithResubscribe() {
    this.props.moveThought(...arguments);
    //resubskrypcja naprawiająca buga przy przenoszeniu myśli:

  }
  componentWillMount() {
//    this.props.subscribe('getUserData');
    //this.props.subscribe('lthoughts', this.props.pathParams);
//    this.props.subscribe('wordings')
//    this.props.subscribe('locations')
  }
  /*shouldComponentUpdate(nextProps) {
    if (JSON.stringify(nextProps.thoughts) == JSON.stringify(this.props.thoughts)) {
      return false;
    }
    return true;
  }*/
  componentWillUpdate(nextProps) {
    if (JSON.stringify(nextProps.pathParams) != JSON.stringify(this.props.pathParams)) {
      //Meteor.subscribe('lthoughts', nextProps.pathParams);
      //debugger
    }
  }
  componentWillReceiveProps() {
    ///**/nlog('#: TheBigSpace std', 'componentWillReceiveProps()')/**/
    //console.log('!!!! componentWillReceiveProps')
    //this.props.subscribe('lthoughts', this.props.pathParams);
    /*this.props.subscribe('getUserData');
    this.props.subscribe('thoughts')//, this.props.pathParams);
    this.props.subscribe('wordings')
    this.props.subscribe('locations')*/
  }
  login(callback) {
    this.props.setModal('LOGIN', callback);
  }
  onAddButton() {
    if (Meteor.user()) {
      this.props.openEditor()
    } else {
      this.login((()=>{
        if (Meteor.user()) {
          this.props.openEditor();
        }
      }).bind(this))
    }
  }
  onProfileButton() {
    if (!Meteor.user()) {
      this.login();
    } else {
      this.props.openUserThought(Meteor.userId());
    }
  }
  onLogoutButton() {
    Meteor.logout();
  }
  onHelpClick() {
    this.refs.ride.start();
    //this.refs.joyride.start(true);
  }
  renderAddButton() {
    return <div type={'button'} className={'addThoughtButton'} onClick={this.onAddButton.bind(this)}>+</div>;
  }
  onParentClickCreator(screenIdx) {
    return () => {
      this.props.goToMainParent(screenIdx)
    }
  }
  renderParents(vb, screenIdx) {
    //TODO uzupełnić to o renderowanie innych rodziców oprócz głównego
    if (this.props.thoughts['main_'+screenIdx].core.mainLocation) {
      return <Rect1 className='go-to-parent-button'
        filter='url(#sh2)'
        x={vb.x + gConst('goToParentButton.xShift')} y={vb.y + gConst('goToParentButton.yShift')}
        height={gConst('goToParentButton.height')} width={gConst('goToParentButton.width')} 
      onClick={this.props.goToMainParent.bind(this, screenIdx)} />
    }
  }
  onLinkClickCreator(link) {
    return function(e) {
      e.preventDefault();
      this.props.setMainThought(null, 0, link);
    }
  }
  renderSidePanel() {
    return <div className='sidePanel'>
      <div className='panel'>
        <a href='/heart'><div id='main-logo' className='logo' onClick={this.onLinkClickCreator('heart').bind(this)} /></a>
        <div className='version' ><span className='v1'>{i18n('sidePanel.version')}&nbsp;</span><span className='v2'>v</span><span className='v3'>{pjson.version}</span><span className='v4'>, {pjson.versionComment}</span></div>
        <a><div className='sideLink' id='mission' onClick={this.onLinkClickCreator('mission').bind(this)}>
          <span className='fa fa-globe'></span><span className='txt'> {i18n('sidePanel.mission')}</span>
        </div></a>
        <a><div className='sideLink' id='foundations' onClick={this.onLinkClickCreator('foundations').bind(this)}>
          <span className='fa fa-star'></span><span className='txt'> {i18n('sidePanel.foundations')}</span>
        </div></a>
        <a><div className='sideLink' id='ideas' onClick={this.onLinkClickCreator('ideas').bind(this)}>
          <span className='fa fa-lightbulb-o'></span><span className='txt'> {i18n('sidePanel.ideas')}</span>
        </div></a>
        <a><div className='sideLink' id='bugs' onClick={this.onLinkClickCreator('bugs').bind(this)}>
          <span className='fa fa-bug'></span><span className='txt'> {i18n('sidePanel.bugs')}</span>
        </div></a>
        {/*<div className='helpIcon' onClick={this.onHelpClick.bind(this)}>
          <span className='fa fa-question'></span>
        </div>TODO przywrócić gdy naprawi się Ride*/}
      </div>
    </div>
  }
  renderScreen(screen, lid) {
    /**/nlog('render()','#: TheBigSpace std');/**/
    var vb = screen.viewBox;
    return <div>
      <svg className={'main-svg'} viewBox={`${vb.x} ${vb.y} ${vb.width} ${vb.height}`}>
        <SVGDefs/>
        {this.renderParents(vb, screen.idx)}
        <ThoughtSpace
          key={lid}
          thought={this.props.thoughts[lid]}
          screen={screen}
          {...this.props} />
        <ThoughtLayer
          key={'thLayer'+lid}
          coords={screen.globalCoords}
          {...this.props}
          tidsToLids={this.props.addings.tidsToLids}
          openThought={this.openThoughtWithResubscribe.bind(this)}
          moveThought={this.moveThoughtWithResubscribe.bind(this)}
        screen={screen} />
      </svg>
      {this.props.addings.editor.isOpen ? <ThoughtEditor closeEditor={this.props.closeEditor}
        editor={this.props.addings.editor}
        screen={screen}
        {...this.props}
      /> : this.renderAddButton()}
      {/*<Ride ref='ride' {...this.props} />TODO odkomentować i zrobić sensowny ride*/}
    </div>
  }
  renderModal() {
    switch(this.props.addings.modal.type) {
      case 'LOGIN':
        return <LoginModal modal={this.props.addings.modal}
                setModal={this.props.setModal} />;
      case 'REGISTER':
        return <RegisterModal modal={this.props.addings.modal}
                setModal={this.props.setModal} />;
      case 'QUESTION':
        return <QuestionModal modal={this.props.addings.modal}
                setModal={this.props.setModal} />;
      default:
        return null;
    }
  }
  renderProfileButton() {
    var u = Meteor.user();
    if (u) {
      return <div className={'profile'}>
        <div className={'profileButton'} onClick={this.onProfileButton.bind(this)}>
          <UserAvatar user={u} size={50} />
        </div>
        <div className={'logoutButton'} onClick={this.onLogoutButton.bind(this)}>
          <span className="fa fa-power-off"></span>
        </div>
      </div>
    } else {
      return <div className={'profile'}>
        <div className={'profileButton'} onClick={this.onProfileButton.bind(this)}>
          <span className="glyphicon glyphicon-user"></span>
        </div>
      </div>
    }
  }
  /*render() {
    return <div>
      <div dangerouslySetInnerHTML={{__html: JSON.stringify( this.context.store.getState().addings, null, '&emsp;').replace(/\n/g,'<br/>')}}></div>
    </div>
  }*/
  render() {
    //return this.props.screens.map(renderScreen); //TODO zrobić wyświetlanie większej ilości ekranów na raz
    /**/ntimer.log('START', 'TBS render')/**/
    return <div>
      {this.props.screens[0] ? this.renderScreen(this.props.screens[0], 'main_0') : null}
      {this.renderProfileButton()}
      {this.renderSidePanel()}
      {this.renderModal()}
    </div>;
  }
}

TheBigSpace.propTypes = {
  subscribe: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setPathParams: PropTypes.func.isRequired,
  openThought: PropTypes.func.isRequired,
  closeThought: PropTypes.func.isRequired,
  closeAllChildren: PropTypes.func.isRequired,
  setMainThought: PropTypes.func.isRequired,
  highlightThought: PropTypes.func.isRequired,
  lowlightThought: PropTypes.func.isRequired,
  moveThought: PropTypes.func.isRequired,
  thoughts: PropTypes.object.isRequired,
  screens: PropTypes.array.isRequired
};
TheBigSpace.contextTypes = {
  store: PropTypes.object.isRequired
};
const mapStateToProps = state => {
  return { ...state };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({...GlobalActions, ...ThoughtActions, ...UserActions, ...ThoughtEditorActions}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(SubscribeComponent(TheBigSpace));
