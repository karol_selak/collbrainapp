import React, { Component, PropTypes } from 'react';
import ThoughtImage from '../../mods/Thought/client/components/ThoughtImage';
import RelationImage from '../../mods/Thought/client/components/RelationImage';

class ThoughtLayer extends Component {
  renderCommonThoughts() {
    var result = [];
    for (let lid of Object.keys(this.props.coords)) {
      var th = this.props.thoughts[lid];
      if (!this.props.coords[lid].ifRelation && !th.isConditional && !th.isVirtual) {
        result.push(<ThoughtImage key={lid}
          thought={th}
          coords={this.props.coords[lid].thoughtCoords}
          openThought={this.props.openThought}
          openUserThought={this.props.openUserThought}
          setMainThought={this.props.setMainThought}
          moveThought={this.props.moveThought}
          addLocation={this.props.addLocation}
          deleteThought={this.props.deleteThought}
          restoreDraft={this.props.restoreDraft}
          openEditor={this.props.openEditor}
        screen={this.props.screen} />);
      }
    }
    return result;
  }
  renderThIm(thought) {
    return <g className='th-image-of-relation' key={thought.lid}>
      <ThoughtImage 
        thought={thought}
        noLabels={true}
        coords={this.props.coords[thought.lid].thoughtCoords}
        openThought={this.props.setMainThought /*relacje otwierają się od razu jako mainy*/}
        openUserThought={this.props.openUserThought}
        setMainThought={this.props.setMainThought}
        moveThought={this.props.moveThought}
        addLocation={this.props.addLocation}
        deleteThought={this.props.deleteThought}
        restoreDraft={this.props.restoreDraft}
        openEditor={this.props.openEditor}
      screen={this.props.screen} />
    </g>
  }
  renderRelations() {
    var result = [];
    var thoughts = [];
    for (let i=0; ;i++) {
      var th = this.props.thoughts['rel_'+i];
      if (!th) {
        break;
      }
      var secondLid = undefined;
      var lids = this.props.tidsToLids[th.tid];
      if (lids.length > 1) {
        for (let i in lids) {
          if (lids[i].slice(0,4) != 'rel_') {
            secondLid = lids[i]; //przypadek z drugą lokacją, gdy wyginamy strzałkę w zależności od jej położenia
            break;
          }
        }
      }; //gdy nie ma drugiej lokacji, secondLid jest undefined i renderuje się strzałka prosta
      var input = this.props.thoughts[this.props.tidsToLids[th.core.input_tid][0]];
      var output = this.props.thoughts[this.props.tidsToLids[th.core.output_tid][0]];
      result.push(<RelationImage key={'rel_'+i+'_arrow'}
        thought={th}
        input={input}
        output={output}
        coords={this.props.coords}
        secondLocation={secondLid && this.props.thoughts[secondLid]}
        setMainThought={this.props.setMainThought}
        moveThought={this.props.moveThought}
        addLocation={this.props.addLocation}
        deleteThought={this.props.deleteThought}
        deleteLocation={this.props.deleteLocation}
        openEditor={this.props.openEditor}
      screen={this.props.screen} />);
      if (secondLid) {
        var th = this.props.thoughts[secondLid];
        if (th.isConditional || th.isVirtual) {
          thoughts.push(this.renderThIm(this.props.thoughts[secondLid]));
        }
      } else {
        thoughts.push(this.renderThIm(th));
      }
    }
    result = result.concat(thoughts);
    return result;
  }
  render() {
    /**/nlog(' ','#: Screen std nts');/**/
    /**/nlog('render()', '#: Screen std');/**/
    return <g>
      {this.renderRelations()}
      {this.renderCommonThoughts()}
    </g>;
  }
}
ThoughtLayer.propTypes = {
  coords: PropTypes.object.isRequired,
  thoughts: PropTypes.object.isRequired,
  screen: PropTypes.object.isRequired,
  openThought: PropTypes.func.isRequired,
  moveThought: PropTypes.func.isRequired,
  addLocation: PropTypes.func.isRequired,
  openUserThought: PropTypes.func.isRequired
};
export default ThoughtLayer;
