import React, { Component, PropTypes } from 'react';

class MainLayout extends Component {
  render() {
    /**/window._MainLayout = this;/**/
    return <main>
      {this.props.main}
    </main>;
  }
}
export default MainLayout;
