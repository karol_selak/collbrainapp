import '../imports/collections';
import '../imports/mods/Thought/ThoughtMethods';
import '../imports/mods/User/UserMethods';
import '../imports/methods';

//zablokowanie możliwości zaśmiecania bazy danych przez usera:
Meteor.users.deny({
  update() { return true; }
});

ServiceConfiguration.configurations.remove({
  service: 'facebook'
});
Meteor.settings.public.env = process.env.NODE_ENV;
console.log('===================================');
console.log('process.env.NODE_ENV == '+process.env.NODE_ENV);
console.log('===================================');
if (process.env.NODE_ENV === 'development') {
  ServiceConfiguration.configurations.insert({
    service: 'facebook',
    appId: '700539860099132',
    secret: '6c8d68b2585c867c3d1467c8c7fd124c'
  });
} else {
  ServiceConfiguration.configurations.insert({
    service: 'facebook',
    appId: '420209751465479',
    secret: 'bd229edebb5bd23ebaa6c9de8cfe1461'
  });
}

